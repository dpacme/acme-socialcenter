
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Client;
import domain.Penalty;
import domain.Request;
import domain.SocialCenter;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class RequestServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private RequestService		requestService;

	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private ClientService		clientService;

	// Templates --------------------------------------------------------------

	// Un actor autenticado como encargado debe ser capaz de listar los clientes de la aplicaci�n y navegar hacia sus penalizaciones, incidencias, peticiones y reservas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template3(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findOne(clientId);
			Request request = ((List<Request>) client.getRequests()).get(0);
			System.out.println(request.getId());
			
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
		
	// Un actor autenticado como encargado debe ser capaz de tramitar las peticiones de nuevos recursos realizadas por los clientes.
	// Comprobamos que se puede hacer una petici�n de un recurso correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de un centro social.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer requestId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			
			Request request = requestService.findOne(requestId);
			request.setStatus("completed");
			Request requestSaved = requestService.saveAndFlush(request);
			Assert.isTrue(requestSaved != null);
			System.out.println(requestSaved.getStatus());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	// Un actor autenticado como cliente debe ser capaz de realizar la petici�n de un recurso que el centro no dispone actualmente.
	// Comprobamos que se puede hacer una petici�n de un recurso correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de un centro social.
	// Test positivo y 2 tests negativos

	protected void template(String username, Integer socialCenterId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#requestResource");

			Request request = requestService.create();
			request.setComment("Comment");
			request.setStatus("pending");
			request.setClient(clientService.findByPrincipal());
			request.setSocialCenter(socialCenterService.findOne(socialCenterId));

			Request requestSaved = requestService.saveAndFlush(request);
			Assert.isTrue(requestSaved != null);
			System.out.println(requestSaved.getComment() + " - " + requestSaved.getStatus());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	//Drivers

	@Test
	public void requestResource() {

		List<SocialCenter> socialCenters = (List<SocialCenter>) socialCenterService.findAll();
		Object testingData[][] = {
			{
				"client1", socialCenters.get(0).getId(), null
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	
	@Test
	public void tramitarPeticiones() {

		List<Request> request1 = (List<Request>) requestService.findAll();
		Object testingData[][] = {
			{
				"manager1", request1.get(0).getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	

	@Test
	public void listRequestsFromClient() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Client client1 = clients.get(1);

		final Object testingData[][] = {
			{
				"manager1", client1.getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
