package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.ElectronicDevice;
import enums.TypeDevice;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ElectronicDeviceServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService	managerService;

	@Autowired
	private ElectronicDeviceService	electronicDeviceService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la creaci�n de un dispositivo electronico en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 */
	public void createElectronicDevice(final String username, final String picture, final String brand, final String model, final TypeDevice typeDevice, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			ElectronicDevice electronicDevice = this.electronicDeviceService.create(this.managerService.findByPrincipal().getSocialCenter());

			electronicDevice.setPicture(picture);
			electronicDevice.setBrand(brand);
			electronicDevice.setModel(model);
			electronicDevice.setTypeDevice(typeDevice);

			this.electronicDeviceService.comprobacion(electronicDevice);

			this.electronicDeviceService.save(electronicDevice);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un dispositivo electronico en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 * � El recurso no pertenece al centro social del encargado logueado
	 */
	public void editElectronicDevice(final String username, final Integer electronicDeviceId, final String picture, final String brand, final String model, final TypeDevice typeDevice, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			ElectronicDevice electronicDevice = this.electronicDeviceService.findOne(electronicDeviceId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(electronicDevice));

			electronicDevice.setPicture(picture);
			electronicDevice.setBrand(brand);
			electronicDevice.setModel(model);
			electronicDevice.setTypeDevice(typeDevice);

			this.electronicDeviceService.comprobacion(electronicDevice);

			this.electronicDeviceService.save(electronicDevice);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un dispositivo electronico en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � El recurso no pertenece al centro social del encargado logueado
	 * � El id del libro no existe
	 */
	public void deleteRoom(final String username, final Integer electronicDeviceId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			ElectronicDevice electronicDevice = this.electronicDeviceService.findOne(electronicDeviceId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(electronicDevice));

			this.electronicDeviceService.delete(electronicDevice);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createElectronicDeviceDriver() {

		final Object testingData[][] = {
			// Creacion de dispositivo electronico sin autentificarse -> false
			{
				null, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Creacion de dispositivo electronico autentificado (1) -> false
			{
				"admin", "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Creacion de dispositivo electronico autentificado (2) -> false
			{
				"client1", "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Creacion de dispositivo electronico autentificado (3) -> false
			{
				"monitor1", "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Creacion de dispositivo electronico con campos incorrectos -> false
			{
				"manager1", "http://www.picture.com", "", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Creacion de dispositivo electronico correctamente -> true
			{
				"manager1", "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createElectronicDevice((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (TypeDevice) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void editElectronicDeviceDriver() {

		final Object testingData[][] = {
			// Edicion de dispositivo electronico sin autentificarse -> false
			{
				null, 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Edicion de dispositivo electronico autentificado (1) -> false
			{
				"admin", 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Edicion de dispositivo electronico autentificado (2) -> false
			{
				"client1", 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Edicion de dispositivo electronico autentificado (3) -> false
			{
				"monitor1", 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Edicion de dispositivo electronico con campos incorrectos -> false
			{
				"manager1", 221, "http://www.picture.com", "", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// El dispositivo electronico no pertenece al centro social del manager logueado -> false
			{
				"manager2", 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, IllegalArgumentException.class
			},
			// Edicion de dispositivo electronico correctamente -> true
			{
				"manager1", 221, "http://www.picture.com", "Brand", "Model", TypeDevice.COMPUTER, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editElectronicDevice((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (TypeDevice) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void deleteElectronicDeviceDriver() {

		final Object testingData[][] = {
			// Borrado de dispositivo electronico sin autentificarse -> false
			{
				null, 221, IllegalArgumentException.class
			},
			// Borrado de dispositivo electronico autentificado (1) -> false
			{
				"admin", 221, IllegalArgumentException.class
			},
			// Borrado de dispositivo electronico autentificado (2) -> false
			{
				"client1", 221, IllegalArgumentException.class
			},
			// Borrado de dispositivo electronico autentificado (3) -> false
			{
				"monitor1", 221, IllegalArgumentException.class
			},
			// El id del dispositivo electronico no existe -> false
			{
				"manager1", 9999, IllegalArgumentException.class
			},
			// El dispositivo electronico no pertenece al centro social del manager logueado -> false
			{
				"manager2", 221, IllegalArgumentException.class
			},
			// Borrado de dispositivo electronico correctamente -> true
			{
				"manager1", 221, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteRoom((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
