
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Manager;
import forms.ManagerForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ManagerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService managerService;

	@Autowired
	private AdministratorService	administratorService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor que no est� autenticado debe poder: Registrarse como cliente.
	 *
	 * En este caso de uso se llevara a cabo el registro de un manager en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como administrador
	 * � Atributos del registro incorrectos
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerManager(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String identifier, final String newUsername, final String password,
		final String secondPassword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.administratorService.checkIfAdministrator();

			// Inicializamos los atributos para la creaci�n
			ManagerForm actor = new ManagerForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setDni(identifier);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			//Reconsturimos
			Manager manager = this.managerService.reconstruct(actor);

			//Comprobamos atributos
			this.managerService.comprobacion(manager);

			//Guardamos
			this.managerService.saveForm(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado debe ser capaz de: Editar su informaci�n personal.
	 *
	 * En este caso de uso se llevara a cabo la edicion del perfil de un encargado en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � Atributos incorrectos
	 */
	public void editManager(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			// Inicializamos los atributos para la creaci�n
			Manager manager = this.managerService.findByPrincipal();

			manager.setName(name);
			manager.setSurname(surname);
			manager.setEmail(email);
			manager.setPhone(phone);
			manager.setPostalAddress(postalAddress);

			//Comprobamos atributos
			this.managerService.comprobacion2(manager);

			//Guardamos
			this.managerService.save(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerManagerDriver() {

		final Object testingData[][] = {
			// Edicion de manager sin autentificarse -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username1", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager como autentificado (1) -> false
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username2", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager como autentificado (2) -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username3", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager como autentificado (3) -> false
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username3", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager con postalAddress incorrecto -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "9999", "username4", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager con usuario no �nico -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "client1", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager con contrase�as no coincidentes -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username7", "password1", "password2", IllegalArgumentException.class
			},
			// Edicion de manager con tel�fono incorrecto -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "9999", "username10", "password1", "password1", IllegalArgumentException.class
			},
			// Edicion de manager con todo correcto -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username8", "password1", "password1", null
			},
			// Edicion de manager con codigopostal vacio -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "2", "username9", "password1", "password1", null
			},
			// Edicion de manager con telefono vacio -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "3", "username13", "password1", "password1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}


	@Test
	public void editManagerDriver() {

		final Object testingData[][] = {
			// Edicion de encargado como autentificado (1) -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", IllegalArgumentException.class
			},
			// Edicion de encargado con postalAddress incorrecto -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", IllegalArgumentException.class
			},
			// Edicion de encargado con tel�fono incorrecto -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", IllegalArgumentException.class
			},
			// Edicion de encargado con todo correcto -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", null
			},
			// Edicion de administrator con codigopostal vacio -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", null
			},
			// Edicion de encargado con telefono vacio -> true
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
