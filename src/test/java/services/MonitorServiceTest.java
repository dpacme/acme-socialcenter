
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Monitor;
import forms.MonitorForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MonitorServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private MonitorService monitorService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor que no est� autenticado debe poder: Registrarse como monitor.
	 *
	 * En este caso de uso se llevara a cabo el registro de un monitor en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerMonitor(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String identifier, final String newUsername,
		final String password,
		final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			MonitorForm actor = new MonitorForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setDni(identifier);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			Monitor monitor = this.monitorService.reconstruct(actor);

			//Comprobamos atributos
			this.monitorService.comprobacion(monitor);

			//Guardamos
			this.monitorService.saveForm(monitor);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado debe ser capaz de: Editar su informaci�n personal.
	 *
	 * En este caso de uso se llevara a cabo la edicion del perfil de un monitor en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � Atributos incorrectos
	 */
	public void editMonitor(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			// Inicializamos los atributos para la creaci�n
			Monitor monitor = this.monitorService.findByPrincipal();

			monitor.setName(name);
			monitor.setSurname(surname);
			monitor.setEmail(email);
			monitor.setPhone(phone);
			monitor.setPostalAddress(postalAddress);

			//Comprobamos atributos
			this.monitorService.comprobacion2(monitor);

			//Guardamos
			this.monitorService.save(monitor);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------


	@Test
	public void registerMonitortDriver() {

		final Object testingData[][] = {
			// Creaci�n de monitor como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor como autentificado (2) -> false
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor como autentificado (3) -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor como autentificado (4) -> false
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "9999", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de monitor con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "client1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "9999", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de monitor con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "9999", "username8", "password1", "password1", true, null
			},
			// Creaci�n de monitor con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "2", "username9", "password1", "password1", true, null
			},
			// Creaci�n de monitor con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "3", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerMonitor((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (Boolean) testingData[i][10], (Class<?>) testingData[i][11]);
	}

	@Test
	public void editMonitorDriver() {

		final Object testingData[][] = {
			// Edicion de monitor como autentificado (1) -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", IllegalArgumentException.class
			},
			// Edicion de monitor con postalAddress incorrecto -> false
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", IllegalArgumentException.class
			},
			// Edicion de monitor con tel�fono incorrecto -> false
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", IllegalArgumentException.class
			},
			// Edicion de monitor con todo correcto -> true
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", null
			},
			// Edicion de monitor con codigopostal vacio -> true
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", null
			},
			// Edicion de monitor con telefono vacio -> true
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editMonitor((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
