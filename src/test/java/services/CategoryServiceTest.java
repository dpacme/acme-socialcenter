
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Category;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CategoryService categoryService;

	// Templates --------------------------------------------------------------


	//Un actor autenticado como encargado debe ser capaz de administrar las categor�as de los libros, incluyendo listar, crear, editar y borrar las mismas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	@Test
	public void manageCategory() {

		Category category = categoryService.create();
		category.setTitle("Category");
		Category categorySaved = categoryService.saveAndFlush(category);

		final Object testingData[][] = {
			{
				"manager1", categorySaved.getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer categoryId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#manageCategory");
			List<Category> categories = (List<Category>) categoryService.findAll();
			Assert.isTrue(categories != null && !categories.isEmpty());
			for (Category c : categories)
				System.out.println(c.getTitle());

			Category category = categoryService.create();
			category.setTitle("Category");
			Category categorySaved = categoryService.saveAndFlush(category);
			Assert.isTrue(categorySaved != null);
			System.out.println(categorySaved.getTitle());

			Category categoryEdited = categoryService.findOne(categoryId);
			categoryEdited.setTitle("Category Modificada");
			Category categorySavedModified = categoryService.saveAndFlush(categoryEdited);
			Assert.isTrue(categorySavedModified != null);
			System.out.println(categorySavedModified.getTitle());

			categoryService.delete(categorySavedModified);
			try {
				categoryService.findOne(categorySavedModified.getId());
			} catch (Exception e) {
				System.out.println("Categoria eliminada correctamente");
			}

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
