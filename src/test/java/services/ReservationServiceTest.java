
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Book;
import domain.Category;
import domain.Client;
import domain.ElectronicDevice;
import domain.Manager;
import domain.Penalty;
import domain.Reservation;
import domain.Room;
import domain.SocialCenter;
import enums.TypeDevice;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ReservationServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ReservationService		reservationService;
	@Autowired
	private ClientService			clientService;

	@Autowired
	private ResourceService			resourceService;

	@Autowired
	private BookService				bookService;

	@Autowired
	private RoomService				roomService;

	@Autowired
	private ElectronicDeviceService	electronicDeviceService;

	@Autowired
	private ManagerService			managerService;
	@Autowired
	private CategoryService			categoryService;

	@Autowired
	private PenaltyService			penaltyService;

	// Templates --------------------------------------------------------------


	// Un actor autenticado como encargado debe ser capaz de listar los clientes de la aplicaci�n y navegar hacia sus penalizaciones, incidencias, peticiones y reservas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template2(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findOne(clientId);
			List<Reservation> reservations = (List<Reservation>) client.getReservations();

			System.out.println(reservations.get(0).getId());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de realizar la reserva de alg�n recurso que est� disponible.
	// Dispositivo o libro

	protected void reserveResourceDriver(String username, int resourceId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			resourceService.reserveResource(resourceId);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de realizar la reserva de alg�n recurso que est� disponible.
	// Sala

	protected void reserveRoomDriver(String username, String codigo, int resourceId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			reservationService.createReservationRoom(codigo, resourceId);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de listar las reservas activas que tenga.

	protected void activeReservationsDriver(String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findByPrincipal();

			Collection<Reservation> reservations = reservationService.activeReservations(client.getId());

			System.out.println("\n####activeReservations####");
			System.out.println("\nActive reservation from " + client.getName());
			for (Reservation reservation : reservations) {
				System.out.println("\nRescurso reservado: " + reservation.getResource().getTicker());
			}

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de cancelar una reserva siempre y
	// cuando el momento de la cancelaci�n sea al menos un d�a antes de la fecha de inicio de la reserva.

	protected void cancelReservationsDriver(String username, int reservationId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);

			Reservation reservation = reservationService.findOne(reservationId);
			reservationService.delete(reservation);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de listar su historial de reservas

	protected void reservationsHistoryDriver(String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findByPrincipal();

			Collection<Reservation> reservations = reservationService.inactiveReservations(client.getId());

			System.out.println("\n####ReservationsHistory####");
			System.out.println("\nHistory of reservation from " + client.getName());
			for (Reservation reservation : reservations) {
				System.out.println("\nRescurso reservado: " + reservation.getResource().getTicker());
			}

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como encargadp debe ser capaz de tramitar la devoluci�n de un recurso de su centro social

	protected void finalizeReservationDriver(String username, int reservationId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);

			reservationService.finalizeReservation(reservationId);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	//********************* TESTS ****************//

	@Test
	public void listReservationsFromClient() {


		List<Client> clients = (List<Client>) clientService.findAll();
		Client client = clients.get(1);

		final Object testingData[][] = {
			{
				"manager1", client.getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/**
	 * Para realizar este test es necesario que se ejecute entre las 10 de la ma�ana y 10 de la noche
	 * porque el servicio controla que no se realicen reservas mas all� de las 10, como regla de negocio.
	 */
	@Test
	public void reserveResource() {


		Calendar calendar = Calendar.getInstance();
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
		//		Assert.isTrue(!(calendar.get(Calendar.HOUR_OF_DAY) >= 22 || calendar.get(Calendar.DAY_OF_YEAR) > dayOfYear),
		//			"Para realizar este test es necesario que se ejecute entre las 10 de la ma�ana y 10 de la noche porque el servicio controla que no se realicen reservas mas all� de las 10, como regla de negocio.");
		calendar.add(Calendar.YEAR, -2);

		Class<?> esperado = null;
		Class<?> caught = null;

		List<Category> categories = (List<Category>) categoryService.findAll();

		authenticate("manager1");
		Manager manager = managerService.findByPrincipal();
		SocialCenter socialCenter = manager.getSocialCenter();

		Book book = bookService.create(socialCenter);
		book.setIsbn("isbnTest");
		book.setTitle("titleTest");
		book.setAuthor("AuthorTest");
		book.setEditorial("EditorialTest");
		book.setDescription("DescriptionTest");
		book.setPublicationDate(calendar.getTime());
		book.setCategories(categories);
		book = bookService.saveAndFlush(book);

		Book book2 = bookService.create(socialCenter);
		book2.setIsbn("isbnTest");
		book2.setTitle("titleTest");
		book2.setAuthor("AuthorTest");
		book2.setEditorial("EditorialTest");
		book2.setDescription("DescriptionTest");
		book2.setPublicationDate(calendar.getTime());
		book2.setCategories(categories);
		book2 = bookService.saveAndFlush(book2);

		ElectronicDevice device = electronicDeviceService.create(socialCenter);
		device.setBrand("BrandTest");
		device.setModel("TestModel");
		device.setTypeDevice(TypeDevice.EBOOK);
		device = electronicDeviceService.saveAndFlush(device);

		ElectronicDevice device2 = electronicDeviceService.create(socialCenter);
		device2.setBrand("BrandTest");
		device2.setModel("TestModel");
		device2.setTypeDevice(TypeDevice.EBOOK);
		device2 = electronicDeviceService.saveAndFlush(device2);

		ElectronicDevice device3 = electronicDeviceService.create(socialCenter);
		device3.setBrand("BrandTest");
		device3.setModel("TestModel");
		device3.setTypeDevice(TypeDevice.EBOOK);
		device3 = electronicDeviceService.saveAndFlush(device3);

		unauthenticate();

		try {
			if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 || calendar.get(Calendar.DAY_OF_YEAR) > dayOfYear) {
				esperado = IllegalArgumentException.class;
			}

			authenticate("client6");
			resourceService.reserveResource(book2.getId());
			resourceService.reserveResource(device2.getId());

			unauthenticate();

			authenticate("client2");
			Calendar startDate = Calendar.getInstance();
			startDate.add(Calendar.DAY_OF_YEAR, -4);
			Calendar endDate = Calendar.getInstance();
			endDate.add(Calendar.MONTH, 4);

			Client client = clientService.findByPrincipal();
			Penalty penalty = penaltyService.create();
			penalty.setClient(client);
			penalty.setStartDate(startDate.getTime());
			penalty.setEndDate(endDate.getTime());
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}
		if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 || calendar.get(Calendar.DAY_OF_YEAR) > dayOfYear) {
			checkExceptions(esperado, caught);
		}





		final Object testingData[][] = {
			{//Reserva correcta de un libro
				"client1", book.getId(), null
			}, {//Reserva correcta de un dispositivo
				"client1", device.getId(), esperado
			}, {//Reserva erronea de un libro actualmente reservado
				"client1", book2.getId(), IllegalArgumentException.class
			}, {//Reserva erronea de un dispositivo actualmente reservado
				"client1", device2.getId(), IllegalArgumentException.class
			}, {//Reserva erronea con un actor erroneo
				"monitor1", book.getId(), IllegalArgumentException.class
			}, {//Reserva erronea con un actor con 3 reservas activas
				"client4", book.getId(), IllegalArgumentException.class
			}, {//Reserva erronea con un actor baneado
				"client3", book.getId(), IllegalArgumentException.class
			}, {//Reserva erronea con un actor penalizado
				"client2", book.getId(), IllegalArgumentException.class
			}, {//Reserva erronea con un id de recurso erroneo
				"client1", 0, IllegalArgumentException.class
			},
		};

		for (int i = 0; i < testingData.length; i++)
			reserveResourceDriver((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void reserveRoom() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -2);

		authenticate("manager1");
		Manager manager = managerService.findByPrincipal();
		SocialCenter socialCenter = manager.getSocialCenter();

		Room room = roomService.create(socialCenter);
		room.setCapacity(5);
		room.setBoard(true);
		room.setTv(false);
		room = roomService.saveAndFlush(room);

		unauthenticate();

		authenticate("client1");
		reservationService.createReservationRoom("X-12:00- 14:00", room.getId());

		unauthenticate();

		final Object testingData[][] = {
			{//Reserva correcta de un libro
				"client1", "M-12:00- 14:00", room.getId(), null
			}, {//Reserva correcta de un libro
				"client2", "M-12:00- 14:00", room.getId(), null
			}, {//Reserva erronea, actor incorrecto
				"manager1", "M-12:00- 14:00", room.getId(), IllegalArgumentException.class
			}, {//Reserva erronea, c�digo incorrecto
				"manager1", "AA", room.getId(), IllegalArgumentException.class
			}, {//Reserva erronea, id de sala incorrecto
				"manager1", "M-12:00- 14:00", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			reserveRoomDriver((String) testingData[i][0], (String) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void activeReservations() {

		final Object testingData[][] = {
			{//Listado correcto
				"client1", null
			}, {//Listado correcto
				"client2", null
			}, {//Error, usuario de otro tipo logado
				"manager1", IllegalArgumentException.class
			}, {//Error, usuario inexsistente
				"0", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			activeReservationsDriver((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void cancelReservations() {

		authenticate("manager1");
		Manager manager = managerService.findByPrincipal();
		SocialCenter socialCenter = manager.getSocialCenter();

		Room room = roomService.create(socialCenter);
		room.setCapacity(5);
		room.setBoard(true);
		room.setTv(false);
		room = roomService.saveAndFlush(room);

		unauthenticate();

		authenticate("client1");
		Reservation reservation = reservationService.createReservationRoom("X-12:00- 14:00", room.getId());
		unauthenticate();

		final Object testingData[][] = {
			{//Cancelar correctamente
				"client1", reservation.getId(), null
			}, {//Cancelar erroneamente con id de reserva incorrecto
				"client1", 0, IllegalArgumentException.class
			}, {//Cancelar erroneamente con otro cliente que no haya creado la reserva
				"client2", reservation.getId(), IllegalArgumentException.class
			}, {//Cancelar erroneamente con usuario incorrecto
				"manager1", reservation.getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			cancelReservationsDriver((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void ReservationsHistory() {

		final Object testingData[][] = {
			{//Listado correcto
				"client1", null
			}, {//Listado correcto
				"client2", null
			}, {//Error, usuario de otro tipo logado
				"manager1", IllegalArgumentException.class
			}, {//Error, usuario inexsistente
				"0", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			reservationsHistoryDriver((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void finalizeReservation() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -2);

		List<Category> categories = (List<Category>) categoryService.findAll();

		authenticate("manager1");
		Manager manager = managerService.findByPrincipal();
		SocialCenter socialCenter = manager.getSocialCenter();

		Book book = bookService.create(socialCenter);
		book.setIsbn("isbnTest");
		book.setTitle("titleTest");
		book.setAuthor("AuthorTest");
		book.setEditorial("EditorialTest");
		book.setDescription("DescriptionTest");
		book.setPublicationDate(calendar.getTime());
		book.setCategories(categories);
		book = bookService.saveAndFlush(book);


		unauthenticate();

		authenticate("client6");
		Reservation reservation = resourceService.reserveResource(book.getId());

		unauthenticate();

		final Object testingData[][] = {
			{//Finalizar reserva correctamente
				"manager1", reservation.getId(), null
			},
			{//Error, manager de otro centro social no puede cancelar la reserva de este recurso
				"manager2", reservation.getId(), IllegalArgumentException.class
			},
			{//Error, usuario logado incorrecto
				"admin", reservation.getId(), IllegalArgumentException.class
			}, {//Error, id del recurso incorrecta
				"manager1", 0, IllegalArgumentException.class
			},
		};

		for (int i = 0; i < testingData.length; i++)
			finalizeReservationDriver((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
