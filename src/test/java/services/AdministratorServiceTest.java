
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AdministratorServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AdministratorService administratorService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor autenticado debe ser capaz de: Editar su informaci�n personal.
	 *
	 * En este caso de uso se llevara a cabo la edicion del perfil de un administrador en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � Atributos incorrectos
	 */
	public void editAdministrator(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			// Inicializamos los atributos para la creaci�n
			Administrator administrator = this.administratorService.findByPrincipal();

			administrator.setName(name);
			administrator.setSurname(surname);
			administrator.setEmail(email);
			administrator.setPhone(phone);
			administrator.setPostalAddress(postalAddress);

			//Comprobamos atributos
			this.administratorService.comprobacion(administrator);

			//Guardamos
			this.administratorService.save(administrator);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------




	@Test
	public void editAdministratorDriver() {

		final Object testingData[][] = {
			// Edicion de administrator como autentificado (1) -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", IllegalArgumentException.class
			},
			// Edicion de administrator con postalAddress incorrecto -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", IllegalArgumentException.class
			},
			// Edicion de administrator con tel�fono incorrecto -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", IllegalArgumentException.class
			},
			// Edicion de administrator con todo correcto -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", null
			},
			// Edicion de administrator con codigopostal vacio -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", null
			},
			// Edicion de administrator con telefono vacio -> true
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editAdministrator((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
