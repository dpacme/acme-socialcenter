package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Room;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class RoomServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService	managerService;

	@Autowired
	private RoomService		roomService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la creaci�n de una sala en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 */
	public void createRoom(final String username, final String picture, final Integer capacity, final Boolean tv, final Boolean board, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Room room = this.roomService.create(this.managerService.findByPrincipal().getSocialCenter());

			room.setPicture(picture);
			room.setCapacity(capacity);
			room.setBoard(board);
			room.setTv(tv);

			this.roomService.comprobacion(room);

			this.roomService.save(room);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la edicion de una sala en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 * � El recurso no pertenece al centro social del encargado logueado
	 */
	public void editRoom(final String username, final Integer roomId, final String picture, final Integer capacity, final Boolean tv, final Boolean board, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Room room = this.roomService.findOne(roomId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(room));

			room.setPicture(picture);
			room.setCapacity(capacity);
			room.setBoard(board);
			room.setTv(tv);

			this.roomService.comprobacion(room);

			this.roomService.save(room);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo el borrado de una sala en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � El recurso no pertenece al centro social del encargado logueado
	 * � El id del libro no existe
	 */
	public void deleteRoom(final String username, final Integer roomId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Room room = this.roomService.findOne(roomId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(room));

			this.roomService.delete(room);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createRoomDriver() {

		final Object testingData[][] = {
			// Creacion de sala sin autentificarse -> false
			{
				null, "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Creacion de sala autentificado (1) -> false
			{
				"admin", "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Creacion de sala autentificado (2) -> false
			{
				"client1", "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Creacion de sala autentificado (3) -> false
			{
				"monitor1", "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Creacion de sala con campos incorrectos -> false
			{
				"manager1", "http://www.picture.com", null, true, false, IllegalArgumentException.class
			},
			// Creacion de sala correctamente -> true
			{
				"manager1", "http://www.picture.com", 6, true, false, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createRoom((String) testingData[i][0], (String) testingData[i][1], (Integer) testingData[i][2], (Boolean) testingData[i][3], (Boolean) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void editRoomDriver() {

		final Object testingData[][] = {
			// Edicion de sala sin autentificarse -> false
			{
				null, 227, "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Edicion de sala autentificado (1) -> false
			{
				"admin", 227, "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Edicion de sala autentificado (2) -> false
			{
				"client1", 227, "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Edicion de sala autentificado (3) -> false
			{
				"monitor1", 227, "http://www.picture.com", 6, true, false, IllegalArgumentException.class
			},
			// Edicion de sala con campos incorrectos -> false
			{
				"manager1", 227, "http://www.picture.com", null, true, false, IllegalArgumentException.class
			},
			// Edicion de sala correctamente -> true
			{
				"manager1", 227, "http://www.picture.com", 6, true, false, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editRoom((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Boolean) testingData[i][4], (Boolean) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void deleteRoomDriver() {

		final Object testingData[][] = {
			// Borrado de sala sin autentificarse -> false
			{
				null, 227, IllegalArgumentException.class
			},
			// Borrado de sala autentificado (1) -> false
			{
				"admin", 227, IllegalArgumentException.class
			},
			// Borrado de sala autentificado (2) -> false
			{
				"client1", 227, IllegalArgumentException.class
			},
			// Borrado de sala autentificado (3) -> false
			{
				"monitor1", 227, IllegalArgumentException.class
			},
			// El id del sala no existe -> false
			{
				"manager1", 9999, IllegalArgumentException.class
			},
			// La sala no pertenece al centro social del manager logueado -> false
			{
				"manager2", 227, IllegalArgumentException.class
			},
			// Borrado de sala correctamente -> true
			{
				"manager1", 228, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteRoom((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
