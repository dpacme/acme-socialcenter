
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Comment;
import domain.Resource;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CommentService	commentService;

	@Autowired
	private ResourceService	resourceService;

	@Autowired
	private ClientService	clientService;

	// Templates --------------------------------------------------------------


	// Un actor autenticado como cliente debe ser capaz de crear comentarios de los recursos que ha usado donde incluye la puntuaci�n.
	// Comprobamos que se crea un comentario correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de un recurso.
	// Test positivo y 2 tests negativos
	@Test
	public void createComment() {

		List<Resource> resources = (List<Resource>) resourceService.findAll();
		Object testingData[][] = {
			{
				"client1", resources.get(0).getId(), null
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer resourceId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#createComment");

			Comment comment = commentService.create();
			comment.setDescription("Comment");
			comment.setScore(3);
			comment.setClient(clientService.findByPrincipal());
			comment.setResource(resourceService.findOne(resourceId));

			Comment commentSaved = commentService.saveAndFlush(comment);
			Assert.isTrue(commentSaved != null);
			System.out.println(commentSaved.getDescription() + " - " + commentSaved.getScore());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
