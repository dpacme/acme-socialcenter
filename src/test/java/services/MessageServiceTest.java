
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Client;
import domain.Incidence;
import domain.Manager;
import domain.Monitor;
import domain.Reservation;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MessageServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private MessageService	messageService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private MonitorService		monitorService;

	@Autowired
	private IncidenceService	incidenceService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor autenticado como cliente debe ser capaz de escribir un mensaje nuevo a un encargado
	 * sobre una incidencia y contestar a un mensaje de encargado existente.
	 *
	 * Test positivo cuando un cliente envia un mensaje sobre una de sus incidencias
	 * Test negativo sin estar autenticado como cliente o manager
	 * Test negativo cuando un cliente trata de enviar un mensaje sobre la incidencia de otro cliente
	 */
	@Test
	public void clientSendMessageIncidence() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() >= 2, "No hay suficientes clientes para realizar el test");

		List<Manager> managers = (List<Manager>) managerService.findAll();
		Assert.isTrue(managers != null && !managers.isEmpty(), "No hay suficientes managers para realizar el test");
		Manager manager = managers.get(0);

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		//Se buscan 2 clientes con incidencias
		Assert.isTrue(clients != null && !clients.isEmpty());
		Client client1 = null;
		Incidence incidence1 = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				for (Reservation r : c.getReservations()) {
					if (r.getIncidences() != null && !r.getIncidences().isEmpty()) {
						for (Incidence i : r.getIncidences()) {
							if (i.getStatus().equals("open")) {
								incidence1 = i;
								client1 = c;
								break;
							}
						}

					}
				}
			}
			if (client1 != null) {
				break;
			}
		}

		Assert.isTrue(client1 != null && incidence1 != null);

		clients.remove(client1);

		Client client2 = null;
		Incidence incidence2 = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				for (Reservation r : c.getReservations()) {
					if (r.getIncidences() != null && !r.getIncidences().isEmpty()) {
						List<Incidence> incidences = (List<Incidence>) r.getIncidences();
						incidence2 = incidences.get(0);
						client2 = c;
						break;
					}
				}
			}
			if (client2 != null) {
				break;
			}
		}

		Assert.isTrue(client2 != null && incidence2 != null);

		Object testingData[][] = {
			{
				client1.getUserAccount().getUsername(), incidence1.getId(), manager.getId(), null
			}, {
				monitor.getUserAccount().getUsername(), incidence1.getId(), manager.getId(), IllegalArgumentException.class
			}, {
				client1.getUserAccount().getUsername(), incidence2.getId(), manager.getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	protected void template3(String username, Integer incidenceId, Integer actorReceivedId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			boolean exito = this.messageService.sendMessage("Hola", incidenceId, actorReceivedId, null);
			Assert.isTrue(exito);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de escribir un mensaje nuevo
	 * a un cliente sobre una incidencia y contestar a un mensaje de cliente existente.
	 * Siempre y cuando la incidencia est� abierta.
	 *
	 * Test positivo cuando un manager envia un mensaje sobre una de las incidencias abiertas del sistema
	 * Test negativo estando autenticado como monitor
	 * Test negativo cuando se trata de enviar un mensaje sobre una incidencia que no est� abierta
	 */
	@Test
	public void managerSendMessageIncidence() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() >= 2, "No hay suficientes clientes para realizar el test");

		List<Manager> managers = (List<Manager>) managerService.findAll();
		Assert.isTrue(managers != null && !managers.isEmpty(), "No hay suficientes managers para realizar el test");
		Manager manager = managers.get(0);

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		Collection<Incidence> incidences = this.incidenceService.findAll();
		Incidence incidenciaAbierta = null;
		Incidence incidenciaNoAbierta = null;
		for (Incidence i : incidences) {
			if (i.getStatus().equals("open")) {
				incidenciaAbierta = i;
			} else {
				incidenciaNoAbierta = i;
			}
			if (incidenciaAbierta != null && incidenciaNoAbierta != null) {
				break;
			}
		}

		Assert.notNull(incidenciaNoAbierta);
		Assert.notNull(incidenciaAbierta);

		Object testingData[][] = {
			{
				manager.getUserAccount().getUsername(), incidenciaAbierta.getId(), incidenciaAbierta.getReservation().getClient().getId(), null
			}, {
				monitor.getUserAccount().getUsername(), incidenciaAbierta.getId(), incidenciaAbierta.getReservation().getClient().getId(), IllegalArgumentException.class
			}, {
				manager.getUserAccount().getUsername(), incidenciaNoAbierta.getId(), incidenciaNoAbierta.getReservation().getClient().getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	protected void template(String username, Integer incidenceId, Integer actorReceivedId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			boolean exito = this.messageService.sendMessage("Hola", incidenceId, actorReceivedId, null);
			Assert.isTrue(exito);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
