
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Category;
import domain.Client;
import domain.Monitor;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ActivityServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private MonitorService monitorService;

	// Templates --------------------------------------------------------------


	@SuppressWarnings("deprecation")
	// Un actor autenticado como monitor debe ser capaz de gestionar una actividad, lo que incluye, listar, crear, editar o borrar. Una actividad solo puede ser editada o borrada si no tiene clientes inscritos en la actividad.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	@Test
	public void manageActivity() {

		List<Activity> activity = (List<Activity>) activityService.findAll();

		final Object testingData[][] = {
			{
				"monitor1", activity.get(0).getId(), null
			}, {
				"monitor1", null, DataIntegrityViolationException.class
			}, {
				"monitor1", 0, DataIntegrityViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	@SuppressWarnings("deprecation")
	protected void template3(String username, Integer activityId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#manageActivity");
			List<Activity> activities = (List<Activity>) activityService.findAll();
			Assert.isTrue(activities != null && !activities.isEmpty());
			for (Activity c : activities)
				System.out.println(c.getTitle());

			Activity activity = activityService.create();
			activity.setTitle("Activity");
			activity.setDescription("Descripcion");
			Date date = new Date();
			date.setYear(2020);
			activity.setEndDate(date);
			List<Monitor> monitor = (List<Monitor>) monitorService.findAll();
			activity.setMonitor(monitor.get(0));
			activity.setSeats(4);
			activity.setStartDate(new Date());
			Activity activitySaved = activityService.saveAndFlush(activity);
			Assert.isTrue(activitySaved != null);
			System.out.println(activitySaved.getTitle());

			Activity activityEdited = activityService.findOne(activityId);
			activityEdited.setTitle("Activity Modificada");
			Activity activitySavedModified = activityService.saveAndFlush(activityEdited);
			Assert.isTrue(activitySavedModified != null);
			System.out.println(activitySavedModified.getTitle());

			activityService.delete(activitySavedModified);
			try {
				activityService.findOne(activitySavedModified.getId());
			} catch (Exception e) {
				System.out.println("Avtividad eliminada correctamente");
			}

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	// Un actor autenticado como cliente debe ser capaz de apuntarse a una actividad y darse de baja de la misma, siempre y cuando la actividad a�n no haya empezado.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template2(String username, Integer activityId, String apply, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			
			Activity activity = activityService.findOne(activityId);
			Collection<Client> clients = activity.getClients();
			if(apply == "apply")
				clients.add(clientService.findByPrincipal());
			else 
				clients.remove(clientService.findByPrincipal());
			activity.setClients(clients);
			Activity activitySaved = activityService.saveAndFlush(activity);
			System.out.println(activitySaved.getTitle());
			
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	// Un actor autenticado como cliente debe ser capaz de listar las actividades existentes y consultar los detalles de cada una de ellas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template(String username, Integer activityId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			
			Activity activity = activityService.findOne(activityId);
			System.out.println(activity.getTitle());
			
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	
	//Drivers
	
	@Test
	public void listActivities() {

		List<Activity> activity = (List<Activity>) activityService.findAll();

		final Object testingData[][] = {
			{
				"client1", activity.get(0).getId(), null
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	
	@Test
	public void applyUnapplyActivity() {
		List<Activity> activity = (List<Activity>) activityService.findAll();

		final Object testingData[][] = {
			{
				"client1", activity.get(0).getId(), "unapply", null
			},{
				"client1", activity.get(1).getId(), "apply", null
			}, {
				"client1", null, null, NullPointerException.class
			}, {
				"client1", 0, "", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}
	

}
