
package services;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.PenaltyParameter;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PenaltyParameterServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private PenaltyParameterService penaltyParameterService;

	// Templates --------------------------------------------------------------

	// Un actor autenticado como administrador debe ser capaz de editar el valor por defecto del tiempo de una penalización.


	protected void updatePenaltyParameterDriver(String username, int days, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			List<PenaltyParameter> penaltyParameters = (List<PenaltyParameter>) penaltyParameterService.findAll();

			PenaltyParameter penaltyParameter = penaltyParameters.get(0);
			penaltyParameter.setDays(days);

			penaltyParameterService.save(penaltyParameter);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	//Drivers

	@Test
	public void updatePenaltyParameter() {

		final Object testingData[][] = {
			{
				"admin", 5, null
			},
			{
				"admin", -5, IllegalArgumentException.class
			},
			{
				"manager1", 5, ConstraintViolationException.class
			},
		};

		for (int i = 0; i < testingData.length; i++)
			updatePenaltyParameterDriver((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}



}
