package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Book;
import domain.Category;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class BookServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService	managerService;

	@Autowired
	private BookService		bookService;

	@Autowired
	private CategoryService	categoryService;

	// Templates --------------------------------------------------------------


	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la creaci�n de un libro en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 */
	public void createBook(final String username, final String picture, final String isbn, final String title, final String author, final String editorial, final String description, final String publicationDate, final Integer categoryId,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Book book = this.bookService.create(this.managerService.findByPrincipal().getSocialCenter());

			book.setPicture(picture);
			book.setIsbn(isbn);
			book.setTitle(title);
			book.setAuthor(author);
			book.setEditorial(editorial);
			book.setDescription(description);
			book.setPublicationDate(new Date(publicationDate));

			Collection<Category> categories = new ArrayList<Category>();
			Category category = this.categoryService.findOne(categoryId);
			categories.add(category);
			book.setCategories(categories);

			this.bookService.comprobacion(book);

			this.bookService.save(book);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un libro en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � Atributos del registro incorrectos
	 * � El recurso no pertenece al centro social del encargado logueado
	 */
	public void editBook(final String username, final Integer bookId, final String picture, final String isbn, final String title, final String author, final String editorial, final String description, final String publicationDate,
		final Integer categoryId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Book book = this.bookService.findOne(bookId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(book));

			book.setPicture(picture);
			book.setIsbn(isbn);
			book.setTitle(title);
			book.setAuthor(author);
			book.setEditorial(editorial);
			book.setDescription(description);
			book.setPublicationDate(new Date(publicationDate));

			Collection<Category> categories = new ArrayList<Category>();
			Category category = this.categoryService.findOne(categoryId);
			categories.add(category);
			book.setCategories(categories);

			this.bookService.comprobacion(book);

			this.bookService.save(book);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de administrar su centro social, lo que incluye listar, crear, editar y borrar sus recursos.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un libro en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado como manager
	 * � El recurso no pertenece al centro social del encargado logueado
	 * � El id del libro no existe
	 */
	public void deleteBook(final String username, final Integer bookId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			Book book = this.bookService.findOne(bookId);

			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(book));

			this.bookService.delete(book);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createBookDriver() {

		final Object testingData[][] = {
			// Creacion de libro sin autentificarse -> false
			{
				null, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Creacion de libro autentificado (1) -> false
			{
				"admin", "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Creacion de libro autentificado (2) -> false
			{
				"client1", "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Creacion de libro autentificado (3) -> false
			{
				"monitor1", "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Creacion de libro con campos incorrectos -> false
			{
				"manager1", "http://www.picture.com", "", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Creacion de libro con campos incorrectos -> false
			{
				"manager1", "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/2018", 250, IllegalArgumentException.class
			},
			// Creacion de libro correctamente -> true
			{
				"manager1", "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createBook((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Integer) testingData[i][8], (Class<?>) testingData[i][9]);
	}

	@Test
	public void editBookDriver() {

		final Object testingData[][] = {
			// Edicion de libro sin autentificarse -> false
			{
				null, 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Edicion de libro autentificado (1) -> false
			{
				"admin", 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Edicion de libro autentificado (2) -> false
			{
				"client1", 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Edicion de libro autentificado (3) -> false
			{
				"monitor1", 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// Edicion de libro con campos incorrectos -> false
			{
				"manager1", 239, "http://www.picture.com", "", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, IllegalArgumentException.class
			},
			// El libro no pertenece al centro social del manager logueado -> false
			{
				"manager2", 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/2018", 250, IllegalArgumentException.class
			},
			// Edicion de libro correctamente -> true
			{
				"manager1", 239, "http://www.picture.com", "Isbn", "Title", "Author", "Editorial", "Description", "10/21/1994", 250, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editBook((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Integer) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void deleteBookDriver() {

		final Object testingData[][] = {
			// Borrado de libro sin autentificarse -> false
			{
				null, 232, IllegalArgumentException.class
			},
			// Borrado de libro autentificado (1) -> false
			{
				"admin", 232, IllegalArgumentException.class
			},
			// Borrado de libro autentificado (2) -> false
			{
				"client1", 232, IllegalArgumentException.class
			},
			// Borrado de libro autentificado (3) -> false
			{
				"monitor1", 232, IllegalArgumentException.class
			},
			// El id del libro no existe -> false
			{
				"manager1", 9999, IllegalArgumentException.class
			},
			// El libro no pertenece al centro social del manager logueado -> false
			{
				"manager2", 232, IllegalArgumentException.class
			},
			// Borrado de libro correctamente -> true
			{
				"manager1", 232, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteBook((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
