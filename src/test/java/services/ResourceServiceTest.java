
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Resource;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ResourceServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ResourceService resourceService;

	// Templates --------------------------------------------------------------

	/**
	 * Un actor que no est� autenticado debe poder buscar un recurso que contiene una palabra clave en su t�tulo, autor o descripci�n.
	 *
	 * Test positivo realizando una busqueda correcta general (sin id de socialCenter como due�o del recurso para que busque en todos)
	 * Test negativo facilitando un id de socialCenter asociado (para filtrar sobre los recursos de un centro social concreto) inv�lido
	 *
	 * No se realiza segundo test negativo dado que el m�todo est� preparado para que se le puedan pasar valores nulos como parametro
	 * y no hay control de roles.
	 */
	@Test
	public void searchResource() {

		/*
		 * Se busca el primer recurso que se encuentre para usar su ticker como
		 * keyword para validar que funciona el filtro
		 */
		List<Resource> resources = (List<Resource>) resourceService.findAll();
		Assert.isTrue(resources != null && !resources.isEmpty());

		String keyword = resources.get(0).getTicker();

		/*
		 * Usamos el id de la socialCenter como id de socialCenter incorrecto
		 */
		Integer idSocialCenter = resources.get(0).getId();

		final Object testingData[][] = {
			{
				keyword, null, null
			}, {
				keyword, idSocialCenter, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String keyword, Integer socialCenterId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			List<Resource> resources = (List<Resource>) resourceService.searchResources(keyword, socialCenterId, false);
			Assert.isTrue(resources != null && !resources.isEmpty());
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}


}
