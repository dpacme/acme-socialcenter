
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Assessment;
import domain.Like;
import domain.SocialCenter;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AssessmentServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AssessmentService	assessmentService;

	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private LikeService			likeService;
	// Templates --------------------------------------------------------------


	// Un actor autenticado como cliente debe ser capaz de realizar una valoraci�n sobre un centro social.
	// Comprobamos que se puede crear la valoraci�n de un centro social correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de un centro social.
	// Test positivo y 2 tests negativos
	@Test
	public void writeAssessment() {

		List<SocialCenter> socialCenters = (List<SocialCenter>) socialCenterService.findAll();
		Object testingData[][] = {
			{
				"client1", socialCenters.get(0).getId(), null
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer socialCenterId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#assessmentResource");

			Assessment assessment = assessmentService.create();
			assessment.setComment("Comment");
			assessment.setTitle("Title");
			assessment.setClient(clientService.findByPrincipal());
			assessment.setSocialCenter(socialCenterService.findOne(socialCenterId));
			Assessment assessmentSaved = assessmentService.saveAndFlush(assessment);

			Like like = likeService.create();
			like.setLikeMe(true);
			like.setClient(clientService.findByPrincipal());
			like.setAssessment(assessmentSaved);
			Like likeSaved = likeService.saveAndFlush(like);

			Assert.isTrue(assessmentSaved != null && likeSaved.getLikeMe());
			System.out.println(assessmentSaved.getComment() + " - " + assessmentSaved.getTitle() + " - " + likeSaved.getLikeMe());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
