
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Assessment;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class LikeServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private AssessmentService assessmentService;

	// Templates --------------------------------------------------------------


	// Un actor autenticado como cliente debe ser capaz de realizar un "Me gusta" o "No me gusta" de una valoración de un centro social.
	// Comprobamos que se puede realizar un like/dislike de una valoración de un centro social correctamente y para los tests negativos vemos si salta la excepción correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de una valoración.
	// Test positivo y 2 tests negativos
	@Test
	public void likeDislikeAssessment() {

		List<Assessment> assessments = (List<Assessment>) assessmentService.findAll();
		Object testingData[][] = {
			{
				"client1", assessments.get(0).getId(), null
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer assessmentId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#likeDislikeAssessment");

			Assessment assessment = assessmentService.findOne(assessmentId);

			boolean like = assessmentService.likeAssessment(assessmentId);
			boolean dislike = assessmentService.dislikeAssessment(assessmentId);
			Assert.isTrue(like == true && dislike == true);
			System.out.println(assessment.getTitle() + " - " + like + " - " + dislike);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
