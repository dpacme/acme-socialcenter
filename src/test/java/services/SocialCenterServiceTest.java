
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Assessment;
import domain.Book;
import domain.Category;
import domain.Manager;
import domain.Resource;
import domain.SocialCenter;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class SocialCenterServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private BookService			bookService;

	@Autowired
	private CategoryService		categoryService;

	@Autowired
	private ResourceService		resourceService;
	// Templates --------------------------------------------------------------


	// Un actor que no est� autenticado debe poder navegar por la lista de centros sociales, incluyendo los recursos y sus valoraciones, ordenadas en funci�n del ratio likes/dislikes.
	// Comprobamos que los centros sociales,recursos y valores se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void listSocialCentersResourcesAssessments() {

		List<SocialCenter> socialCenters = (List<SocialCenter>) socialCenterService.findAll();
		Object testingData[][] = {
			{
				"", socialCenters.get(0).getId(), null
			}, {
				"", null, NullPointerException.class
			}, {
				"", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer socialCenterId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			Collection<SocialCenter> socialCenters = socialCenterService.findAll();
			SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);
			System.out.println("#listSocialCentersResourcesAssessments");
			Assert.isTrue(socialCenters != null && !socialCenters.isEmpty() && socialCenter.getResources() != null && !socialCenter.getResources().isEmpty() && socialCenter.getAssessments() != null && !socialCenter.getAssessments().isEmpty());
			for (SocialCenter o : socialCenters)
				System.out.println(o.getName());
			for (domain.Resource o : socialCenter.getResources())
				System.out.println(o.getTicker());
			for (Assessment o : socialCenter.getAssessments())
				System.out.println(o.getTitle());
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * Un actor que no est� autenticado debe poder buscar un centro social concreto que contiene una palabra clave en su nombre, mostrando un mapa con marcadores en la posici�n de los centros encontrados.
	 *
	 * Test positivo realizando una busqueda correcta general
	 *
	 * No se realiza ning�n test negativo dado que el m�todo est� preparado para que se le puedan pasar valores nulos en todos los par�metros
	 * y no hay control de roles.
	 */
	@Test
	public void searchSocialCenters() {

		/*
		 * Se busca el primer socialCenter que se encuentre para usar su nombre como
		 * keyword para validar que funciona el filtro
		 */
		List<SocialCenter> socialCenters = (List<SocialCenter>) socialCenterService.findAll();
		Assert.isTrue(socialCenters != null && !socialCenters.isEmpty());

		String keyword = socialCenters.get(0).getName();

		final Object testingData[][] = {
			{
				keyword, null, null
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String keyword, Integer idSocialCenter, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			List<SocialCenter> socialCenters = (List<SocialCenter>) socialCenterService.searchSocialCenters(keyword);
			Assert.isTrue(socialCenters != null && !socialCenters.isEmpty());
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * Un actor autenticado como administrador debe ser capaz de administrar los centros sociales, incluyendo la creaci�n,
	 * edici�n y borrado de centros sociales. Al crear un centro social, debe asociarse a, al menos, un encargado existente.
	 *
	 */

	protected void manageSocialCenterDriver(String username, String name, String city, Double latitude, Double longitude, Integer managerId, Integer resourceId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);

			Manager manager = managerService.findOne(managerId);
			List<Manager> managers = new ArrayList<>();
			managers.add(manager);

			SocialCenter socialCenter = socialCenterService.create();
			socialCenter.setName(name);
			socialCenter.setCity(city);
			socialCenter.setLongitude(longitude);
			socialCenter.setLatitude(latitude);
			socialCenter.setManagers(managers);
			if (resourceId != null) {
				List<Resource> resources = new ArrayList<>();
				Resource resource = resourceService.findOne(resourceId);
				resources.add(resource);
				socialCenter.setResources(resources);
			}

			SocialCenter socialCenterSaved = socialCenterService.saveAndFlush(socialCenter);


			socialCenterSaved.setCity("newCity");
			socialCenterSaved.setName("NewName");
			socialCenterSaved.setLatitude(12.0);
			socialCenterSaved.setLongitude(12.0);

			SocialCenter socialCenterUpdated = socialCenterService.saveAndFlush(socialCenterSaved);

			socialCenterService.delete(socialCenterUpdated);

			unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	@Test
	public void manageSocialCenter() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -2);

		List<Category> categories = (List<Category>) categoryService.findAll();

		List<Manager> managers = (List<Manager>) managerService.findAll();

		Manager manager = managers.get(managers.size() - 1);

		authenticate("manager1");
		Manager manager2 = managerService.findByPrincipal();
		SocialCenter socialCenter = manager2.getSocialCenter();

		Book book = bookService.create(socialCenter);
		book.setIsbn("isbnTest");
		book.setTitle("titleTest");
		book.setAuthor("AuthorTest");
		book.setEditorial("EditorialTest");
		book.setDescription("DescriptionTest");
		book.setPublicationDate(calendar.getTime());
		book.setCategories(categories);
		book = bookService.saveAndFlush(book);

		unauthenticate();

		final Object testingData[][] = {
			{//Manage correcto
				"admin", "nameTest", "cityTest", 85.8787, -85.8787, manager.getId(), null, null
			}, {//Error en la creaci�n, coordenadas incorrectas
				"admin", "nameTest", "cityTest", 185.8787, -85.8787, manager.getId(), null, IllegalArgumentException.class
			}, {//Error en la creaci�n, id manager incorrecto
				"admin", "nameTest", "cityTest", 85.8787, -85.8787, 0, null, IllegalArgumentException.class
			}, {//Error delete, el centro social tiene recursos
				"admin", "nameTest", "cityTest", 85.8787, -85.8787, manager.getId(), book.getId(), IllegalArgumentException.class
			}, {//Error create, nombre = null
				"admin", null, "cityTest", 85.8787, -85.8787, manager.getId(), null, ConstraintViolationException.class
			}, {//Error create, city = null
				"admin", "nameTest", null, 85.8787, -85.8787, manager.getId(), null, ConstraintViolationException.class
			}, {//Error create, latitude = null
				"admin", "nameTest", "cityTest", null, -85.8787, manager.getId(), null, NullPointerException.class
			}, {//Error create, longitude = null
				"admin", "nameTest", "cityTest", -85.8787, null, manager.getId(), null, NullPointerException.class
			}, {//Error create, usuario incorrecto
				"manager1", "nameTest", "cityTest", -85.8787, -85.8787, manager.getId(), null, IllegalArgumentException.class
			}, {//Error create, usuario inexistente
				"0", "nameTest", "cityTest", -85.8787, -85.8787, manager.getId(), null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			manageSocialCenterDriver((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Double) testingData[i][3], (Double) testingData[i][4], (Integer) testingData[i][5], (Integer) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}
}
