
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Client;
import forms.ClientForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ClientServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private PenaltyService penaltyService;
	@Autowired
	private MonitorService	monitorService;

	// Templates --------------------------------------------------------------

	// Un actor autenticado como encargado debe ser capaz de listar los clientes de la aplicaci�n y navegar hacia sus penalizaciones, incidencias, peticiones y reservas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);

			Client client = this.clientService.findOne(clientId);
			System.out.println(client.getName());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	//Drivers

	@Test
	public void listClients() {

		List<Client> client = (List<Client>) this.clientService.findAll();

		final Object testingData[][] = {
			{
				"manager1", client.get(0).getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}








	/*
	 * Un actor que no est� autenticado debe poder: Registrarse como cliente.
	 *
	 * En este caso de uso se llevara a cabo el registro de un cliente en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerClient(final String username, final String name, final String surname, final String email, final String phone, final String picture, final String postalAddress, final String identifier, final String newUsername,
		final String password,
		final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			ClientForm actor = new ClientForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setPicture(picture);
			actor.setDni(identifier);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			Client client = this.clientService.reconstruct(actor);

			//Comprobamos atributos
			this.clientService.comprobacion(client);

			//Guardamos
			this.clientService.saveForm(client);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado debe ser capaz de: Editar su informaci�n personal.
	 *
	 * En este caso de uso se llevara a cabo la edicion del perfil de un cliente en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � Atributos incorrectos
	 */
	public void editClient(final String username, final String name, final String surname, final String email, final String phone, final String picture, final String postalAddress, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			// Inicializamos los atributos para la creaci�n
			Client client = this.clientService.findByPrincipal();

			client.setName(name);
			client.setSurname(surname);
			client.setEmail(email);
			client.setPhone(phone);
			client.setPostalAddress(postalAddress);
			client.setPicture(picture);

			//Comprobamos atributos
			this.clientService.comprobacion2(client);

			//Guardamos
			this.clientService.save(client);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como monitor debe ser capaz de: Ver la lista de clientes que asistir�n a una actividad.
	 *
	 * En este caso de uso se llevara a cabo el listado de los clientes que asistiran a una actividad.
	 * Para provocar el error se pueden dar los siguientes casos:
	 *
	 * � El usuario no esta autentificado como monitor
	 * � El id de la actividad no existe
	 */
	public void listClientActivity(final String username, final Integer activityId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.monitorService.checkIfMonitor();

			Activity activity = this.activityService.findOne(activityId);
			Collection<Client> clients = activity.getClients();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------


	@Test
	public void registerClientDriver() {

		final Object testingData[][] = {
			// Edicion de cliente como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente como autentificado (2) -> false
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente como autentificado (3) -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente como autentificado (4) -> false
			{
				"monitor1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "56118916511", "9999", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Edicion de cliente con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "client1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Edicion de cliente con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "http://www.picture.com", "41010", "9999", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Edicion de cliente con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", "9999", "username8", "password1", "password1", true, null
			},
			// Edicion de cliente con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "", "2", "username9", "password1", "password1", true, null
			},
			// Edicion de cliente con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "http://www.picture.com", "41010", "3", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerClient((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
	}

	@Test
	public void editClientDriver() {

		final Object testingData[][] = {
			// Creaci�n de cliente como autentificado (1) -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", IllegalArgumentException.class
			},
			// Creaci�n de cliente con postalAddress incorrecto -> false
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "56118916511", IllegalArgumentException.class
			},
			// Creaci�n de cliente con tel�fono incorrecto -> false
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "http://www.picture.com", "41010", IllegalArgumentException.class
			},
			// Creaci�n de cliente con todo correcto -> true
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "41010", null
			},
			// Creaci�n de cliente con codigopostal vacio -> true
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "http://www.picture.com", "", null
			},
			// Creaci�n de cliente con telefono vacio -> true
			{
				"client1", "NameTest1", "SurnameTest1", "email@domain.com", "", "http://www.picture.com", "41010", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editClient((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	@Test
	public void listClientActivityDriver() {

		final Object testingData[][] = {
			// List de cliente como no autentificado -> false
			{
				null, 210, IllegalArgumentException.class
			},
			// List de cliente como autentificado (1) -> false
			{
				"admin", 210, IllegalArgumentException.class
			},
			// List de cliente como autentificado (2) -> false
			{
				"client1", 210, IllegalArgumentException.class
			},
			// List de cliente como autentificado (3) -> false
			{
				"manager1", 210, IllegalArgumentException.class
			},
			// El id del activity no existe -> false
			{
				"monitor1", 9999, IllegalArgumentException.class
			},
			// List de cliente correcto -> true
			{
				"monitor1", 210, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listClientActivity((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
