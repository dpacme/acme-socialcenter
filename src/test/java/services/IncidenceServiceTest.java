
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Client;
import domain.Incidence;
import domain.Penalty;
import domain.Manager;
import domain.Message;
import domain.Monitor;
import domain.Reservation;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class IncidenceServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private IncidenceService incidenceService;
	@Autowired
	private ClientService clientService;

	@Autowired
	private MonitorService		monitorService;

	@Autowired
	private ManagerService		managerService;

	// Templates --------------------------------------------------------------

	// Un actor autenticado como encargado debe ser capaz de listar los clientes de la aplicaci�n y navegar hacia sus penalizaciones, incidencias, peticiones y reservas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template2(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findOne(clientId);
			List<Reservation> reservations = (List<Reservation>) client.getReservations();
			for(Reservation r:reservations) {
				if(!r.getIncidences().isEmpty()) {
					System.out.println(r.getIncidences());
				}
			}
			
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como encargado debe ser capaz de cambiar el estado de una incidencia a cerrada (si la incidencia no puede ser resuelta) o resuelta (si la incidencia se ha solventado correctamente).
	// Comprobamos que se puede cambiar el estado de una incidencia correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de una incidencia.
	// Test positivo y 2 tests negativos

	@Test
	public void changeStatusIncidence() {

		List<Incidence> incidences = (List<Incidence>) incidenceService.findAll();
		Object testingData[][] = {
			{
				"monitor1", incidences.get(0).getId(), null
			}, {
				"monitor1", null, NullPointerException.class
			}, {
				"monitor1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer incidenceId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			System.out.println("#changeStatusIncidence");

			incidenceService.changeStatusIncidence(incidenceId, true);

			Incidence incidence = incidenceService.findOne(incidenceId);

			Assert.isTrue(incidence != null && incidence.getStatus() == "solved");
			System.out.println(incidence.getTitle() + " - " + incidence.getStatus());

			incidenceService.changeStatusIncidence(incidenceId, false);

			Incidence incidenceF = incidenceService.findOne(incidenceId);

			Assert.isTrue(incidenceF != null && incidenceF.getStatus() == "closed");
			System.out.println(incidence.getTitle() + " - " + incidence.getStatus());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
	

	@Test
	public void listIncidencesFromClient() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Client client = clients.get(0);

		final Object testingData[][] = {
			{
				"manager1", client.getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Un actor autenticado como cliente debe ser capaz de listar las incidencias que tenga asociadas.
	 *
	 * Test positivo obteniendo las incidencias de un cliente correctamente
	 * Test negativo sin estar autenticado como cliente o manager
	 * Test negativo cuando un cliente trata de obtener los datos de otro
	 */
	@Test
	public void listIncidenceFromClient() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() >= 2, "No hay suficientes clientes para realizar el test");

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		Client client1 = clients.get(0);
		Client client2 = clients.get(1);
		Object testingData[][] = {
			{
				client1.getUserAccount().getUsername(), client1.getId(), null
			}, {
				monitor.getUserAccount().getUsername(), client1.getId(), IllegalArgumentException.class
			}, {
				client1.getUserAccount().getUsername(), client2.getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template7((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template7(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Collection<Incidence> incidences = this.incidenceService.findByClientId(clientId);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como cliente debe ser capaz de ver los detalles de una de sus incidencias,
	 * incluyendo los mensajes asociados a la incidencia.
	 *
	 * Test positivo obteniendo los mensajes de la incidencia de un cliente correctamente
	 * Test negativo sin estar autenticado como cliente o manager
	 * Test negativo cuando un cliente trata de obtener los mensajes de la incidencia de otro
	 */
	@Test
	public void showIncidenceFromClient() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() >= 2, "No hay suficientes clientes para realizar el test");

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		//Se buscan 2 clientes con incidencias
		Assert.isTrue(clients != null && !clients.isEmpty());
		Client client1 = null;
		Incidence incidence1 = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				for (Reservation r : c.getReservations()) {
					if (r.getIncidences() != null && !r.getIncidences().isEmpty()) {
						List<Incidence> incidences = (List<Incidence>) r.getIncidences();
						incidence1 = incidences.get(0);
						client1 = c;
						break;
					}
				}
			}
			if (client1 != null) {
				break;
			}
		}

		Assert.isTrue(client1 != null && incidence1 != null);

		clients.remove(client1);

		Client client2 = null;
		Incidence incidence2 = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				for (Reservation r : c.getReservations()) {
					if (r.getIncidences() != null && !r.getIncidences().isEmpty()) {
						List<Incidence> incidences = (List<Incidence>) r.getIncidences();
						incidence2 = incidences.get(0);
						client2 = c;
						break;
					}
				}
			}
			if (client2 != null) {
				break;
			}
		}

		Assert.isTrue(client2 != null && incidence2 != null);

		Object testingData[][] = {
			{
				client1.getUserAccount().getUsername(), incidence1.getId(), null
			}, {
				monitor.getUserAccount().getUsername(), incidence1.getId(), IllegalArgumentException.class
			}, {
				client1.getUserAccount().getUsername(), incidence2.getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void template3(String username, Integer incidenceId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Collection<Message> messages = this.incidenceService.findMessagesOrdered(incidenceId);
			Assert.notNull(messages);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de crear una nueva incidencia relacionada con la reserva de un cliente.
	 *
	 * Test positivo encargado crea incidencia sobre una reserva
	 * Test negativo sin aportar titulo ni descripcion
	 * Test negativo actor que no tiene rol de encargado trata de crear la incidencia
	 */
	@Test
	public void managerCreateIncidence() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() > 0, "No hay suficientes clientes para realizar el test");

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		List<Manager> managers = (List<Manager>) managerService.findAll();
		Assert.isTrue(managers != null && !managers.isEmpty(), "No hay suficientes managers para realizar el test");
		Manager manager = managers.get(0);

		//Se buscan cliente con incidencia
		Assert.isTrue(clients != null && !clients.isEmpty());
		Client client1 = null;
		Reservation reservation = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				List<Reservation> incidences = (List<Reservation>) c.getReservations();
				reservation = incidences.get(0);
				client1 = c;
				break;
			}
		}

		Assert.isTrue(client1!=null&&reservation!=null);

		Object testingData[][] = {
			{
				manager.getUserAccount().getUsername(), "title", "description", reservation.getId(), null
			}, {
				manager.getUserAccount().getUsername(), null, null, reservation.getId(), IllegalArgumentException.class
			}, {
				monitor.getUserAccount().getUsername(), "title", "description", reservation.getId(), IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template4((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	protected void template4(String username,String title, String description, Integer reservationId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Incidence incidence = this.incidenceService.create(title, description, reservationId);
			Assert.notNull(incidence);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Un actor autenticado como encargado debe ser capaz de listar todas las incidencias del sistema y ver sus detalles,
	 * incluyendo los mensajes asociados a cada incidencia.
	 *
	 * Test positivo obteniendo todas las incidencias del sistema y obteniendo los mensajes asociados de una de ellas
	 * Test negativo estando logueado como monitor
	 * Test negativo intentando obtener los mensajes con un id de incidencia incorrecto
	 */
	@Test
	public void managerListIncidenceAndDetails() {

		List<Client> clients = (List<Client>) clientService.findAll();
		Assert.isTrue(clients != null && clients.size() > 0, "No hay suficientes clientes para realizar el test");

		List<Monitor> monitors = (List<Monitor>) monitorService.findAll();
		Assert.isTrue(monitors != null && !monitors.isEmpty(), "No hay suficientes monitores para realizar el test");
		Monitor monitor = monitors.get(0);

		List<Manager> managers = (List<Manager>) managerService.findAll();
		Assert.isTrue(managers != null && !managers.isEmpty(), "No hay suficientes managers para realizar el test");
		Manager manager = managers.get(0);

		//Se busca incidencia
		Assert.isTrue(clients != null && !clients.isEmpty());
		Client client1 = null;
		Incidence incidence1 = null;
		for (Client c : clients) {
			if (c.getReservations() != null && !c.getReservations().isEmpty()) {
				for (Reservation r : c.getReservations()) {
					if (r.getIncidences() != null && !r.getIncidences().isEmpty()) {
						List<Incidence> incidences = (List<Incidence>) r.getIncidences();
						incidence1 = incidences.get(0);
						client1 = c;
						break;
					}
				}
			}
			if (client1 != null) {
				break;
			}
		}

		Assert.isTrue(client1 != null && incidence1 != null);

		Integer idIncidenceIncorrecto = client1.getId();

		Object testingData[][] = {
			{
				manager.getUserAccount().getUsername(), incidence1.getId(), null
			}, {
				monitor.getUserAccount().getUsername(), incidence1.getId(), IllegalArgumentException.class
			}, {
				manager.getUserAccount().getUsername(), idIncidenceIncorrecto, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template5((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void template5(String username, Integer incidenceId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Collection<Incidence> incidences = this.incidenceService.findAll();
			Assert.notNull(incidences);
			Collection<Message> messages = this.incidenceService.findMessagesOrdered(incidenceId);
			Assert.notNull(messages);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}
}
