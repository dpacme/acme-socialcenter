
package services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Client;
import domain.Penalty;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PenaltyServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ClientService clientService;
	@Autowired
	private PenaltyService penaltyService;

	// Templates --------------------------------------------------------------

	// Un actor autenticado como encargado debe ser capaz de editar la fecha final de una penalización de un cliente.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepción correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	@SuppressWarnings("deprecation")
	protected void template3(String username, Integer penaltyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Penalty penalty = penaltyService.findOne(penaltyId);
			Date date = penalty.getEndDate();
			date.setYear(2020);
			penalty.setEndDate(date);
			Penalty penaltySaved = penaltyService.saveAndFlush(penalty);
			System.out.println(penaltySaved.getEndDate());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como encargado debe ser capaz de listar los clientes de la aplicación y navegar hacia sus penalizaciones, incidencias, peticiones y reservas.
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepción correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template2(String username, Integer clientId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Client client = clientService.findOne(clientId);
			Penalty penalty = ((List<Penalty>) client.getPenalties()).get(0);
			System.out.println(penalty.getId());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// Un actor autenticado como cliente debe ser capaz de consultar sus penalizaciones
	// Comprobamos que se listan, crea, edita y borra una categoria correctamente y para los tests negativos vemos si salta la excepción correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto de la categoria.
	// Test positivo y 2 tests negativos
	protected void template(String username, Integer penaltyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);

			Penalty penalty = penaltyService.findOne(penaltyId);
			System.out.println(penalty.getId());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	//Drivers

	@Test
	public void listPenalties() {

		Penalty penalty = ((List<Penalty>) penaltyService.findAll()).get(0);

		final Object testingData[][] = {
			{
				"client2", penalty.getId(), null
			},{
				"client", penalty.getId(), IllegalArgumentException.class
			}, {
				"client1", null, NullPointerException.class
			}, {
				"client1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listPenaltiesFromClient() {
		Penalty penalty = null;
		List<Client> client = (List<Client>) clientService.findAll();
		for(Client c:client) {
			if(!c.getPenalties().isEmpty()) {
				penalty = ((List<Penalty>) c.getPenalties()).get(0);
				break;
			}
		}

		final Object testingData[][] = {
			{
				"manager1", penalty.getId(), null
			}, {
				"manager1", null, NullPointerException.class
			}, {
				"manager1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}



}
