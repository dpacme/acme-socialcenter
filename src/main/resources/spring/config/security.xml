<?xml version="1.0" encoding="UTF-8"?>

<!-- 
 * security.xml
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 -->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:security="http://www.springframework.org/schema/security"	
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd		
        http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.2.xsd
    ">

	<!-- Security infrastructure -->

	<bean id="loginService" class="security.LoginService" />

	<bean id="passwordEncoder"
		class="org.springframework.security.authentication.encoding.Md5PasswordEncoder" />

	<!-- Access control -->

	<security:http auto-config="true" use-expressions="true">
		<security:intercept-url pattern="/" access="permitAll" /> 

		<security:intercept-url pattern="/favicon.ico" access="permitAll" /> 
		<security:intercept-url pattern="/images/**" access="permitAll" /> 
		<security:intercept-url pattern="/scripts/**" access="permitAll" /> 
		<security:intercept-url pattern="/styles/**" access="permitAll" /> 

		<security:intercept-url pattern="/views/misc/index.jsp" access="permitAll" /> 

		<security:intercept-url pattern="/security/login.do" access="permitAll" /> 
		<security:intercept-url pattern="/security/loginFailure.do" access="permitAll" /> 

		<security:intercept-url pattern="/welcome/index.do" access="permitAll" /> 
		
		<security:intercept-url pattern="/penaltyParameter/administrator/**" access="hasRole('ADMINISTRATOR')" />
		
		<!-- Social Center -->
		
		<security:intercept-url pattern="/socialCenter/all.do" access="permitAll" /> 
		<security:intercept-url pattern="/socialCenter/showDisplay.do" access="permitAll" /> 
		<security:intercept-url pattern="/socialCenter/search.do" access="permitAll" /> 
		<security:intercept-url pattern="/socialCenter/manager/**" access="hasRole('MANAGER')" /> 

		<security:intercept-url pattern="/socialCenter/administrator/**" access="hasRole('ADMINISTRATOR')" />
		
		<!-- Reservation -->

		<security:intercept-url pattern="/reservation/client/listByClient.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/reservation/client/**" access="hasRole('CLIENT')" />
		<security:intercept-url pattern="/reservation/manager/**" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/reservation/showDisplay.do" access="hasRole('CLIENT') || hasRole('MANAGER')" />
		
		<!-- Resource -->

		<security:intercept-url pattern="/resource/**" access="permitAll" />
		<security:intercept-url pattern="/resource/manager/loanResource.do" access="hasRole('MANAGER')" /> 
		<security:intercept-url pattern="/resource/client/**" access="hasRole('CLIENT')" /> 
		<security:intercept-url pattern="/comment/client/**" access="hasRole('CLIENT')" /> 

		<!-- Request -->
		
		<security:intercept-url pattern="/request/client/listByClient.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/request/manager/**" access="hasRole('MANAGER')" /> 
		<security:intercept-url pattern="/request/client/**" access="hasRole('CLIENT')" /> 
		
		<!-- Assessment -->
		
		<security:intercept-url pattern="/assessment/client/**" access="hasRole('CLIENT')" /> 
		
		<!-- Activity -->
		
		<security:intercept-url pattern="/activity/list.do" access="hasAnyRole('CLIENT','MONITOR')" />
		<security:intercept-url pattern="/activity/client/list.do" access="hasAnyRole('CLIENT','MONITOR')" />   
		<security:intercept-url pattern="/activity/listActivities.do" access="hasAnyRole('CLIENT','MONITOR')" /> 
		<security:intercept-url pattern="/activity/client/apply.do" access="hasRole('CLIENT')" /> 
		<security:intercept-url pattern="/activity/client/unapply.do" access="hasRole('CLIENT')" /> 
		<security:intercept-url pattern="/activity/monitor/myActivities.do" access="hasRole('MONITOR')" />
		<security:intercept-url pattern="/activity/monitor/create.do" access="hasRole('MONITOR')" />
		<security:intercept-url pattern="/activity/monitor/edit.do" access="hasRole('MONITOR')" />
		
		<!-- Activity Reservation-->
		
		<security:intercept-url pattern="/activity/activityReservation/show.do" access="hasAnyRole('CLIENT','MONITOR')" />  
		
		<!-- Administrator -->
		
		<security:intercept-url pattern="/administrator/**" access="hasRole('ADMINISTRATOR')" /> 
		
		<!-- Penalty -->
		
		<security:intercept-url pattern="/penalty/list.do" access="hasRole('CLIENT')" /> 
		<security:intercept-url pattern="/penalty/listByClient.do" access="hasRole('MANAGER')" /> 
		<security:intercept-url pattern="/penalty/manager/edit.do" access="hasRole('MANAGER')" /> 

		<!-- Client -->
		
		<security:intercept-url pattern="/client/create.do" access="isAnonymous()" /> 
		<security:intercept-url pattern="/client/edit.do" access="hasRole('CLIENT')" /> 
		<security:intercept-url pattern="/client/list.do" access="hasAnyRole('CLIENT','MANAGER')" /> 
		<security:intercept-url pattern="/client/listClients.do" access="hasAnyRole('CLIENT','MONITOR')" /> 
		
		<!-- Manager -->
		
		<security:intercept-url pattern="/managerUri/edit.do" access="hasRole('MANAGER')" /> 
		<security:intercept-url pattern="/managerUri/administrator/create.do" access="hasRole('ADMINISTRATOR')" />
		<security:intercept-url pattern="/category/manager/**" access="hasRole('MANAGER')" /> 
		
		<!-- Monitor -->
		
		<security:intercept-url pattern="/monitor/edit.do" access="hasRole('MONITOR')" /> 
		<security:intercept-url pattern="/monitor/show.do" access="permitAll" />
		<security:intercept-url pattern="/monitor/create.do" access="isAnonymous()" /> 
		
		<!-- Room -->
		
		<security:intercept-url pattern="/room/manager/**" access="hasRole('MANAGER')" /> 
		
		<!-- Incidence -->
		
		<security:intercept-url pattern="/incidence/client/listByClient.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/incidence/client/**" access="hasRole('CLIENT')" />  
		<security:intercept-url pattern="/incidence/showDisplay.do" access="hasRole('CLIENT') || hasRole('MANAGER')"/> 
		<security:intercept-url pattern="/incidence/manager/**" access="hasRole('MANAGER')"/> 
		
		<!-- Message -->
		
		<security:intercept-url pattern="/incidence/sendMessage.do" access="hasRole('CLIENT') || hasRole('MANAGER')"/>
		
		<!-- Book -->
		
		<security:intercept-url pattern="/book/manager/**" access="hasRole('MANAGER')" /> 
		
		<!-- Electronic device -->
		
		<security:intercept-url pattern="/electronicDevice/manager/**" access="hasRole('MANAGER')" /> 
		
		<!-- Essentials -->

		<security:intercept-url pattern="/misc/terms.do"
			access="permitAll" />
		<security:intercept-url pattern="/misc/aboutUs.do"
			access="permitAll" />
	
		<security:intercept-url pattern="/**" access="hasRole('NONE')" />

		<security:form-login 
			login-page="/security/login.do"
			password-parameter="password" 
			username-parameter="username"
			authentication-failure-url="/security/loginFailure.do" />

		<security:logout 
			logout-success-url="/" 
			invalidate-session="true" />
	</security:http>

</beans>