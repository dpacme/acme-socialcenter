
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Assessment;
import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;

@Repository
public interface SocialCenterRepository extends JpaRepository<SocialCenter, Integer> {

	@Query("select b from Book b where b.socialCenter.id=?1")
	Collection<Book> findBooksResources(Integer socialCenterId);

	@Query("select b from ElectronicDevice b where b.socialCenter.id=?1")
	Collection<ElectronicDevice> findElectronicDevicesResources(int id);

	@Query("select b from Room b where b.socialCenter.id=?1")
	Collection<Room> findRoomsResources(int id);

	@Query("select s from SocialCenter s order by (select count(l) from Like l join l.assessment as2 join as2.socialCenter sc where sc.id=s.id and l.likeMe=true)/(select count(l2) from Like l2 join l2.assessment as3 join as3.socialCenter sc2 where sc2.id=s.id)")
	Collection<SocialCenter> findAllOrderByRatioLikesDislikes();

	@Query("select a from Assessment a where a.client.banned=false and a.socialCenter.id = ?1 order by (select count(l) from Like l join l.assessment as2 where as2.id=a.id and l.likeMe=true)/(select count(l2) from Like l2 join l2.assessment as3 where as3.id=a.id)")
	Collection<Assessment> getAssessmentsWithoutClientBanned(Integer socialCenterId);

	@Query("select c from SocialCenter c order by c.resources.size desc")
	Collection<SocialCenter> getSocialCentersWithResources();

	@Query("select a from SocialCenter a where a.name like ?1")
	Collection<SocialCenter> getSocialCentersByKeyword(String keyword);

	@Query("select s from SocialCenter s where (select count(ass) from SocialCenter s2 join s2.assessments ass where s2.id=s.id) >= all(select count(ass2) from SocialCenter s3 join s3.assessments ass2 group by s3)")
	Collection<SocialCenter> getSocialCenterMoreValorate();

}
