
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	@Query("select count(r) from Request r where r.status = 'rejected'")
	Double rejectedPerSocialCenter();

	@Query("select count(r) from Request r where r.status = 'accepted'")
	Double acceptedPerSocialCenter();

}
