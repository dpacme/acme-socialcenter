
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Monitor;
import security.UserAccount;

@Repository
public interface MonitorRepository extends JpaRepository<Monitor, Integer> {

	@Query("select a from Monitor a where a.userAccount = ?1")
	Monitor findByUserAccount(UserAccount userAccount);
}
