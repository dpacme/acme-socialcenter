
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Penalty;

@Repository
public interface PenaltyRepository extends JpaRepository<Penalty, Integer> {


}
