
package repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Resource;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, Integer> {

	@Query("select ed from ElectronicDevice ed where (?2 is null or ed.socialCenter.id = ?2) and (ed.brand like ?1 or ed.model like ?1 or ed.ticker like ?1) ")
	Collection<Resource> getElectronicDevicesByKeyword(String keyword, Integer socialCenterId);

	@Query("select b from Book b where (?2 is null or b.socialCenter.id = ?2) and (b.title like ?1 or b.author like ?1 or b.description like ?1 or b.ticker like ?1)")
	Collection<Resource> getBooksByKeyword(String keyword, Integer socialCenterId);

	@Query("select r from Room r where (?2 is null or r.socialCenter.id = ?2) and (r.ticker like ?1)")
	Collection<Resource> getRoomByTicker(String keyword, Integer socialCenterId);

	@Query("select size(r.reservations) from Resource r join r.reservations re where re.returnDate = null and r.id = ?1")
	Integer resourceIsAvailable(Integer resourceId);

	@Query("select r from Resource r join r.reservations re where re.client.id=?1 and re.returnDate is not null and re.returnDate <= current_date")
	Collection<Resource> getResourcesWithReservationsUsedByClient(Integer clientId);

	@Query("select r from Resource r order by r.reservations.size")
	Collection<Resource> getResourcesOrderByNumReservations();

	@Query("select count(re.size) from SocialCenter c join c.resources r join r.reservations re group by c.id")
	List<Long> getCountReservationsForSocialCenter();

	@Query("select count(re.size) from SocialCenter c join c.resources r join r.reservations re where re.returnDate=null group by c.id")
	List<Long> getCountPendingReservationsForSocialCenter();

	@Query("select r from Comment c join c.resource r group by r order by c.score desc")
	List<Resource> orderedResources();

}
