
package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

	@Query("select r from Reservation r where r.client.id=?1 and r.resource.id=?2")
	Reservation findReservation(Integer clientId, Integer resourceId);

	@Query("select r from Reservation r where (?1 is null or r.client.id=?1) and r.returnDate = null")
	Collection<Reservation> activeReservation(Integer clientId);

	@Query("select r from Reservation r where (?1 is null or r.client.id=?1) and r.returnDate != null")
	Collection<Reservation> inactiveReservation(Integer clientId);

	@Query("select r from Reservation r where r.startDate = ?1 and r.resource.id = ?2")
	Collection<Reservation> findReservationByDate(Date date, int resourceId);

	@Query("select r from Reservation r where r.returnDate = null")
	Collection<Reservation> allActiveReservation();

	@Query("select r from Reservation r where r.resource.socialCenter.id = ?1 and r.returnDate = null")
	Collection<Reservation> activeReservationBySocialCenter(int socialCenterId);

	@Query("select r from Reservation r where r.resource.socialCenter.id = ?1")
	Collection<Reservation> allReservationBySocialCenter(int socialCenterId);


}
