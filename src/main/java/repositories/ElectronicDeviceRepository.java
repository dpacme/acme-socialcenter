
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.ElectronicDevice;

@Repository
public interface ElectronicDeviceRepository extends JpaRepository<ElectronicDevice, Integer> {


}
