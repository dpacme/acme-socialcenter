
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Assessment;
import domain.Like;

@Repository
public interface AssessmentRepository extends JpaRepository<Assessment, Integer> {

	@Query("select l from Like l where l.assessment.id = ?1 and l.client.id=?2")
	Like getLikeFromAssessmentAndClient(Integer assessmentId, Integer clientId);

}
