
package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Activity;
import domain.Client;
import domain.Penalty;
import security.UserAccount;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

	@Query("select a from Client a where a.userAccount = ?1")
	Client findByUserAccount(UserAccount userAccount);

	@Query("select c from Client c join c.reservations r where r.startDate <= current_date and r.endDate > current_date and (r.returnDate is null or r.returnDate > current_date) group by c.id order by r.size desc")
	Collection<Client> getClientsOrderByReservations();

	@Query("select c from Client c where ?1 IN(c.activities)")
	Collection<Client> getClientsByActivity(Activity activity);

	@Query("select count(c) from Client c where c.banned = 1")
	Integer numClientsBanned();

	@Query("select c from Client c where c.penalties.size = (select max(c.penalties.size) from Client c)")
	Collection<Client> getClientWithMorePenalties();

	@Query("select c from Client c where c.banned = true")
	Collection<Client> findBannedClients();

	@Query("select p from Penalty p where p.endDate > ?1 and p.client.id = ?2")
	Collection<Penalty> hasPenalty(Date date, Integer clientId);

}
