
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {


	@Query("select a from Activity a where (0.5*a.seats) < a.clients.size")
	Collection<Activity> listActivitiesHaveMore50CapacityOccupied();

	@Query("select a from Activity a where a.endDate < CURRENT_DATE order by a.monitor.id")
	Collection<Activity> findFinishedActivitiesPerMonitor();

	@Query("select a from Activity a where a.startDate > CURRENT_DATE order by a.monitor.id")
	Collection<Activity> findNotStartedActivitiesPerMonitor();


}