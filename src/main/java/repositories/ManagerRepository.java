
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Manager;
import security.UserAccount;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	@Query("select a from Manager a where a.userAccount = ?1")
	Manager findByUserAccount(UserAccount userAccount);

	@Query("select m from Manager m where (m.socialCenter = null) or (m.socialCenter.id=?1)")
	Collection<Manager> managerWithoutSocialCenter(int socialCenterId);
}
