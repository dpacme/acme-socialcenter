
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

	@Query("select b from Book b  where MONTH(b.publicationDate) >= MONTH(CURRENT_DATE) -1 and YEAR(b.publicationDate) >= YEAR(CURRENT_DATE)")
	Collection<Book> getBooksPublishedLastMonth();
}
