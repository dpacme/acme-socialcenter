/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Activity;
import domain.ActivityReservation;
import domain.Client;
import domain.Monitor;

import services.ActivityReservationService;
import services.ActivityService;
import services.ClientService;
import services.MonitorService;


@Controller
@RequestMapping("/activityReservation")
public class ActivityReservationController extends AbstractController {

	// Services

	@Autowired
	private ActivityService		activityService;

	@Autowired
	private ActivityReservationService		activityReservationService;
	
	@Autowired
	private MonitorService		monitorService;



	// Constructors -----------------------------------------------------------

	public ActivityReservationController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final Integer activityReservationId) {
		ModelAndView result;
		final ActivityReservation activityReservation = activityReservationService.findOne(activityReservationId);

		result = new ModelAndView("activityReservation/show");
		result.addObject("activityReservation", activityReservation);

		return result;
	}
	
	
	
	


}
