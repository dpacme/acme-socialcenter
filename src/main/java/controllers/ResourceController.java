
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Book;
import domain.Client;
import domain.ElectronicDevice;
import domain.Resource;
import domain.Room;
import domain.SocialCenter;
import services.BookService;
import services.ClientService;
import services.ElectronicDeviceService;
import services.ReservationService;
import services.ResourceService;
import services.RoomService;
import services.SocialCenterService;

@Controller
@RequestMapping("/resource")
public class ResourceController extends AbstractController {

	// Services
	@Autowired
	private ResourceService			resourceService;

	@Autowired
	private BookService				bookService;

	@Autowired
	private ElectronicDeviceService	electronicDeviceService;

	@Autowired
	private RoomService				roomService;

	@Autowired
	private ClientService			clientService;

	@Autowired
	private SocialCenterService		socialCenterService;

	@Autowired
	private ReservationService		reservationService;


	// Constructors -----------------------------------------------------------

	public ResourceController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		Collection<Resource> resources = resourceService.findAll();
		List<Resource> aux = resourceService.orderedResources();
		List<Resource> orderedResources = new ArrayList<Resource>();

		if (aux.size() > 5)
			for (int i = 0; i <= 5; i++)
				orderedResources.add(aux.get(i));
		else
			orderedResources = aux;

		result = new ModelAndView("resource/list");
		result.addObject("resources", resources);
		result.addObject("orderedResources", orderedResources);
		result.addObject("requestURI", "resource/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(String keyword, Integer socialCenterId, @RequestParam(required = false) Boolean loanResources) {
		return searchResources(keyword, socialCenterId, loanResources);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(String keyword, Integer socialCenterId, @RequestParam(required = false) Boolean loanResources) {
		return searchResources(keyword, socialCenterId, loanResources);
	}

	private ModelAndView searchResources(String keyword, Integer socialCenterId, Boolean loanResources) {
		ModelAndView result;
		final Collection<Resource> res;

		res = resourceService.searchResources(keyword, socialCenterId, loanResources);

		result = new ModelAndView("resource/list");
		result.addObject("resources", res);
		result.addObject("requestURI", "resource/search.do");
		result.addObject("create", false);
		if (loanResources != null && loanResources) {
			result.addObject("loanResources", true);
		}
		if (socialCenterId != null) {
			SocialCenter socialCenter = this.socialCenterService.findOne(socialCenterId);
			result.addObject("socialCenter", socialCenter);
		}

		return result;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int resourceId, @RequestParam(required = false) Boolean used) {
		ModelAndView result;

		Resource resource = resourceService.findOne(resourceId);
		Client client;
		try {
			client = clientService.findByPrincipal();
		} catch (Exception e) {
			client = null;
		}

		Book book = null;
		ElectronicDevice electronicDevice;
		Room room = null;

		String name = resource.getClass().getName();

		result = new ModelAndView("resource/showDisplay");
		result.addObject("resource", resource);
		if (used != null && used == true)
			result.addObject("used", used);
		else
			result.addObject("used", false);

		result.addObject("requestURI", "resource/showDisplay.do");

		if (client != null) {
			if (clientService.hasPenalty(client.getId())) {
				result.addObject("penalty", true);
			} else if (reservationService.activeReservations(client.getId()).size() >= 3) {
				result.addObject("limited", true);
			} else if (!reservationService.timeIsCorrect()) {
				result.addObject("timeIsCorrect", false);
			} else
				result.addObject("isAvailable", resourceService.resourceIsAvailable(resourceId));
		}

		try {
			if (name.equals("domain.Book")) {
				book = bookService.findOne(resourceId);
				result.addObject("isBook", true);
				result.addObject("comments", book.getComments());
			} else if (name.equals("domain.ElectronicDevice")) {
				electronicDevice = electronicDeviceService.findOne(resourceId);
				result.addObject("isElectronicDevice", true);
				result.addObject("comments", electronicDevice.getComments());
			} else if (name.equals("domain.Room")) {
				room = roomService.findOne(resourceId);
				result.addObject("isRoom", true);
				result.addObject("comments", room.getComments());
			}

		} catch (Exception e) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

}
