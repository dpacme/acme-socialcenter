
package controllers.monitor;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Monitor;
import forms.MonitorForm;
import security.UserAccountService;
import services.MonitorService;

@Controller
@RequestMapping("/monitor")
public class MonitorController extends AbstractController {

	// Services
	@Autowired
	private MonitorService monitorService;

	@Autowired
	private UserAccountService	userAccountService;


	// Constructors -----------------------------------------------------------

	public MonitorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Monitor monitor = this.monitorService.findByPrincipal();

		res = this.createEditModelAndView(monitor);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Monitor monitor, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(monitor);
			if (!StringUtils.isEmpty(monitor.getPostalAddress()) && !monitor.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(monitor.getPhone()) && !monitor.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
			System.out.println(binding.getAllErrors());
		} else
			try {

				List<String> errores = this.monitorService.comprobacionEditarListErrores(monitor);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createEditModelAndView(monitor);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.monitorService.save(monitor);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(monitor);
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final Integer monitorId) {
		ModelAndView result;
		final Monitor monitor = this.monitorService.findOne(monitorId);

		result = new ModelAndView("monitor/show");
		result.addObject("requestURI", "monitor/show.do");
		result.addObject("monitor", monitor);

		return result;
	}

	// (REGISTRO) Creaci�n de un client
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final MonitorForm monitorForm = new MonitorForm();

		res = this.createFormModelAndView(monitorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final MonitorForm monitorForm, final BindingResult binding) {
		ModelAndView res;
		Monitor monitor;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(monitorForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(monitorForm.getPostalAddress()) && !monitorForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(monitorForm.getPhone()) && !monitorForm.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				monitor = this.monitorService.reconstruct(monitorForm);

				final List<String> errores = this.monitorService.comprobacionEditarListErrores(monitor);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(monitorForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.monitorService.saveForm(monitor);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(monitorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					if (this.userAccountService.findByUsername(monitorForm.getUsername()) != null)
						res.addObject("duplicate", "duplicate");
					else
						res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("pass", "pass");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createEditModelAndView(Monitor monitor) {
		ModelAndView res;

		res = this.createEditModelAndView(monitor, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Monitor monitor, final String message) {
		ModelAndView res;

		res = new ModelAndView("monitor/edit");
		res.addObject("monitor", monitor);
		res.addObject("message", message);

		return res;
	}

	protected ModelAndView createFormModelAndView(MonitorForm monitorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(monitorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(MonitorForm monitorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("monitor/create");
		res.addObject("monitorForm", monitorForm);
		res.addObject("message", message);

		return res;
	}

}
