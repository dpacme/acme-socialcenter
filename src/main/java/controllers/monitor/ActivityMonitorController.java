/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.monitor;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import services.ActivityReservationService;
import services.ActivityService;
import services.MonitorService;


@Controller
@RequestMapping("/activity/monitor")
public class ActivityMonitorController extends AbstractController {

	// Services

	@Autowired
	private MonitorService		monitorService;

	@Autowired
	private ActivityService	activityService;

	@Autowired
	private ActivityReservationService	activityReservationService;



	// Constructors -----------------------------------------------------------

	public ActivityMonitorController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/myActivities", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Activity> activities;
		activities = this.monitorService.findByPrincipal().getActivities();
		result = new ModelAndView("activity/monitor/myActivities");
		result.addObject("activities", activities);
		result.addObject("requestURI", "activity/monitor/myActivities.do");

		return result;
	}

	// Creaci�n de una activity
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final Activity activity = this.activityService.create();

		res = this.createModelAndView(activity);
		return res;
	}

	// Guardar en la base de datos la nueva activity
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Activity activity, final BindingResult binding) {
		ModelAndView res;
		Date date = new Date();
		if (binding.hasErrors()) {
			res = this.createModelAndView(activity);
			if(activity.getEndDate()!=null && activity.getStartDate()!=null)
				if (activity.getEndDate().before(activity.getStartDate()) || activity.getEndDate().before(date) || activity.getStartDate().before(date))
					res.addObject("errorDate", "errorDate");
			System.out.println(binding.getAllErrors());
		} else
			try {
				res = this.createModelAndView(activity);
				if (activity.getEndDate().before(activity.getStartDate()) || activity.getEndDate().before(date) || activity.getStartDate().before(date)) {
					res.addObject("errorDate", "errorDate");
					System.out.println(binding.getAllErrors());
				} else {
					this.activityService.save(activity);
					res = new ModelAndView("redirect:/activity/monitor/myActivities.do");
				}

			} catch (final Throwable oops) {
				res = this.createModelAndView(activity);
				if (oops.getLocalizedMessage().contains("La fecha final debe ser posterior a la de comienzo"))
					res.addObject("errorDate", "errorDate");
				res.addObject("error", "error");
			}

		return res;
	}

	// Edicion de una activity
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final Integer activityId) {
		ModelAndView res;
		try {
			final Activity activity = this.activityService.findOne(activityId);
			Assert.isTrue(activity.getMonitor().getId() == this.monitorService.findByPrincipal().getId());
			res = this.editModelAndView(activity);
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Activity activity, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(activity);
			//Errores no gestionados por binding
			if (activity.getEndDate() != null && activity.getStartDate() != null)
				if (!activity.getEndDate().after(activity.getStartDate()) || !activity.getEndDate().after(new Date()))
					System.out.println(binding.getAllErrors());
		} else

			try {
				this.activityService.save(activity);
				res = new ModelAndView("redirect:/activity/monitor/myActivities.do");
			} catch (final Throwable oops) {
				res = this.editModelAndView(activity);
				if (oops.getLocalizedMessage().contains("La fecha final debe ser posterior a la de comienzo"))
					res.addObject("errorDate", "errorDate");
				res.addObject("error", "error");
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid Activity activity) {
		ModelAndView res;
		try {
			Assert.isTrue(activity.getClients().isEmpty());
			if(activity.getActivityReservation()!=null)
				this.activityReservationService.delete(activity.getActivityReservation());
			this.activityService.delete(activity);
			res = new ModelAndView("redirect:/activity/list.do");
		} catch (final Throwable oops) {
			res = this.editModelAndView(activity);
			res.addObject("error", "error");
			res.addObject("errorClients", "errorClients");
		}
		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createModelAndView(final Activity activity) {
		ModelAndView res;

		res = this.createModelAndView(activity, null);

		return res;
	}

	protected ModelAndView createModelAndView(final Activity activity, final String message) {
		ModelAndView res;

		res = new ModelAndView("activity/monitor/create");
		res.addObject("activity", activity);
		res.addObject("message", message);
		res.addObject("requestURI", "activity/monitor/create.do");

		return res;
	}

	protected ModelAndView editModelAndView(final Activity activity) {
		ModelAndView res;

		res = this.editModelAndView(activity, null);

		return res;
	}

	protected ModelAndView editModelAndView(final Activity activity, final String message) {
		ModelAndView res;

		res = new ModelAndView("activity/monitor/edit");
		res.addObject("activity", activity);
		res.addObject("message", message);
		res.addObject("requestURI", "activity/monitor/edit.do");

		return res;
	}

}
