
package controllers.administrator;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import domain.Administrator;
import domain.Book;
import domain.Client;
import domain.Resource;
import domain.SocialCenter;
import services.ActivityService;
import services.AdministratorService;
import services.BookService;
import services.ClientService;
import services.RequestService;
import services.ResourceService;
import services.SocialCenterService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services
	@Autowired
	private AdministratorService administratorService;

	@Autowired
	private SocialCenterService		socialCenterService;

	@Autowired
	private ResourceService			resourceService;

	@Autowired
	private ClientService			clientService;

	@Autowired
	private BookService				bookService;

	@Autowired
	private ActivityService			activityService;

	@Autowired
	private RequestService			requestService;


	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Administrator administrator = this.administratorService.findByPrincipal();

		res = this.createEditModelAndView(administrator);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Administrator administrator, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(administrator);
			if (!StringUtils.isEmpty(administrator.getPostalAddress()) && !administrator.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(administrator.getPhone()) && !administrator.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
			System.out.println(binding.getAllErrors());
		} else
			try {

				List<String> errores = this.administratorService.comprobacionEditarListErrores(administrator);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createEditModelAndView(administrator);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.administratorService.save(administrator);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(administrator);
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Dashboard for administrator

	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result;

		//Lista de los centros sociales ordenados por el n�mero de recursos del que dispone descendentemente
		Collection<SocialCenter> getSocialCentersWithResources = this.socialCenterService.getSocialCentersWithResources();

		//M�nimo, m�ximo y media del n�mero de reservas por centro social

		Collection<Object> getMaxMinAvgReservationsForSocialCenter = this.resourceService.getMaxMinAvgReservationsForSocialCenter();

		Long minReservationsForSocialCenter = (long) 0;
		Long maxReservationsForSocialCenter = (long) 0;
		Double avgReservationsForSocialCenter = 0.0;
		Integer aux = 0;
		for (final Object o : getMaxMinAvgReservationsForSocialCenter)
			try {

				if (o != null && aux == 0)
					maxReservationsForSocialCenter = (long) o;
				if (o != null && aux == 1)
					minReservationsForSocialCenter = (long) o;
				if (o != null && aux == 2)
					avgReservationsForSocialCenter = (double) o;
				aux++;

			} catch (final Exception e) {
				e.printStackTrace();
			}

		//M�nimo, m�ximo y media del n�mero de reservas pendientes de entrega por centro social

		Collection<Object> getMaxMinAvgPendingReservationsForSocialCenter = this.resourceService.getMaxMinAvgPendingReservationsForSocialCenter();

		Long minPendingReservationsForSocialCenter = (long) 0;
		Long maxPendingReservationsForSocialCenter = (long) 0;
		Double avgPendingReservationsForSocialCenter = 0.0;
		Integer aux2 = 0;
		for (final Object o : getMaxMinAvgPendingReservationsForSocialCenter)
			try {

				if (o != null && aux2 == 0)
					maxPendingReservationsForSocialCenter = (long) o;
				if (o != null && aux2 == 1)
					minPendingReservationsForSocialCenter = (long) o;
				if (o != null && aux2 == 2)
					avgPendingReservationsForSocialCenter = (double) o;
				aux2++;

			} catch (final Exception e) {
				e.printStackTrace();
			}


		//Lista de los clientes ordenado por el n�mero de reservas vigentes

		Collection<Client> getClientsOrderByReservations = this.clientService.getClientsOrderByReservations();

		//Lista de recursos ordenada por el n�mero de reservas que se han hecho de �l

		Collection<Resource> getResourcesOrderByNumReservations = this.resourceService.getResourcesOrderByNumReservations();

		//Lista de los libros publicados en el �ltimo mes

		Collection<Book> getBooksPublishedLastMonth = this.bookService.getBooksPublishedLastMonth();

		//N�mero de clientes bloqueados actualmente en el sistema

		Integer numClientsBanned = this.clientService.numClientsBanned();

		//Lista de clientes bloqueados actualmente en el sistema

		Collection<Client> findBannedClients = this.clientService.findBannedClients();

		//M�nimo, m�ximo y media del n�mero de actividades por mes de los �ltimos seis meses por monitor

		Integer minNumActivitiesLastSixMonthPerMonitor = this.activityService.getMinActivitiesPerMonthLastSixMonthsPerMonitorAux();
		Integer maxNumActivitiesLastSixMonthPerMonitor = this.activityService.getMaxActivitiesPerMonthLastSixMonthsPerMonitorAux();
		Double avgNumActivitiesLastSixMonthPerMonitor = this.activityService.getAvgActivitiesPerMonthLastSixMonthsPerMonitorAux();

		//Media de peticiones aceptadas y denegadas por centro social

		Double rejectedPerSocialCenter = this.requestService.rejectedPerSocialCenter();
		Double acceptedPerSocialCenter = this.requestService.acceptedPerSocialCenter();


		//Lista de actividades finalizadas por monitor

		Collection<Activity> findFinishedActivitiesPerMonitor = this.activityService.findFinishedActivitiesPerMonitor();

		//Lista de actividades sin empezar por monitor

		Collection<Activity> findNotStartedActivitiesPerMonitor = this.activityService.findNotStartedActivitiesPerMonitor();

		//El centro social m�s valorado
		Collection<SocialCenter> getSocialCenterMoreValorate = this.socialCenterService.getSocialCenterMoreValorate();

		//Lista de actividades que tengan m�s del 50% de su capacidad ocupada

		Collection<Activity> listActivitiesHaveMore50CapacityOccupied = this.activityService.listActivitiesHaveMore50CapacityOccupied();

		//El cliente que tenga m�s penalizaciones

		Collection<Client> getClientWithMorePenalties = this.clientService.getClientWithMorePenalties();

		//*************************************************************************//
		result = new ModelAndView("administrator/dashboard");

		result.addObject("getSocialCentersWithResources", getSocialCentersWithResources);
		result.addObject("minReservationsForSocialCenter", minReservationsForSocialCenter);
		result.addObject("maxReservationsForSocialCenter", maxReservationsForSocialCenter);
		result.addObject("avgReservationsForSocialCenter", avgReservationsForSocialCenter);

		result.addObject("minPendingReservationsForSocialCenter", minPendingReservationsForSocialCenter);
		result.addObject("maxPendingReservationsForSocialCenter", maxPendingReservationsForSocialCenter);
		result.addObject("avgPendingReservationsForSocialCenter", avgPendingReservationsForSocialCenter);

		result.addObject("minNumActivitiesLastSixMonthPerMonitor", minNumActivitiesLastSixMonthPerMonitor);
		result.addObject("maxNumActivitiesLastSixMonthPerMonitor", maxNumActivitiesLastSixMonthPerMonitor);
		result.addObject("avgNumActivitiesLastSixMonthPerMonitor", avgNumActivitiesLastSixMonthPerMonitor);

		result.addObject("avgAcceptedRequestForSocialCenter", acceptedPerSocialCenter);
		result.addObject("avgRejectedRequestForSocialCenter", rejectedPerSocialCenter);

		result.addObject("listClientsOrderByNumberCurrentReserves", getClientsOrderByReservations);
		result.addObject("getResourcesOrderByNumReservations", getResourcesOrderByNumReservations);
		result.addObject("getBooksPublishedLastMonth", getBooksPublishedLastMonth);
		result.addObject("numClientsBanned", numClientsBanned);
		result.addObject("findBannedClients", findBannedClients);
		result.addObject("activitiesCompletedByMonitor", findFinishedActivitiesPerMonitor);
		result.addObject("activitiesWithoutBeginningByMonitor", findNotStartedActivitiesPerMonitor);
		result.addObject("getSocialCenterMoreValorate", getSocialCenterMoreValorate);
		result.addObject("listActivitiesMoreFiftyPercentTheirCapacityOccupied", listActivitiesHaveMore50CapacityOccupied);
		result.addObject("clientMorePenalties", getClientWithMorePenalties);


		result.addObject("requestURI", "administrator/dashboard.do");

		return result;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createEditModelAndView(Administrator administrator) {
		ModelAndView res;

		res = this.createEditModelAndView(administrator, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Administrator administrator, final String message) {
		ModelAndView res;

		res = new ModelAndView("administrator/edit");
		res.addObject("administrator", administrator);
		res.addObject("message", message);

		return res;
	}

}
