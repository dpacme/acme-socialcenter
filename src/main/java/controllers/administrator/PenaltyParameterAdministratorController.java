
package controllers.administrator;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.PenaltyParameter;
import services.PenaltyParameterService;

@Controller
@RequestMapping("/penaltyParameter/administrator")
public class PenaltyParameterAdministratorController extends AbstractController {

	// Services

	@Autowired
	private PenaltyParameterService penaltyParameterService;


	// Constructors -----------------------------------------------------------

	public PenaltyParameterAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay() {
		ModelAndView result;

		List<PenaltyParameter> penaltyParameters = (List<PenaltyParameter>) penaltyParameterService.findAll();

		result = new ModelAndView("penaltyParameter/showDisplay");
		result.addObject("penaltyParameter", penaltyParameters.get(0));

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;

		List<PenaltyParameter> penaltyParameters = (List<PenaltyParameter>) penaltyParameterService.findAll();

		res = this.createFormModelAndView(penaltyParameters.get(0));
		return res;
	}

	// (REGISTRO) Guardar en la base de datos
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid PenaltyParameter penaltyParameter, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(penaltyParameter);
			System.out.println(binding.getAllErrors());
			if (penaltyParameter.getDays() == null)
				res.addObject("days", "null");
		} else
			try {
				penaltyParameterService.save(penaltyParameter);
				res = new ModelAndView("redirect:/penaltyParameter/administrator/showDisplay.do");
			} catch (Throwable oops) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(PenaltyParameter penaltyParameter) {
		ModelAndView res;

		res = this.createFormModelAndView(penaltyParameter, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(PenaltyParameter penaltyParameter, String message) {
		ModelAndView res;

		res = new ModelAndView("penaltyParameter/edit");
		res.addObject("penaltyParameter", penaltyParameter);
		res.addObject("message", message);
		res.addObject("requestURI", "penaltyParameter/administrator/edit.do");

		return res;
	}

}
