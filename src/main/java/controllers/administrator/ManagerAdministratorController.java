
package controllers.administrator;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import forms.ManagerForm;
import security.UserAccountService;
import services.ManagerService;

@Controller
@RequestMapping("/managerUri/administrator")
public class ManagerAdministratorController extends AbstractController {

	// Services

	@Autowired
	private ManagerService			managerService;

	@Autowired
	private UserAccountService	userAccountService;


	// Constructors -----------------------------------------------------------

	public ManagerAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un client
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ManagerForm managerForm = new ManagerForm();

		res = this.createFormModelAndView(managerForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid ManagerForm managerForm, BindingResult binding) {
		ModelAndView res;
		Manager manager;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(managerForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(managerForm.getPostalAddress()) && !managerForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(managerForm.getPhone()) && !managerForm.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				manager = this.managerService.reconstruct(managerForm);

				List<String> errores = this.managerService.comprobacionEditarListErrores(manager);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(managerForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.managerService.saveForm(manager);
					res = new ModelAndView("redirect:/");
				}

			} catch (Throwable oops) {
				res = this.createFormModelAndView(managerForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					if (this.userAccountService.findByUsername(managerForm.getUsername()) != null)
						res.addObject("duplicate", "duplicate");
					else
						res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("pass", "pass");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(ManagerForm managerForm) {
		ModelAndView res;

		res = this.createFormModelAndView(managerForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(ManagerForm managerForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/administrator/create");
		res.addObject("managerForm", managerForm);
		res.addObject("message", message);

		return res;
	}

}
