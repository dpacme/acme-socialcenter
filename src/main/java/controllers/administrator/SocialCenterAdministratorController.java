
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import domain.Resource;
import domain.SocialCenter;
import services.ManagerService;
import services.SocialCenterService;

@Controller
@RequestMapping("/socialCenter/administrator")
public class SocialCenterAdministratorController extends AbstractController {

	// Services

	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private ManagerService		managerService;


	// Constructors -----------------------------------------------------------

	public SocialCenterAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		try {
			SocialCenter socialCenter = socialCenterService.create();

			result = this.createModelAndView(socialCenter);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid SocialCenter socialCenter, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(socialCenter);
			System.out.println(binding.getAllErrors());
		} else
			try {
				if (socialCenter.getManagers() == null || socialCenter.getManagers().isEmpty()) {
					res = this.createModelAndView(socialCenter);
					res.addObject("managerNotNull", "managerNotNull");
				}
				SocialCenter socialCenter2 = socialCenterService.saveAndFlush(socialCenter);
				res = new ModelAndView("redirect:/socialCenter/showDisplay.do?socialCenterId=" + socialCenter2.getId());

			} catch (final Throwable oops) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int socialCenterId) {
		ModelAndView result;

		try {
			SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);
			result = editModelAndView(socialCenter);
		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid SocialCenter socialCenter, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(socialCenter);
			System.out.println(binding.getAllErrors());

		} else
			try {
				socialCenterService.save(socialCenter);
				res = new ModelAndView("redirect:/socialCenter/showDisplay.do?socialCenterId=" + socialCenter.getId());
			} catch (Throwable oops) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createModelAndView(SocialCenter socialCenter) {
		ModelAndView res;

		res = this.createModelAndView(socialCenter, null);

		return res;
	}

	protected ModelAndView createModelAndView(SocialCenter socialCenter, String message) {
		ModelAndView res;

		res = new ModelAndView("socialCenter/create");
		res.addObject("socialCenter", socialCenter);
		res.addObject("message", message);
		res.addObject("requestURI", "socialCenter/administrator/create.do");
		Collection<Manager> managers = managerService.managerWithoutSocialCenter(socialCenter.getId());
		if (managers == null || managers.isEmpty()) {
			res.addObject("needManagers", true);
		} else {
			res.addObject("managers", managers);
		}
		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView editModelAndView(SocialCenter socialCenter) {
		ModelAndView res;

		res = this.editModelAndView(socialCenter, null);

		return res;
	}

	protected ModelAndView editModelAndView(SocialCenter socialCenter, String message) {
		ModelAndView res;

		res = new ModelAndView("socialCenter/edit");
		res.addObject("socialCenter", socialCenter);
		res.addObject("message", message);
		res.addObject("requestURI", "socialCenter/administrator/edit.do");
		res.addObject("managers", managerService.managerWithoutSocialCenter(socialCenter.getId()));
		Collection<Resource> resources = socialCenter.getResources();

		if (resources == null || resources.isEmpty()) {
			res.addObject("canDelete", true);
		}

		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int socialCenterId) {
		ModelAndView res;

		try {
			SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);

			socialCenterService.delete(socialCenter);
			res = new ModelAndView("redirect:/socialCenter/all.do");

		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

}
