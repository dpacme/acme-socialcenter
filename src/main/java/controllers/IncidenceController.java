
package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Client;
import domain.Incidence;
import domain.Manager;
import domain.Message;
import services.ClientService;
import services.IncidenceService;
import services.ManagerService;
import services.MessageService;

@Controller
@RequestMapping("/incidence")
public class IncidenceController extends AbstractController {

	// Services

	@Autowired
	private IncidenceService incidenceService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private MessageService		messageService;

	// Constructors -----------------------------------------------------------

	public IncidenceController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int incidenceId) {
		ModelAndView result;

		Incidence incidence = incidenceService.findOne(incidenceId);

		//Variable para saber a quien se est� contestando (por defecto)
		String messageFor = "";
		Integer actorReceivedId = null;

		boolean tienePermiso = true;

		/**
		 * Si el actor logado es un manager
		 * o es el cliente due�o de la incidencia, tiene permiso
		 */
		boolean managerLogado = this.managerService.isManagerLogged();
		boolean clientLogado = false;
		if (!managerLogado) {
			clientLogado = this.clientService.isClientLogged();
			if (clientLogado) {
				Client client = this.clientService.findByPrincipal();
				if (incidence.getReservation().getClient().getId() != client.getId()) {
					tienePermiso = false;
				}
			} else {
				tienePermiso = false;
			}
		}


		if (tienePermiso) {
			List<Manager> managers = (List<Manager>) this.managerService.findAll();

			//Obtener mensajes en orden de aparicion
			List<Message> messages = this.incidenceService.findMessagesOrdered(incidenceId);

			/*
			 * Si est� logado un manager, se contesta al cliente de la incidencia.
			 * Si est� logado un cliente, se contesta al ultimo manager que haya escrito
			 */
			if (managerLogado) {
				actorReceivedId = incidence.getReservation().getClient().getId();
				messageFor = incidence.getReservation().getClient().getUserAccount().getUsername();
			}else {
				/*
				 * Si es cliente, se recorre la lista ordenada para obtener el ultimo mensaje
				 * de manager, que ser� al que se conteste por defecto
				 */
				Message ultimoMensajeManager = null;
				for(Message message : messages) {
					if (message.getActorSend().getClass() == domain.Manager.class) {
						ultimoMensajeManager = message;
					}
				}
				if (ultimoMensajeManager != null) {
					actorReceivedId = ultimoMensajeManager.getActorSend().getId();
					messageFor = ultimoMensajeManager.getActorSend().getUserAccount().getUsername();
				}else {
					if(managers!=null && !managers.isEmpty()) {
						messageFor = managers.get(0).getUserAccount().getUsername();
						actorReceivedId = managers.get(0).getId();
					}
				}
			}

			//El mensaje padre de cualquier mensaje escrito, es el ultimo que exista
			Integer parentMessageId = null;
			if(messages!=null && !messages.isEmpty()) {
				parentMessageId = messages.get(messages.size()-1).getId();
			}

			result = new ModelAndView("incidence/showDisplay");
			result.addObject("requestURI", "incidence/showDisplay.do");
			result.addObject("incidence", incidence);
			result.addObject("messages", messages);
			result.addObject("actorReceivedId", actorReceivedId);
			result.addObject("messageFor", messageFor);
			result.addObject("parentMessageId", parentMessageId);
			result.addObject("managers", managers);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST, params = "sendMessage")
	public ModelAndView sendMessage(String text, Integer incidenceId, Integer actorReceivedId, Integer parentMessageId) {
		ModelAndView result;

		Incidence incidence = incidenceService.findOne(incidenceId);

		/**
		 * Si el actor logado es un manager
		 * o es el cliente due�o de la incidencia, tiene permiso
		 */
		boolean tienePermiso = true;

		boolean managerLogado = this.managerService.isManagerLogged();

		if (!managerLogado) {
			boolean clientLogado = this.clientService.isClientLogged();
			if (clientLogado) {
				Client client = this.clientService.findByPrincipal();
				if (incidence.getReservation().getClient().getId() != client.getId()) {
					tienePermiso = false;
				}
			} else {
				tienePermiso = false;
			}
		}
		if (!incidence.getStatus().equals("open")) {
			tienePermiso = false;
		}

		if (tienePermiso) {
			//Envio del mensaje
			boolean exito = this.messageService.sendMessage(text, incidenceId, actorReceivedId, parentMessageId);

			if (exito) {
				result = showDisplay(incidenceId);
			} else {
				result = new ModelAndView("misc/error");
				result.addObject("codigoError", "error.authorization");
			}
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}

}
