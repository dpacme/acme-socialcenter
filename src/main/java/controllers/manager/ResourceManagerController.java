
package controllers.manager;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Resource;
import domain.SocialCenter;
import services.ManagerService;
import services.ResourceService;

@Controller
@RequestMapping("/resource/manager")
public class ResourceManagerController extends AbstractController {

	// Services
	@Autowired
	private ResourceService resourceService;

	@Autowired
	private ManagerService	managerService;


	// Constructors -----------------------------------------------------------

	public ResourceManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/loanResources", method = RequestMethod.GET)
	public ModelAndView loanResources() {
		ModelAndView result;

		try {
			Assert.notNull(this.managerService.findByPrincipal().getSocialCenter(), "No tiene social center");
			SocialCenter socialCenter = this.managerService.findByPrincipal().getSocialCenter();
			Collection<Resource> resources = this.resourceService.findLoanResources(this.managerService.findByPrincipal().getSocialCenter());

			result = new ModelAndView("resource/manager/loanResources");
			result.addObject("resources", resources);
			result.addObject("requestURI", "resource/manager/loanResources.do");
			result.addObject("socialCenter", socialCenter);
			result.addObject("loanResources", true);

		} catch (Throwable oops) {
			result = new ModelAndView("resource/manager/loanResources");
			if (oops.getLocalizedMessage().contains("No tiene social center"))
				result.addObject("noSocialCenter", "noSocialCenter");
		}

		return result;
	}

}
