
package controllers.manager;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import services.ManagerService;

@Controller
@RequestMapping("/managerUri")
public class ManagerController extends AbstractController {

	// Services
	@Autowired
	private ManagerService managerService;


	// Constructors -----------------------------------------------------------

	public ManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Manager manager = this.managerService.findByPrincipal();

		res = this.createEditModelAndView(manager);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Manager manager, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(manager);
			if (!StringUtils.isEmpty(manager.getPostalAddress()) && !manager.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(manager.getPhone()) && !manager.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
			System.out.println(binding.getAllErrors());
		} else
			try {

				List<String> errores = this.managerService.comprobacionEditarListErrores(manager);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createEditModelAndView(manager);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.managerService.save(manager);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(manager);
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createEditModelAndView(Manager manager) {
		ModelAndView res;

		res = this.createEditModelAndView(manager, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Manager manager, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/edit");
		res.addObject("manager", manager);
		res.addObject("message", message);

		return res;
	}

}
