
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;
import services.BookService;
import services.CategoryService;
import services.ManagerService;
import services.ResourceService;
import services.SocialCenterService;

@Controller
@RequestMapping("/book/manager")
public class BookManagerController extends AbstractController {

	// Services

	@Autowired
	private BookService			bookService;

	@Autowired
	private SocialCenterService socialCenterService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private CategoryService		categoryService;

	@Autowired
	private ResourceService		resourceService;


	// Constructors -----------------------------------------------------------

	public BookManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int socialCenterId) {
		ModelAndView result;
		try {
			SocialCenter socialCenter = this.socialCenterService.findOne(socialCenterId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getId() == socialCenter.getId());

			Book book = this.bookService.create(socialCenter);

			result = this.createModelAndView(book);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid Book book, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(book);
			System.out.println(binding.getAllErrors());
		} else
			try {
				if (book.getCategories() != null && book.getCategories().contains(null))
					book.getCategories().remove(null);
				this.bookService.save(book);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(book);
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int bookId) {
		ModelAndView result;
		try {
			Book book = this.bookService.findOne(bookId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(book));

			result = this.editModelAndView(book);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Book book, BindingResult binding) {
		ModelAndView res;


		if (binding.hasErrors()) {
			res = this.editModelAndView(book);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.bookService.save(book);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.editModelAndView(book);
			}

		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int bookId) {
		ModelAndView res;
		Book book = this.bookService.findOne(bookId);

		try {
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(book), "No tienes acceso");
			Assert.isTrue(this.resourceService.compruebaRecurso(book), "No puedes borrarlo");
			this.bookService.delete(book);
			res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

		} catch (final Throwable oops) {
			if (oops.getLocalizedMessage().contains("No tienes acceso")) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			} else {
				res = new ModelAndView("socialCenter/manager/mySocialCenter");
				SocialCenter socialCenter = this.managerService.findByPrincipal().getSocialCenter();
				res.addObject("socialCenter", socialCenter);
				Collection<Book> books = this.socialCenterService.findBooksResources(socialCenter);
				Collection<ElectronicDevice> electronicDevices = this.socialCenterService.findElectronicDevicesResources(socialCenter);
				Collection<Room> rooms = this.socialCenterService.findRoomsResources(socialCenter);
				res.addObject("books", books);
				res.addObject("electronicDevices", electronicDevices);
				res.addObject("rooms", rooms);
				res.addObject("assessments", this.socialCenterService.getAssessmentsWithoutClientBanned(socialCenter.getId()));
				res.addObject("owner", "owner");
				res.addObject("deleteBook", "deleteBook");
				res.addObject("requestURI", "socialCenter/showDisplay.do");
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(Book book) {
		ModelAndView res;

		res = this.createModelAndView(book, null);

		return res;
	}

	protected ModelAndView createModelAndView(Book book, final String message) {
		ModelAndView res;

		res = new ModelAndView("book/manager/create");
		res.addObject("book", book);
		res.addObject("categories", this.categoryService.findAll());
		res.addObject("message", message);
		res.addObject("requestURI", "book/manager/create.do");

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView editModelAndView(Book book) {
		ModelAndView res;

		res = this.editModelAndView(book, null);

		return res;
	}

	protected ModelAndView editModelAndView(Book book, String message) {
		ModelAndView res;

		res = new ModelAndView("book/manager/edit");
		res.addObject("book", book);
		res.addObject("categories", this.categoryService.findAll());
		res.addObject("message", message);
		res.addObject("requestURI", "book/manager/edit.do");

		return res;
	}

}
