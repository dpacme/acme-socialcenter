/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.manager;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Penalty;
import services.PenaltyService;


@Controller
@RequestMapping("/penalty/manager")
public class PenaltyManagerController extends AbstractController {

	// Services

	@Autowired
	private PenaltyService penaltyService;



	// Constructors -----------------------------------------------------------

	public PenaltyManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView listByClient(@RequestParam Integer penaltyId) {
		ModelAndView result;
		Penalty penalty = this.penaltyService.findOne(penaltyId);

		result = this.createEditModelAndView(penalty);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Penalty penalty, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(penalty);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.penaltyService.save(penalty);
				res = new ModelAndView("redirect:/penalty/listByClient.do?clientId=" + penalty.getClient().getId());
			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(penalty);
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createEditModelAndView(Penalty penalty) {
		ModelAndView res;

		res = this.createEditModelAndView(penalty, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Penalty penalty, final String message) {
		ModelAndView res;

		res = new ModelAndView("penalty/manager/edit");
		res.addObject("penalty", penalty);
		res.addObject("message", message);

		return res;
	}


}
