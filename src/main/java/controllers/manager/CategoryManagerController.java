
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Category;
import services.CategoryService;

@Controller
@RequestMapping("/category/manager")
public class CategoryManagerController extends AbstractController {

	// Services

	@Autowired
	private CategoryService categoryService;

	// Constructors -----------------------------------------------------------


	public CategoryManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		Collection<Category> categories = categoryService.findAll();

		result = new ModelAndView("category/list");
		result.addObject("categories", categories);
		result.addObject("requestURI", "category/manager/all.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		try {

			Category category = categoryService.create();

			result = this.createModelAndView(category);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid Category category, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(category);
			System.out.println(binding.getAllErrors());
		} else
			try {
				categoryService.save(category);
				res = new ModelAndView("redirect:all.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(category);
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int categoryId) {
		ModelAndView result;
		try {
			Category category = categoryService.findOne(categoryId);

			result = this.editModelAndView(category);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Category category, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(category);
			System.out.println(binding.getAllErrors());
		} else
			try {
				categoryService.save(category);
				res = new ModelAndView("redirect:all.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(category);
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam int categoryId) {
		ModelAndView res;
		Category category = categoryService.findOne(categoryId);

		try {
			Assert.isTrue(category.getBooks() == null || category.getBooks().isEmpty(), "No puedes borrarlo");
			categoryService.delete(category);
			res = new ModelAndView("redirect:all.do");

		} catch (Throwable oops) {
			res = editModelAndView(category, "category.commit.error");
		}

		return res;

	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(Category category) {
		ModelAndView res;

		res = this.createModelAndView(category, null);

		return res;
	}

	protected ModelAndView createModelAndView(Category category, final String message) {
		ModelAndView res;

		res = new ModelAndView("category/manager/create");
		res.addObject("category", category);
		res.addObject("categories", categoryService.findAll());
		res.addObject("message", message);
		res.addObject("requestURI", "category/manager/create.do");

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView editModelAndView(Category category) {
		ModelAndView res;

		res = this.editModelAndView(category, null);

		return res;
	}

	protected ModelAndView editModelAndView(Category category, final String message) {
		ModelAndView res;

		res = new ModelAndView("category/manager/edit");
		res.addObject("category", category);
		res.addObject("categories", categoryService.findAll());
		res.addObject("message", message);
		res.addObject("requestURI", "category/manager/edit.do?categoryId=" + category.getId());

		return res;
	}

}
