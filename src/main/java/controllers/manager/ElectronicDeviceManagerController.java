
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;
import services.ElectronicDeviceService;
import services.ManagerService;
import services.ResourceService;
import services.SocialCenterService;

@Controller
@RequestMapping("/electronicDevice/manager")
public class ElectronicDeviceManagerController extends AbstractController {

	// Services

	@Autowired
	private ElectronicDeviceService	electronicDeviceService;

	@Autowired
	private SocialCenterService socialCenterService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ResourceService			resourceService;


	// Constructors -----------------------------------------------------------

	public ElectronicDeviceManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int socialCenterId) {
		ModelAndView result;
		try {
			SocialCenter socialCenter = this.socialCenterService.findOne(socialCenterId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getId() == socialCenter.getId());

			ElectronicDevice electronicDevice = this.electronicDeviceService.create(socialCenter);

			result = this.createModelAndView(electronicDevice);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid ElectronicDevice electronicDevice, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(electronicDevice);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.electronicDeviceService.save(electronicDevice);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(electronicDevice);
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int electronicDeviceId) {
		ModelAndView result;
		try {
			ElectronicDevice electronicDevice = this.electronicDeviceService.findOne(electronicDeviceId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(electronicDevice));

			result = this.createModelAndView(electronicDevice);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid ElectronicDevice electronicDevice, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(electronicDevice);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.electronicDeviceService.save(electronicDevice);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.editModelAndView(electronicDevice);
			}

		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int electronicDeviceId) {
		ModelAndView res;
		ElectronicDevice electronicDevice = this.electronicDeviceService.findOne(electronicDeviceId);

		try {
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(electronicDevice), "No tienes acceso");
			Assert.isTrue(this.resourceService.compruebaRecurso(electronicDevice), "No puedes borrarlo");
			this.electronicDeviceService.delete(electronicDevice);
			res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

		} catch (final Throwable oops) {
			if (oops.getLocalizedMessage().contains("No tienes acceso")) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			} else {
				res = new ModelAndView("socialCenter/manager/mySocialCenter");
				SocialCenter socialCenter = this.managerService.findByPrincipal().getSocialCenter();
				res.addObject("socialCenter", socialCenter);
				Collection<Book> books = this.socialCenterService.findBooksResources(socialCenter);
				Collection<ElectronicDevice> electronicDevices = this.socialCenterService.findElectronicDevicesResources(socialCenter);
				Collection<Room> rooms = this.socialCenterService.findRoomsResources(socialCenter);
				res.addObject("books", books);
				res.addObject("electronicDevices", electronicDevices);
				res.addObject("rooms", rooms);
				res.addObject("assessments", this.socialCenterService.getAssessmentsWithoutClientBanned(socialCenter.getId()));
				res.addObject("owner", "owner");
				res.addObject("deleteElectronicDevice", "deleteElectronicDevice");
				res.addObject("requestURI", "socialCenter/showDisplay.do");
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(ElectronicDevice electronicDevice) {
		ModelAndView res;

		res = this.createModelAndView(electronicDevice, null);

		return res;
	}

	protected ModelAndView createModelAndView(ElectronicDevice electronicDevice, final String message) {
		ModelAndView res;

		res = new ModelAndView("electronicDevice/manager/create");
		res.addObject("electronicDevice", electronicDevice);
		res.addObject("message", message);
		res.addObject("requestURI", "electronicDevice/manager/create.do");

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView editModelAndView(ElectronicDevice electronicDevice) {
		ModelAndView res;

		res = this.editModelAndView(electronicDevice, null);

		return res;
	}

	protected ModelAndView editModelAndView(ElectronicDevice electronicDevice, final String message) {
		ModelAndView res;

		res = new ModelAndView("electronicDevice/manager/edit");
		res.addObject("electronicDevice", electronicDevice);
		res.addObject("message", message);
		res.addObject("requestURI", "electronicDevice/manager/edit.do");

		return res;
	}

}
