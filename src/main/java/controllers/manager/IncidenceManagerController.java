
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Incidence;
import domain.Manager;
import domain.Reservation;
import services.ClientService;
import services.IncidenceService;
import services.ManagerService;
import services.MessageService;
import services.ReservationService;

@Controller
@RequestMapping("/incidence/manager")
public class IncidenceManagerController extends AbstractController {

	// Services

	@Autowired
	private IncidenceService	incidenceService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ReservationService	reservationService;

	@Autowired
	private MessageService		messageService;


	// Constructors -----------------------------------------------------------

	public IncidenceManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Collection<Incidence> incidences = incidenceService.findAll();

		result = new ModelAndView("incidence/list");
		result.addObject("incidences", incidences);
		result.addObject("requestURI", "incidence/manager/all.do");

		return result;
	}

	@RequestMapping(value = "/solvedIncidence", method = RequestMethod.GET)
	public ModelAndView solvedIncidence(@RequestParam Integer incidenceId) {
		ModelAndView result;

		incidenceService.changeStatusIncidence(incidenceId, true);
		result = new ModelAndView("redirect:/incidence/showDisplay.do?incidenceId=" + incidenceId);

		return result;
	}

	@RequestMapping(value = "/closedIncidence", method = RequestMethod.GET)
	public ModelAndView closedIncidence(@RequestParam Integer incidenceId) {
		ModelAndView result;

		incidenceService.changeStatusIncidence(incidenceId, false);
		result = new ModelAndView("redirect:/incidence/showDisplay.do?incidenceId=" + incidenceId);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam Integer reservationId) {
		ModelAndView res;
		final Incidence incidence;


		Manager manager = managerService.findByPrincipal();

		incidence = incidenceService.create();
		incidence.setManager(manager);

		Reservation reservation = reservationService.findOne(reservationId);
		incidence.setReservation(reservation);

		incidence.setStatus("open");

		res = new ModelAndView("incidence/edit");
		res.addObject("incidence", incidence);
		res.addObject("requestURI", "incidence/manager/create.do");

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Incidence incidence, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = createEditModelAndView(incidence, null);
			System.out.println(binding.getAllErrors());
		} else
			try {
				incidenceService.create(incidence.getTitle(), incidence.getDescription(), incidence.getReservation().getId());
				res = new ModelAndView("redirect:/reservation/showDisplay.do?reservationId=" + incidence.getReservation().getId());

			} catch (final Throwable oops) {
				res = createEditModelAndView(incidence, null);
			}

		return res;
	}

	// Ancillary methods ---------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Incidence incidence) {
		ModelAndView result;

		result = this.createEditModelAndView(incidence, null);

		return result;
	}
	protected ModelAndView createEditModelAndView(final Incidence incidence, final String message) {
		ModelAndView result;

		result = new ModelAndView("incidence/edit");

		result.addObject("incidence", incidence);
		result.addObject("message", message);

		result.addObject("requestURI", "incidence/manager/edit.do");

		return result;
	}


}
