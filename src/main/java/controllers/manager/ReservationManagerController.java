
package controllers.manager;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import domain.Reservation;
import domain.SocialCenter;
import services.ManagerService;
import services.ReservationService;

@Controller
@RequestMapping("/reservation/manager")
public class ReservationManagerController extends AbstractController {

	// Services

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ReservationService	reservationService;


	// Constructors -----------------------------------------------------------

	public ReservationManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Manager manager = managerService.findByPrincipal();
		Collection<Reservation> reservations;
		try {
			reservations = reservationService.allReservationBySocialCenter(manager.getSocialCenter().getId());
		} catch (Exception e) {
			reservations = null;
		}

		result = new ModelAndView("reservation/all");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/manager/all.do");

		return result;
	}

	@RequestMapping(value = "/activeReservations", method = RequestMethod.GET)
	public ModelAndView activeReservations() {
		ModelAndView result;

		Manager manager = managerService.findByPrincipal();
		Collection<Reservation> reservations;
		try {
			reservations = reservationService.activeReservationBySocialCenter(manager.getSocialCenter().getId());
		} catch (Exception e) {
			reservations = null;
		}

		SocialCenter socialCenter;
		try {
			socialCenter = manager.getSocialCenter();
		} catch (Exception e) {
			socialCenter = null;
		}

		result = new ModelAndView("reservation/activeReservations");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/manager/activeReservations.do");
		result.addObject("actives", true);
		if (socialCenter != null) {
			result.addObject("socialCenterId", manager.getSocialCenter().getId());
		}

		return result;
	}

	@RequestMapping(value = "/finalizeReservation", method = RequestMethod.GET)
	public ModelAndView cancelReservation(@RequestParam int reservationId) {
		ModelAndView res;

		try {
			Assert.isTrue(reservationId != 0);
			reservationService.finalizeReservation(reservationId);
			res = new ModelAndView("redirect:/reservation/manager/all.do");

		} catch (Exception e) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}
}
