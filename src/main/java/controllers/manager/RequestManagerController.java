
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Manager;
import domain.Request;
import services.ManagerService;
import services.RequestService;

@Controller
@RequestMapping("/request/manager")
public class RequestManagerController extends AbstractController {

	// Services
	@Autowired
	private ManagerService		managerService;

	@Autowired
	private RequestService	requestService;

	// Constructors -----------------------------------------------------------

	public RequestManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Manager manager = managerService.findByPrincipal();

		result = new ModelAndView("request/list");
		if (manager.getSocialCenter() != null) {
			result.addObject("requests", manager.getSocialCenter().getRequests());
		} else {
			Collection<Request> requests = new ArrayList<>();
			result.addObject("requests", requests);
		}
		result.addObject("manager", manager);
		result.addObject("requestURI", "request/manager/list.do");

		return result;
	}

	@RequestMapping(value = "/accept", method = RequestMethod.GET)
	public ModelAndView accept(@RequestParam Integer requestId) {
		ModelAndView result;

		try {

			Request request = requestService.findOne(requestId);
			Manager manager = managerService.findByPrincipal();
			Assert.isTrue(manager.getSocialCenter().getRequests().contains(request));
			request.setStatus("accepted");
			requestService.save(request);

			result = new ModelAndView("redirect:/request/manager/list.do");

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/complete", method = RequestMethod.GET)
	public ModelAndView complete(@RequestParam Integer requestId) {
		ModelAndView result;

		try {

			Request request = requestService.findOne(requestId);
			Manager manager = managerService.findByPrincipal();
			Assert.isTrue(manager.getSocialCenter().getRequests().contains(request));
			request.setStatus("completed");
			requestService.save(request);

			result = new ModelAndView("redirect:/request/manager/list.do");

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/reject", method = RequestMethod.GET)
	public ModelAndView reject(@RequestParam Integer requestId) {
		ModelAndView result;

		try {

			Request request = requestService.findOne(requestId);
			Manager manager = managerService.findByPrincipal();
			Assert.isTrue(manager.getSocialCenter().getRequests().contains(request));
			request.setStatus("rejected");
			requestService.save(request);

			result = new ModelAndView("redirect:/request/manager/list.do");

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

}
