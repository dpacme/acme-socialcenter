
package controllers.manager;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;
import services.ManagerService;
import services.SocialCenterService;

@Controller
@RequestMapping("/socialCenter/manager")
public class SocialCenterManagerController extends AbstractController {

	// Services
	@Autowired
	private SocialCenterService socialCenterService;

	@Autowired
	private ManagerService		managerService;


	// Constructors -----------------------------------------------------------

	public SocialCenterManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/mySocialCenter", method = RequestMethod.GET)
	public ModelAndView mySocialCenter() {
		ModelAndView result;
		SocialCenter socialCenter = managerService.findByPrincipal().getSocialCenter();

		result = new ModelAndView("socialCenter/manager/mySocialCenter");

		if (socialCenter != null) {
			result.addObject("socialCenter", socialCenter);
			Collection<Book> books = socialCenterService.findBooksResources(socialCenter);
			Collection<ElectronicDevice> electronicDevices = socialCenterService.findElectronicDevicesResources(socialCenter);
			Collection<Room> rooms = socialCenterService.findRoomsResources(socialCenter);
			result.addObject("books", books);
			result.addObject("electronicDevices", electronicDevices);
			result.addObject("rooms", rooms);
			result.addObject("assessments", socialCenterService.getAssessmentsWithoutClientBanned(socialCenter.getId()));
			result.addObject("owner", "owner");
			result.addObject("requestURI", "socialCenter/manager/mySocialCenter.do");
		} else
			result.addObject("noSocialCenter", "noSocialCenter");

		return result;
	}

}
