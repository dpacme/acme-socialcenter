
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;
import services.ManagerService;
import services.ResourceService;
import services.RoomService;
import services.SocialCenterService;

@Controller
@RequestMapping("/room/manager")
public class RoomManagerController extends AbstractController {

	// Services

	@Autowired
	private RoomService			roomService;

	@Autowired
	private SocialCenterService socialCenterService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ResourceService		resourceService;


	// Constructors -----------------------------------------------------------

	public RoomManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int socialCenterId) {
		ModelAndView result;
		try {
			SocialCenter socialCenter = this.socialCenterService.findOne(socialCenterId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getId() == socialCenter.getId());

			Room room = this.roomService.create(socialCenter);

			result = this.createModelAndView(room);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid Room room, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(room);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.roomService.save(room);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(room);
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int roomId) {
		ModelAndView result;
		try {
			Room room = this.roomService.findOne(roomId);
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(room));

			result = this.createModelAndView(room);

		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Room room, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(room);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.roomService.save(room);
				res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

			} catch (final Throwable oops) {
				res = this.editModelAndView(room);
			}

		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int roomId) {
		ModelAndView res;
		Room room = this.roomService.findOne(roomId);

		try {
			Assert.isTrue(this.managerService.findByPrincipal().getSocialCenter().getResources().contains(room), "No tienes acceso");
			Assert.isTrue(this.resourceService.compruebaRecurso(room), "No puedes borrarlo");
			Assert.isTrue(this.roomService.compruebaRoom(room), "No puedes borrarlo");
			this.roomService.delete(room);
			res = new ModelAndView("redirect:/socialCenter/manager/mySocialCenter.do");

		} catch (final Throwable oops) {
			if (oops.getLocalizedMessage().contains("No tienes acceso")) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			} else {
				res = new ModelAndView("socialCenter/manager/mySocialCenter");
				SocialCenter socialCenter = this.managerService.findByPrincipal().getSocialCenter();
				res.addObject("socialCenter", socialCenter);
				Collection<Book> books = this.socialCenterService.findBooksResources(socialCenter);
				Collection<ElectronicDevice> electronicDevices = this.socialCenterService.findElectronicDevicesResources(socialCenter);
				Collection<Room> rooms = this.socialCenterService.findRoomsResources(socialCenter);
				res.addObject("books", books);
				res.addObject("electronicDevices", electronicDevices);
				res.addObject("rooms", rooms);
				res.addObject("assessments", this.socialCenterService.getAssessmentsWithoutClientBanned(socialCenter.getId()));
				res.addObject("owner", "owner");
				res.addObject("deleteRoom", "deleteRoom");
				res.addObject("requestURI", "socialCenter/showDisplay.do");
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(Room room) {
		ModelAndView res;

		res = this.createModelAndView(room, null);

		return res;
	}

	protected ModelAndView createModelAndView(Room room, final String message) {
		ModelAndView res;

		res = new ModelAndView("room/manager/create");
		res.addObject("room", room);
		res.addObject("message", message);
		res.addObject("requestURI", "room/manager/create.do");

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView editModelAndView(Room room) {
		ModelAndView res;

		res = this.editModelAndView(room, null);

		return res;
	}

	protected ModelAndView editModelAndView(Room room, final String message) {
		ModelAndView res;

		res = new ModelAndView("room/manager/edit");
		res.addObject("room", room);
		res.addObject("message", message);
		res.addObject("requestURI", "room/manager/edit.do");

		return res;
	}

}
