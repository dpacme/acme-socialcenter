/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Activity;
import domain.Actor;
import domain.Monitor;
import services.ActivityService;
import services.ActorService;
import services.MonitorService;


@Controller
@RequestMapping("/activity")
public class ActivityController extends AbstractController {

	// Services

	@Autowired
	private ActivityService		activityService;

	@Autowired
	private MonitorService		monitorService;

	@Autowired
	private ActorService		actorService;



	// Constructors -----------------------------------------------------------

	public ActivityController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Activity> activities;
		activities = activityService.findAll();

		Actor actor = actorService.findByPrincipal();

		result = new ModelAndView("activity/list");
		result.addObject("client", actor);
		result.addObject("activities", activities);
		result.addObject("requestURI", "activity/list.do");

		return result;
	}

	@RequestMapping(value = "/listActivities", method = RequestMethod.GET)
	public ModelAndView listActivities(@RequestParam final Integer monitorId) {
		ModelAndView result;
		Monitor monitor = monitorService.findOne(monitorId);
		Collection<Activity> activities = monitor.getActivities();
		result = new ModelAndView("activity/list");
		result.addObject("activities", activities);
		result.addObject("requestURI", "activity/list.do");

		return result;
	}

	@RequestMapping(value = "/activityReservation/show", method = RequestMethod.GET)
	public ModelAndView activityReservationShow(@RequestParam Integer activityId) {
		ModelAndView result;

		try {
			Activity activity = activityService.findOne(activityId);
			Assert.isTrue(activity.getActivityReservation() != null);

			result = new ModelAndView("activity/activityReservation/show");
			result.addObject("activityReservation", activity.getActivityReservation());
			result.addObject("requestURI", "activity/activityReservation/show.do");

		} catch (Exception e) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}
}
