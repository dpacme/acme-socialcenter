/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Penalty;
import services.ClientService;


@Controller
@RequestMapping("/penalty")
public class PenaltyController extends AbstractController {

	// Services

	@Autowired
	private ClientService		clientService;



	// Constructors -----------------------------------------------------------

	public PenaltyController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Penalty> penalties;
		penalties = this.clientService.findByPrincipal().getPenalties();

		result = new ModelAndView("penalty/list");
		result.addObject("penalties", penalties);
		result.addObject("requestURI", "penalty/list.do");

		return result;
	}

	@RequestMapping(value = "/listByClient", method = RequestMethod.GET)
	public ModelAndView listByClient(@RequestParam Integer clientId) {
		ModelAndView result;
		Collection<Penalty> penalties;
		penalties = this.clientService.findOne(clientId).getPenalties();

		result = new ModelAndView("penalty/listByClient");
		result.addObject("penalties", penalties);
		result.addObject("requestURI", "penalty/listByClient.do");

		return result;
	}




}
