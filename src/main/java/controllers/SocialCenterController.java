
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Book;
import domain.ElectronicDevice;
import domain.Room;
import domain.SocialCenter;
import services.ResourceService;
import services.SocialCenterService;

@Controller
@RequestMapping("/socialCenter")
public class SocialCenterController extends AbstractController {

	// Services
	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private ResourceService		resourceService;


	// Constructors -----------------------------------------------------------

	public SocialCenterController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int socialCenterId) {
		ModelAndView result;
		SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);

		result = new ModelAndView("socialCenter/showDisplay");
		result.addObject("socialCenter", socialCenter);
		Collection<Book> books = socialCenterService.findBooksResources(socialCenter);
		Collection<ElectronicDevice> electronicDevices = socialCenterService.findElectronicDevicesResources(socialCenter);
		Collection<Room> rooms = socialCenterService.findRoomsResources(socialCenter);
		result.addObject("books", books);
		result.addObject("electronicDevices", electronicDevices);
		result.addObject("rooms", rooms);
		result.addObject("assessments", socialCenterService.getAssessmentsWithoutClientBanned(socialCenterId));

		result.addObject("requestURI", "socialCenter/showDisplay.do?socialCenterId=" + socialCenterId);

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		Collection<SocialCenter> socialCenters = socialCenterService.findAll();

		result = new ModelAndView("socialCenter/list");
		result.addObject("socialCenters", socialCenters);
		result.addObject("isMap", false);
		result.addObject("requestURI", "socialCenter/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(String keyword) {
		return searchSocialCenters(keyword);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(String keyword) {
		return searchSocialCenters(keyword);
	}

	private ModelAndView searchSocialCenters(String keyword) {
		ModelAndView result;
		Collection<SocialCenter> res;

		res = socialCenterService.searchSocialCenters(keyword);
		result = new ModelAndView("socialCenter/list");

		result.addObject("socialCenters", res);
		result.addObject("isMap", true);
		result.addObject("requestURI", "socialCenter/search.do");

		return result;
	}

}
