
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Client;
import domain.Incidence;
import domain.Reservation;
import services.ClientService;
import services.ManagerService;
import services.ReservationService;
import services.ResourceService;

@Controller
@RequestMapping("/reservation")
public class ReservationController extends AbstractController {

	// Services

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ResourceService		resourceService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ReservationService	reservationService;


	// Constructors -----------------------------------------------------------

	public ReservationController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int reservationId) {
		ModelAndView result;

		Reservation reservation = reservationService.findOne(reservationId);

		boolean tienePermiso = true;

		/**
		 * Si el actor logado es un manager
		 * o es el cliente due�o de la reserva, tiene permiso
		 */
		boolean managerLogado = this.managerService.isManagerLogged();
		boolean clientLogado = false;
		if (!managerLogado) {
			clientLogado = this.clientService.isClientLogged();
			if (clientLogado) {
				Client client = this.clientService.findByPrincipal();
				if (reservation.getClient().getId() != client.getId()) {
					tienePermiso = false;
				}
			} else {
				tienePermiso = false;
			}
		}

		if (tienePermiso) {
			Collection<Incidence> incidences = reservation.getIncidences();

			result = new ModelAndView("reservation/showDisplay");
			result.addObject("requestURI", "reservation/showDisplay.do");
			result.addObject("reservation", reservation);
			result.addObject("incidences", incidences);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

}
