
package controllers.client;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Client;
import domain.Comment;
import domain.Resource;
import services.ClientService;
import services.CommentService;
import services.ReservationService;
import services.ResourceService;

@Controller
@RequestMapping("/comment/client")
public class CommentClientController extends AbstractController {

	// Services
	@Autowired
	private ClientService		clientService;

	@Autowired
	private ReservationService	reservationService;

	@Autowired
	private CommentService		commentService;

	@Autowired
	private ResourceService		resourceService;


	// Constructors -----------------------------------------------------------

	public CommentClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam Integer resourceId) {
		ModelAndView result;

		Client client = clientService.findByPrincipal();
		Assert.notNull(client);
		Resource resource = resourceService.findOne(resourceId);
		Boolean clientOk = reservationService.findClientOk(client.getId(), resourceId);
		if (clientOk) {
			Comment comment = commentService.create();
			comment.setClient(client);
			comment.setResource(resource);
			result = this.createEditModelAndView(comment);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Comment comment, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(comment);
		else
			try {
				Client client = clientService.findByPrincipal();
				Assert.notNull(client);
				if (client.getId() == comment.getClient().getId()) {

					commentService.save(comment);
					result = new ModelAndView("redirect:/resource/showDisplay.do?resourceId=" + comment.getResource().getId());
					result.addObject("used", true);
				} else {
					result = new ModelAndView("misc/error");
					result.addObject("codigoError", "error.authorization");
				}
			} catch (Throwable oops) {
				result = this.createEditModelAndView(comment, "comment.commit.error");
			}
		return result;
	}

	// Ancillary methods -----------------------------------------
	protected ModelAndView createEditModelAndView(Comment comment) {
		ModelAndView result;

		result = this.createEditModelAndView(comment, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Comment comment, final String message) {
		ModelAndView result;

		result = new ModelAndView("comment/edit");
		result.addObject("comment", comment);
		result.addObject("message", message);

		return result;
	}
}
