/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.client;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import domain.Client;
import services.ActivityService;
import services.ClientService;


@Controller
@RequestMapping("/activity/client")
public class ActivityClientController extends AbstractController {

	// Services

	@Autowired
	private ActivityService		activityService;

	@Autowired
	private ClientService		clientService;



	// Constructors -----------------------------------------------------------

	public ActivityClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Client client = this.clientService.findByPrincipal();

		result = new ModelAndView("activity/client/list");
		result.addObject("client", client);
		result.addObject("activities", client.getActivities());
		result.addObject("requestURI", "activity/client/list.do");

		return result;
	}

	@RequestMapping(value = "/apply", method = RequestMethod.GET)
	public ModelAndView apply(@RequestParam Integer activityId) {
		ModelAndView result;
		Collection<Activity> activities;
		
		try {
			final Activity activity = this.activityService.findOne(activityId);
			Assert.isTrue(!activity.getClients().contains(this.clientService.findByPrincipal()));
			try {

				Client client = this.clientService.findByPrincipal();
				activities = client.getActivities();
				Assert.isTrue(activity.getStartDate().after(new Date()));
				Collection<Client> clients = activity.getClients();

				activities.add(this.activityService.findOne(activityId));
				client.setActivities(activities);
				clients.add(client);
				activity.setClients(clients);

				this.activityService.save(activity);
				this.clientService.save(client);

				result = new ModelAndView("redirect:/activity/client/list.do");

			} catch (Throwable oops) {
				activities = this.activityService.findAll();

				Client client = this.clientService.findByPrincipal();

				result = new ModelAndView("activity/client/list");
				result.addObject("client", client);
				result.addObject("activities", activities);
				result.addObject("activityStart", "activityStart");
				result.addObject("requestURI", "activity/client/list.do");
			}
		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		



		return result;
	}

	@RequestMapping(value = "/unapply", method = RequestMethod.GET)
	public ModelAndView unApply(@RequestParam Integer activityId) {
		ModelAndView result;
		Collection<Activity> activities;
		
		try {
			final Activity activity = this.activityService.findOne(activityId);
			Assert.isTrue(activity.getClients().contains(this.clientService.findByPrincipal()));
			try {
				Client client = this.clientService.findByPrincipal();
				activities = client.getActivities();
				Assert.isTrue(activity.getStartDate().after(new Date()));
				Collection<Client> clients = activity.getClients();

				activities.remove(this.activityService.findOne(activityId));
				client.setActivities(activities);
				clients.remove(client);
				activity.setClients(clients);

				this.activityService.save(activity);
				this.clientService.save(client);

				result = new ModelAndView("redirect:/activity/client/list.do");

			} catch (Throwable oops) {
				activities = this.activityService.findAll();

				Client client = this.clientService.findByPrincipal();

				result = new ModelAndView("activity/client/list");
				result.addObject("client", client);
				result.addObject("activities", activities);
				result.addObject("activityStart", "activityStart");
				result.addObject("requestURI", "activity/client/list.do");
			}
		} catch (Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		



		return result;
	}


}
