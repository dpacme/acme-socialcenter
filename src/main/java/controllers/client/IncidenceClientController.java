
package controllers.client;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Client;
import domain.Incidence;
import services.ClientService;
import services.IncidenceService;

@Controller
@RequestMapping("/incidence/client")
public class IncidenceClientController extends AbstractController {

	// Services

	@Autowired
	private IncidenceService incidenceService;

	@Autowired
	private ClientService		clientService;

	// Constructors -----------------------------------------------------------

	public IncidenceClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/myIncidences", method = RequestMethod.GET)
	public ModelAndView myIncidences() {
		ModelAndView result;

		//Comprobacion cliente logado
		Client client = this.clientService.findByPrincipal();

		boolean tienePermiso = client != null;

		if (tienePermiso) {

			Collection<Incidence> incidences = this.incidenceService.findByClientId(client.getId());

			result = new ModelAndView("incidence/list");
			result.addObject("incidences", incidences);
			result.addObject("requestURI", "incidence/client/myIncidences.do");

		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/listByClient", method = RequestMethod.GET)
	public ModelAndView listByClient(@RequestParam Integer clientId) {
		ModelAndView result;
		Collection<Incidence> incidences;
		incidences = this.incidenceService.findByClientId(clientId);

		result = new ModelAndView("incidence/client/listByClient");
		result.addObject("incidences", incidences);
		result.addObject("requestURI", "incidence/client/listByClient.do");

		return result;
	}

}
