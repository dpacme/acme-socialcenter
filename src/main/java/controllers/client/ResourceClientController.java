
package controllers.client;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Client;
import domain.Resource;
import services.ClientService;
import services.ResourceService;

@Controller
@RequestMapping("/resource/client")
public class ResourceClientController extends AbstractController {

	// Services
	@Autowired
	private ClientService	clientService;

	@Autowired
	private ResourceService	resourceService;


	// Constructors -----------------------------------------------------------

	public ResourceClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/myResources", method = RequestMethod.GET)
	public ModelAndView myResources() {
		ModelAndView result;

		Client client = clientService.findByPrincipal();

		Collection<Resource> resources = resourceService.getResourcesWithReservationsUsedByClient(client.getId());

		result = new ModelAndView("resource/list");
		result.addObject("resources", resources);
		result.addObject("used", true);
		result.addObject("requestURI", "client/resource/myResources.do");

		return result;
	}
}
