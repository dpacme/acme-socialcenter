
package controllers.client;

import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Client;
import domain.Reservation;
import domain.Resource;
import services.ClientService;
import services.ReservationService;
import services.ResourceService;

@Controller
@RequestMapping("/reservation/client")
public class ReservationClientController extends AbstractController {


	// Services

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ResourceService		resourceService;

	@Autowired
	private ReservationService	reservationService;


	// Constructors -----------------------------------------------------------

	public ReservationClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Reservation> reservations;
		Client client = clientService.findByPrincipal();
		try {
			reservations = client.getReservations();
		} catch (Exception e) {
			reservations = null;
		}

		result = new ModelAndView("reservation/client/list");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/client/list.do");

		return result;
	}

	@RequestMapping(value = "/activeReservations", method = RequestMethod.GET)
	public ModelAndView activeReservations() {
		ModelAndView result;
		Collection<Reservation> reservations;
		Client client = clientService.findByPrincipal();
		try {
			reservations = reservationService.activeReservations(client.getId());
		} catch (Exception e) {
			reservations = null;
		}

		result = new ModelAndView("reservation/activeReservations");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/client/activeReservations.do");
		result.addObject("actives", true);
		result.addObject("idLogged", client.getId());

		return result;
	}

	@RequestMapping(value = "/inactiveReservations", method = RequestMethod.GET)
	public ModelAndView inactiveReservations() {
		ModelAndView result;

		Client client = clientService.findByPrincipal();
		Collection<Reservation> reservations;
		try {
			reservations = reservationService.inactiveReservations(client.getId());
		} catch (Exception e) {
			reservations = null;
		}

		result = new ModelAndView("reservation/inactiveReservations");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/client/inactiveReservations.do");

		return result;
	}

	// Creación de reservation
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam Integer resourceId) {
		ModelAndView res;

		try {
			Resource resource = resourceService.findOne(resourceId);
			String name = resource.getClass().getName();

			Assert.isTrue(reservationService.timeIsCorrect());

			if (name.equals("domain.Book") || name.equals("domain.ElectronicDevice")) {
				resourceService.reserveResource(resourceId);
				res = new ModelAndView("redirect:/resource/showDisplay.do?resourceId=" + resourceId);
				res.addObject("correctReservation", true);
			} else if (name.equals("domain.Room")) {
				Reservation reservation = reservationService.create();
				reservation.setResource(resource);
				res = createModelAndView(reservation);
			}
			else {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			}

		} catch (Exception e) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/listByClient", method = RequestMethod.GET)
	public ModelAndView listByClient(@RequestParam Integer clientId) {
		ModelAndView result;
		Collection<Reservation> reservations;
		try {
			reservations = clientService.findOne(clientId).getReservations();
		} catch (Exception e) {
			reservations = null;
		}

		result = new ModelAndView("reservation/client/listByClient");
		result.addObject("reservations", reservations);
		result.addObject("requestURI", "reservation/client/listByClient.do");

		return result;
	}

	// Creación de ModelAndView
	protected ModelAndView createModelAndView(Reservation reservation) {
		ModelAndView res;

		res = createEditModelAndView(reservation, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Reservation reservation, String message) {
		ModelAndView res;

		res = new ModelAndView("reservation/edit");
		res.addObject("reservation", reservation);
		res.addObject("requestURI", "reservation/client/edit.do");
		res.addObject("message", message);
		res.addObject("mondayHours", reservationService.horariosPorDía(2, reservation.getResource().getId()));
		res.addObject("tuesdayHours", reservationService.horariosPorDía(3, reservation.getResource().getId()));
		res.addObject("wednesdayHours", reservationService.horariosPorDía(4, reservation.getResource().getId()));
		res.addObject("thursdayHours", reservationService.horariosPorDía(5, reservation.getResource().getId()));
		res.addObject("fridayHours", reservationService.horariosPorDía(6, reservation.getResource().getId()));
		res.addObject("saturdayHours", reservationService.horariosPorDía(7, reservation.getResource().getId()));
		res.addObject("sundayHours", reservationService.horariosPorDía(8, reservation.getResource().getId()));

		return res;
	}

	@RequestMapping(value = "/reservarRoom", method = RequestMethod.POST)
	public ModelAndView reservarRoom(@RequestParam String codigo, int resourceId) {
		ModelAndView res;

		try {
			reservationService.createReservationRoom(codigo, resourceId);
			res = new ModelAndView("redirect:/resource/showDisplay.do?resourceId=" + resourceId);
			res.addObject("correctReservation", true);
		} catch (Exception e) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/cancelReservation", method = RequestMethod.GET)
	public ModelAndView cancelReservation(@RequestParam int reservationId) {
		ModelAndView res;

		try {
			Assert.isTrue(reservationId != 0);
			Reservation reservation = reservationService.findOne(reservationId);
			Assert.notNull(reservation);
			Date date = new Date();
			DateTime dateTimeCurrent = new DateTime(date.getTime());
			DateTime startDate = new DateTime(reservation.getStartDate().getTime());
			String name = reservation.getResource().getClass().getName();
			Assert.isTrue(name.equals("domain.Room"));
			if (Days.daysBetween(startDate, dateTimeCurrent).getDays() == 0) {
				res = new ModelAndView("redirect:/reservation/client/activeReservations.do");
				res.addObject("notCancel", true);
			} else {
				reservationService.delete(reservation);
				res = new ModelAndView("redirect:/reservation/client/activeReservations.do");
			}
		} catch (Exception e) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}


		return res;
	}

}
