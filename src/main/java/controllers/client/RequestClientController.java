
package controllers.client;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Client;
import domain.Request;
import services.ClientService;
import services.ManagerService;
import services.RequestService;
import services.SocialCenterService;

@Controller
@RequestMapping("/request/client")
public class RequestClientController extends AbstractController {

	// Services
	@Autowired
	private ClientService		clientService;

	@Autowired
	private SocialCenterService	socialCenterService;

	@Autowired
	private RequestService		requestService;

	@Autowired
	private ManagerService		managerService;


	// Constructors -----------------------------------------------------------

	public RequestClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Client client = this.clientService.findByPrincipal();
		result = new ModelAndView("request/list");
		result.addObject("requests", client.getRequests());
		result.addObject("requestURI", "request/client/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;

		Client client = this.clientService.findByPrincipal();
		Assert.notNull(client);

		Request request = this.requestService.create();
		request.setClient(client);
		request.setStatus("pending");
		result = this.createEditModelAndView(request);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Request request, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(request);
		else
			try {
				Client client = this.clientService.findByPrincipal();
				Assert.notNull(client);
				if (client.getId() == request.getClient().getId()) {

					this.requestService.save(request);
					result = new ModelAndView("redirect:list.do");

				} else {
					result = new ModelAndView("misc/error");
					result.addObject("codigoError", "error.authorization");
				}
			} catch (Throwable oops) {
				result = this.createEditModelAndView(request, "request.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/listByClient", method = RequestMethod.GET)
	public ModelAndView listByClient(@RequestParam Integer clientId) {
		ModelAndView result;
		Collection<Request> requests;
		requests = this.clientService.findOne(clientId).getRequests();

		result = new ModelAndView("request/client/listByClient");
		result.addObject("requests", requests);
		result.addObject("manager", this.managerService.findByPrincipal());
		result.addObject("requestURI", "request/client/listByClient.do");

		return result;
	}

	// Ancillary methods -----------------------------------------
	protected ModelAndView createEditModelAndView(Request request) {
		ModelAndView result;

		result = this.createEditModelAndView(request, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/edit");
		result.addObject("request", request);
		result.addObject("socialCenters", this.socialCenterService.findAll());
		result.addObject("message", message);

		return result;
	}
}
