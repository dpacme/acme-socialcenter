
package controllers.client;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Assessment;
import domain.SocialCenter;
import services.AssessmentService;
import services.SocialCenterService;

@Controller
@RequestMapping("/assessment/client")
public class AssessmentClientController extends AbstractController {

	// Services

	@Autowired
	private AssessmentService	assessmentService;

	@Autowired
	private SocialCenterService	socialCenterService;


	// Constructors -----------------------------------------------------------

	public AssessmentClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Creaci�n de un assessment
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam Integer socialCenterId) {
		ModelAndView res;

		Assessment assessment = assessmentService.create();
		SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);
		assessment.setSocialCenter(socialCenter);
		res = createModelAndView(assessment);
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Assessment assessment, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = createEditModelAndView(assessment, null);
			System.out.println(binding.getAllErrors());
		} else
			try {
				assessmentService.save(assessment);
				res = new ModelAndView("redirect:/socialCenter/showDisplay.do?socialCenterId=" + assessment.getSocialCenter().getId());

			} catch (final Throwable oops) {
				res = createEditModelAndView(assessment, null);
			}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(Assessment assessment) {
		ModelAndView res;

		res = createEditModelAndView(assessment, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Assessment assessment, String message) {
		ModelAndView res;

		res = new ModelAndView("assessment/edit");
		res.addObject("assessment", assessment);
		res.addObject("requestURI", "assessment/client/edit.do");
		res.addObject("message", message);

		return res;
	}

	@RequestMapping(value = "/likeAssessment", method = RequestMethod.GET)
	public ModelAndView likeAssessment(@RequestParam Integer assessmentId) {
		ModelAndView result;

		boolean exito = assessmentService.likeAssessment(assessmentId);

		Assessment assessment = assessmentService.findOne(assessmentId);
		if (exito)
			result = new ModelAndView("redirect:/socialCenter/showDisplay.do?socialCenterId=" + assessment.getSocialCenter().getId());
		else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}

	@RequestMapping(value = "/dislikeAssessment", method = RequestMethod.GET)
	public ModelAndView dislikeAssessment(@RequestParam Integer assessmentId) {
		ModelAndView result;

		boolean exito = assessmentService.dislikeAssessment(assessmentId);

		Assessment assessment = assessmentService.findOne(assessmentId);
		if (exito)
			result = new ModelAndView("redirect:/socialCenter/showDisplay.do?socialCenterId=" + assessment.getSocialCenter().getId());
		else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}
		return result;
	}
}
