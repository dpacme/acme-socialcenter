package controllers.client;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Activity;
import domain.Client;
import forms.ClientForm;
import security.UserAccountService;
import services.ActivityService;
import services.ClientService;

@Controller
@RequestMapping("/client")
public class ClientController extends AbstractController {

	// Services
	@Autowired
	private ClientService clientService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserAccountService	userAccountService;


	// Constructors -----------------------------------------------------------

	public ClientController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un client
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ClientForm clientForm = new ClientForm();

		res = this.createFormModelAndView(clientForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ClientForm clientForm, final BindingResult binding) {
		ModelAndView res;
		Client client;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(clientForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(clientForm.getPostalAddress()) && !clientForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(clientForm.getPhone()) && !clientForm.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				client = this.clientService.reconstruct(clientForm);

				final List<String> errores = this.clientService.comprobacionEditarListErrores(client);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(clientForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.clientService.saveForm(client);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(clientForm);
				System.out.println(oops.getLocalizedMessage());

				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					if (this.userAccountService.findByUsername(clientForm.getUsername()) != null)
						res.addObject("duplicate", "duplicate");
					else
						res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("pass", "pass");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Client client = this.clientService.findByPrincipal();

		res = this.createEditModelAndView(client);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Client client, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(client);
			if (!StringUtils.isEmpty(client.getPostalAddress()) && !client.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(client.getPhone()) && !client.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
			System.out.println(binding.getAllErrors());
		} else
			try {

				List<String> errores = this.clientService.comprobacionEditarListErrores(client);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createEditModelAndView(client);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.clientService.save(client);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(client);
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicateIdentifier", "duplicateIdentifier");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	@RequestMapping(value = "/listClients", method = RequestMethod.GET)
	public ModelAndView listClients(@RequestParam final Integer activityId) {
		ModelAndView result;
		Activity activity = this.activityService.findOne(activityId);
		Collection<Client> clients;
		clients = activity.getClients();
		result = new ModelAndView("client/listClients");
		result.addObject("clients", clients);
		result.addObject("requestURI", "client/listClients.do");

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Client> clients;
		clients = this.clientService.findAll();
		result = new ModelAndView("client/list");
		result.addObject("clients", clients);
		result.addObject("requestURI", "client/list.do");

		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ClientForm clientForm) {
		ModelAndView res;

		res = this.createFormModelAndView(clientForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ClientForm clientForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("client/create");
		res.addObject("clientForm", clientForm);
		res.addObject("message", message);

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createEditModelAndView(Client client) {
		ModelAndView res;

		res = this.createEditModelAndView(client, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Client client, final String message) {
		ModelAndView res;

		res = new ModelAndView("client/edit");
		res.addObject("client", client);
		res.addObject("message", message);

		return res;
	}

}
