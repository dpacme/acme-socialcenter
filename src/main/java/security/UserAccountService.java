package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Service
@Transactional
public class UserAccountService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private UserAccountRepository userAccountRepository;	
	
	// Supporting services ----------------------------------------------------
		
	// Constructors -----------------------------------------------------------

	public UserAccountService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------

	public UserAccount create() {
		UserAccount result;

		result = new UserAccount();

		return result;
	}
	
	public void save(UserAccount userAccount) {
		Assert.notNull(userAccount);

		userAccountRepository.save(userAccount);
	}
	
	public UserAccount saveAndFlush(UserAccount userAccount) {
		Assert.notNull(userAccount);

		return userAccountRepository.saveAndFlush(userAccount);
	}
	
	// Other business methods -------------------------------------------------
	
	public UserAccount findByUsername(String username){
		return userAccountRepository.findByUsername(username);
	}
}