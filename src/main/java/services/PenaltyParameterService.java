
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.PenaltyParameter;
import repositories.PenaltyParameterRepository;

@Service
@Transactional
public class PenaltyParameterService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private PenaltyParameterRepository	penaltyParameterRepository;

	// Services
	@Autowired
	private AdministratorService		administratorService;

	// Constructor methods --------------------------------------------------------------
	public PenaltyParameterService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public PenaltyParameter findOne(int penaltyParameterId) {
		Assert.isTrue(penaltyParameterId != 0);
		PenaltyParameter result;

		result = penaltyParameterRepository.findOne(penaltyParameterId);
		Assert.notNull(result);

		return result;
	}

	public Collection<PenaltyParameter> findAll() {
		Collection<PenaltyParameter> result;

		result = penaltyParameterRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(PenaltyParameter penaltyParameter) {
		Assert.notNull(penaltyParameter);
		administratorService.checkIfAdministrator();
		Assert.isTrue(penaltyParameter.getDays() > 0);

		penaltyParameterRepository.save(penaltyParameter);
	}

	public PenaltyParameter create() {
		PenaltyParameter penaltyParameter = new PenaltyParameter();

		return penaltyParameter;
	}

	public void delete(PenaltyParameter penaltyParameter) {
		Assert.notNull(penaltyParameter);

		penaltyParameterRepository.delete(penaltyParameter);
	}

	public PenaltyParameter saveAndFlush(PenaltyParameter penaltyParameter) {
		Assert.notNull(penaltyParameter);

		return penaltyParameterRepository.saveAndFlush(penaltyParameter);
	}

	// Other bussines methods -----------------------------------------------------

}
