
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Request;
import repositories.RequestRepository;

@Service
@Transactional
public class RequestService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private RequestRepository requestRepository;

	@Autowired
	private SocialCenterService socialCenterService;


	// Constructor methods --------------------------------------------------------------
	public RequestService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Request create() {

		Request request = new Request();

		Assert.notNull(request);
		return request;
	}

	public Request findOne(final int requestId) {
		Assert.isTrue(requestId != 0);
		Request result;

		result = this.requestRepository.findOne(requestId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Request> findAll() {
		Collection<Request> result;

		result = this.requestRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Request request) {
		Assert.notNull(request);

		this.requestRepository.save(request);
	}

	public void delete(final Request request) {
		Assert.notNull(request);

		this.requestRepository.delete(request);
	}

	public Request saveAndFlush(final Request request) {
		Assert.notNull(request);

		return this.requestRepository.saveAndFlush(request);
	}

	// Other bussines methods -----------------------------------------------------

	//QUERY Media de peticiones aceptadas y denegadas por centro social.
	public Double rejectedPerSocialCenter() {
		return this.requestRepository.rejectedPerSocialCenter();
	}

	public Double acceptedPerSocialCenter() {
		return this.requestRepository.acceptedPerSocialCenter();
	}
}
