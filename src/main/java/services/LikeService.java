
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Like;
import repositories.LikeRepository;

@Service
@Transactional
public class LikeService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private LikeRepository likeRepository;


	// Constructor methods --------------------------------------------------------------
	public LikeService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Like findOne(final int likeId) {
		Assert.isTrue(likeId != 0);
		Like result;

		result = likeRepository.findOne(likeId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Like> findAll() {
		Collection<Like> result;

		result = likeRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Like like) {
		Assert.notNull(like);

		likeRepository.save(like);
	}

	public Like create() {
		Like like = new Like();

		return like;
	}

	public void delete(final Like like) {
		Assert.notNull(like);

		likeRepository.delete(like);
	}

	public Like saveAndFlush(final Like like) {
		Assert.notNull(like);

		return likeRepository.saveAndFlush(like);
	}

	// Other bussines methods -----------------------------------------------------

}
