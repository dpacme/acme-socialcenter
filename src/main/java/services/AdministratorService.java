
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Administrator;
import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AdministratorRepository administratorRepository;


	// Constructor methods --------------------------------------------------------------
	public AdministratorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Administrator findOne(final int administratorId) {
		Assert.isTrue(administratorId != 0);
		Administrator result;

		result = this.administratorRepository.findOne(administratorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = this.administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Administrator administrator) {
		Assert.notNull(administrator);
		this.administratorRepository.save(administrator);
	}

	public Administrator create() {
		final Administrator administrator = new Administrator();
		return administrator;
	}

	public void delete(final Administrator administrator) {
		Assert.notNull(administrator);
		this.administratorRepository.delete(administrator);
	}

	public Administrator saveAndFlush(final Administrator administrator) {
		Assert.notNull(administrator);

		return this.administratorRepository.saveAndFlush(administrator);
	}

	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.administratorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Other bussines methods -----------------------------------------------------

	public List<String> comprobacionEditarListErrores(Administrator administrator) {
		List<String> listaErrores = new ArrayList<String>();
		if (administrator.getPostalAddress() != "" && !administrator.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (administrator.getPhone() != "" && !administrator.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public void comprobacion(Administrator administrator) {
		if (administrator.getPostalAddress() != "")
			Assert.isTrue(administrator.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (administrator.getPhone() != "")
			Assert.isTrue(administrator.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

	public void checkIfAdministrator() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMINISTRATOR))
				res = true;
		Assert.isTrue(res);
	}

}
