
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.ActivityReservation;
import repositories.ActivityReservationRepository;

@Service
@Transactional
public class ActivityReservationService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ActivityReservationRepository activityReservationRepository;


	// Constructor methods --------------------------------------------------------------
	public ActivityReservationService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActivityService activityService;

	// Simple CRUD methods --------------------------------------------------------------

	public ActivityReservation findOne(final int activityReservationId) {
		Assert.isTrue(activityReservationId != 0);
		ActivityReservation result;

		result = this.activityReservationRepository.findOne(activityReservationId);
		Assert.notNull(result);

		return result;
	}

	public Collection<ActivityReservation> findAll() {
		Collection<ActivityReservation> result;

		result = this.activityReservationRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final ActivityReservation activityReservation) {
		Assert.notNull(activityReservation);

		this.activityReservationRepository.save(activityReservation);
	}

	public ActivityReservation create() {
		ActivityReservation activityReservation = new ActivityReservation();

		return activityReservation;
	}

	public void delete(final ActivityReservation activityReservation) {
		Assert.notNull(activityReservation);

		this.activityReservationRepository.delete(activityReservation);

		this.activityService.delete(activityReservation.getActivity());
	}

	public ActivityReservation saveAndFlush(final ActivityReservation activityReservation) {
		Assert.notNull(activityReservation);

		return this.activityReservationRepository.saveAndFlush(activityReservation);
	}

	// Other bussines methods -----------------------------------------------------

}
