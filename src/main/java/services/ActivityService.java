
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Client;
import domain.Monitor;
import repositories.ActivityRepository;

@Service
@Transactional
public class ActivityService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ActivityRepository activityRepository;


	// Constructor methods --------------------------------------------------------------
	public ActivityService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private MonitorService monitorService;

	// Simple CRUD methods --------------------------------------------------------------

	public Activity findOne(final int activityId) {
		Assert.isTrue(activityId != 0);
		Activity result;

		result = this.activityRepository.findOne(activityId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Activity> findAll() {
		Collection<Activity> result;

		result = this.activityRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Activity activity) {
		Assert.notNull(activity);
		Assert.isTrue(activity.getEndDate().after(activity.getStartDate()), "La fecha final debe ser posterior a la de comienzo");

		this.activityRepository.save(activity);
	}

	public Activity create() {
		Activity activity = new Activity();
		activity.setMonitor(this.monitorService.findByPrincipal());
		activity.setClients(new ArrayList<Client>());

		return activity;
	}

	public void delete(final Activity activity) {
		Assert.notNull(activity);

		for (Client c : activity.getClients())
			c.getActivities().remove(activity);

		this.activityRepository.delete(activity);
	}

	public Activity saveAndFlush(final Activity activity) {
		Assert.notNull(activity);

		return this.activityRepository.saveAndFlush(activity);
	}

	// Other bussines methods -----------------------------------------------------


	//QUERY Lista de actividades que tengan m�s del 50% de su capacidad ocupada.
	public Collection<Activity> listActivitiesHaveMore50CapacityOccupied() {
		try {
			return this.activityRepository.listActivitiesHaveMore50CapacityOccupied();
		} catch (Exception e) {
			return new ArrayList<Activity>();
		}
	}

	//QUERY Lista de actividades finalizadas por monitor.
	public Collection<Activity> findFinishedActivitiesPerMonitor() {
		try {
			return this.activityRepository.findFinishedActivitiesPerMonitor();
		} catch (Exception e) {
			return new ArrayList<Activity>();
		}
	}

	//QUERY Lista de actividades sin empezar por monitor.
	public Collection<Activity> findNotStartedActivitiesPerMonitor() {
		try {
			return this.activityRepository.findNotStartedActivitiesPerMonitor();
		} catch (Exception e) {
			return new ArrayList<Activity>();
		}
	}

	//QUERY M�nimo, m�ximo y media del n�mero de actividades por mes de los �ltimos seis meses por monitor.
	public Integer getMinActivitiesPerMonthLastSixMonthsPerMonitorAux() {
		Integer min = 0;
		Date date = new Date();
		date.setMonth(date.getMonth() - 6);

		for (Monitor m : this.monitorService.findAll()) {
			Integer aux = 0;
			for (Activity a : m.getActivities())
				if (a.getStartDate().after(date))
					aux++;
			if (min == 0)
				min = aux;
			else if (aux < min)
				min = aux;
		}

		return min;
	}

	public Integer getMaxActivitiesPerMonthLastSixMonthsPerMonitorAux() {
		Integer max = 0;
		Date date = new Date();
		date.setMonth(date.getMonth() - 6);

		for (Monitor m : this.monitorService.findAll()) {
			Integer aux = 0;
			for (Activity a : m.getActivities())
				if (a.getStartDate().after(date))
					aux++;
			if (max == 0)
				max = aux;
			else if (aux > max)
				max = aux;
		}

		return max;
	}

	public Double getAvgActivitiesPerMonthLastSixMonthsPerMonitorAux() {
		Double avg = 0.0;
		Date date = new Date();
		date.setMonth(date.getMonth() - 6);

		Double aux = 0.0;
		for (Activity a : this.activityRepository.findAll())
			if (a.getStartDate().after(date))
				aux++;

		if (this.activityRepository.findAll().size() != 0)
			avg = aux / this.activityRepository.findAll().size();

		return avg;
	}

}
