
package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Client;
import domain.Incidence;
import domain.Manager;
import domain.Message;
import repositories.MessageRepository;

@Service
@Transactional
public class MessageService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private MessageRepository messageRepository;

	// Services

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private IncidenceService	incidenceService;


	// Constructor methods --------------------------------------------------------------
	public MessageService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Message findOne(final int messageId) {
		Assert.isTrue(messageId != 0);
		Message result;

		result = this.messageRepository.findOne(messageId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Message> findAll() {
		Collection<Message> result;

		result = this.messageRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Message message) {
		Assert.notNull(message);

		this.messageRepository.save(message);
	}

	public Message create() {
		Message message = new Message();

		return message;
	}

	public void delete(final Message message) {
		Assert.notNull(message);

		this.messageRepository.delete(message);
	}

	public Message saveAndFlush(final Message message) {
		Assert.notNull(message);

		return this.messageRepository.saveAndFlush(message);
	}

	public boolean sendMessage(String text, Integer incidenceId, Integer actorReceivedId, Integer parentMessageId) {
		boolean exito = true;
		Assert.notNull(actorReceivedId);
		Assert.notNull(incidenceId);
		Assert.hasLength(text);

		try {
			boolean tienePermiso = true;

			Incidence incidence = this.incidenceService.findOne(incidenceId);
			Assert.notNull(incidence);
			Assert.isTrue(incidence.getStatus().equals("open"));

			Message parentMessage = null;
			if (parentMessageId != null) {
				parentMessage = findOne(parentMessageId);
			}

			boolean managerLogado = this.managerService.isManagerLogged();
			boolean clientLogado = false;
			if (!managerLogado) {
				clientLogado = this.clientService.isClientLogged();
				if (clientLogado) {
					Client client = this.clientService.findByPrincipal();
					if (incidence.getReservation().getClient().getId() != client.getId()) {
						tienePermiso = false;
					}
				} else {
					tienePermiso = false;
				}
			}

			if (tienePermiso) {

				Message message = create();
				message.setText(text);
				message.setIncidence(incidence);
				message.setRelatedTo(parentMessage);

				if (managerLogado) {
					Client clientDestino = null;
					clientDestino = clientService.findOne(actorReceivedId);

					Manager managerOrigen = managerService.findByPrincipal();
					message.setActorRecived(clientDestino);
					message.setActorSend(managerOrigen);
				} else {
					Manager managerDestino = null;
					managerDestino = managerService.findOne(actorReceivedId);

					Client clientOrigen = clientService.findByPrincipal();
					message.setActorRecived(managerDestino);
					message.setActorSend(clientOrigen);
				}

				message.setMommentCreate(new Date());

				save(message);
			} else {
				exito = false;
			}
		} catch (Exception e) {
			exito = false;
		}
		return exito;
	}

	// Other bussines methods -----------------------------------------------------

}
