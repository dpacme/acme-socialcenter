
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Comment;
import repositories.CommentRepository;

@Service
@Transactional
public class CommentService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CommentRepository commentRepository;


	// Constructor methods --------------------------------------------------------------
	public CommentService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Comment create() {

		Comment comment = new Comment();

		Assert.notNull(comment);
		return comment;
	}

	public Comment findOne(final int commentId) {
		Assert.isTrue(commentId != 0);
		Comment result;

		result = commentRepository.findOne(commentId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Comment> findAll() {
		Collection<Comment> result;

		result = commentRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Comment comment) {
		Assert.notNull(comment);

		commentRepository.save(comment);
	}

	public void delete(final Comment comment) {
		Assert.notNull(comment);

		commentRepository.delete(comment);
	}

	public Comment saveAndFlush(final Comment comment) {
		Assert.notNull(comment);

		return commentRepository.saveAndFlush(comment);
	}

	// Other bussines methods -----------------------------------------------------
}
