
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Incidence;
import domain.Manager;
import domain.Message;
import forms.ManagerForm;
import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class ManagerService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ManagerRepository managerRepository;

	@Autowired
	private ActorService		actorService;


	// Constructor methods --------------------------------------------------------------
	public ManagerService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Manager findOne(final int managerId) {
		Assert.isTrue(managerId != 0);
		Manager result;

		result = this.managerRepository.findOne(managerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Manager> findAll() {
		Collection<Manager> result;

		result = this.managerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Manager manager) {
		Assert.notNull(manager);
		this.managerRepository.save(manager);
	}

	public Manager create() {
		final Manager manager = new Manager();
		manager.setIncidences(new ArrayList<Incidence>());
		manager.setMessagesReceived(new ArrayList<Message>());
		manager.setMessagesWritten(new ArrayList<Message>());
		return manager;
	}

	public void delete(final Manager manager) {
		Assert.notNull(manager);
		this.managerRepository.delete(manager);
	}

	public Manager saveAndFlush(final Manager manager) {
		Assert.notNull(manager);

		return this.managerRepository.saveAndFlush(manager);
	}

	public Manager findByPrincipal() {
		Manager result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.managerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/**
	 * M�todo que comprueba si existe un managere logado
	 */
	public boolean isManagerLogged() {
		Manager result;
		boolean logado = false;
		UserAccount userAccount;
		try {
			userAccount = LoginService.getPrincipal();
			Assert.notNull(userAccount);
			result = this.managerRepository.findByUserAccount(userAccount);
			logado = result != null;
		} catch (final Exception e) {
			logado = false;
		}
		return logado;
	}

	// Other bussines methods -----------------------------------------------------

	public List<String> comprobacionEditarListErrores(Manager manager) {
		List<String> listaErrores = new ArrayList<String>();
		if (manager.getPostalAddress() != "" && !manager.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (manager.getPhone() != "" && !manager.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public Manager reconstruct(ManagerForm a) {
		Manager manager = this.create();

		UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		manager.setId(0);
		manager.setVersion(0);
		manager.setName(a.getName());
		manager.setSurname(a.getSurname());
		manager.setEmail(a.getEmail());
		manager.setPhone(a.getPhone());
		manager.setPostalAddress(a.getPostalAddress());
		manager.setUserAccount(account);
		manager.setIdentifier(a.getDni());

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		return manager;
	}

	public void saveForm(Manager manager) {

		Assert.notNull(manager);
		String password;
		Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		Collection<Authority> auths = new ArrayList<>();
		Authority auth = new Authority();
		auth.setAuthority("MANAGER");
		auths.add(auth);

		password = manager.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		manager.getUserAccount().setPassword(password);

		manager.getUserAccount().setAuthorities(auths);

		manager = this.managerRepository.saveAndFlush(manager);
	}

	public Collection<Manager> managerWithoutSocialCenter(int socialCenterId) {
		return this.managerRepository.managerWithoutSocialCenter(socialCenterId);
	}

	public void comprobacion(Manager manager) {
		if (manager.getPostalAddress() != "")
			Assert.isTrue(manager.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (manager.getPhone() != "")
			Assert.isTrue(manager.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(manager.getUserAccount().getUsername()));
		for (Actor actor : this.findAll())
			Assert.isTrue(!actor.getIdentifier().equals(manager.getIdentifier()), "Debe ser unico");
	}

	public void comprobacion2(Manager manager) {
		if (manager.getPostalAddress() != "")
			Assert.isTrue(manager.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (manager.getPhone() != "")
			Assert.isTrue(manager.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

	public void checkIfManager() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

}
