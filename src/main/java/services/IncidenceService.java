
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Client;
import domain.Incidence;
import domain.Manager;
import domain.Message;
import domain.Reservation;
import repositories.IncidenceRepository;

@Service
@Transactional
public class IncidenceService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private IncidenceRepository incidenceRepository;

	// Services

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private ReservationService	reservationService;


	// Constructor methods --------------------------------------------------------------
	public IncidenceService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Incidence findOne(final int incidenceId) {
		Assert.isTrue(incidenceId != 0);
		Incidence result;

		result = incidenceRepository.findOne(incidenceId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Incidence> findAll() {
		Collection<Incidence> result;

		result = incidenceRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Incidence incidence) {
		Assert.notNull(incidence);

		incidenceRepository.save(incidence);
	}

	public Incidence create() {
		//Control de roles: Incidencia debe ser creada por un manager

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		Incidence incidence = new Incidence();
		return incidence;

	}

	public void delete(final Incidence incidence) {
		Assert.notNull(incidence);

		incidenceRepository.delete(incidence);
	}

	public Incidence saveAndFlush(final Incidence incidence) {
		Assert.notNull(incidence);

		return incidenceRepository.saveAndFlush(incidence);
	}

	public Collection<Incidence> findByClientId(int clientId) {
		//Control de roles y que un cliente no acceda a las incidencias de otro
		//Si es manager o es el cliente y quiere obtener sus propias incidencias, todo ok

		Boolean managerLogado = this.managerService.isManagerLogged();
		if (!managerLogado) {
			Client client = this.clientService.findByPrincipal();
			Assert.isTrue(client.getId() == clientId);
		}

		return incidenceRepository.findByClientId(clientId);
	}

	public List<Message> findMessagesOrdered(int incidenceId) {

		Incidence incidence = findOne(incidenceId);

		//Control de roles y que un cliente no acceda a las incidencias de otro
		//Si es manager o es el cliente y quiere obtener sus propias incidencias, todo ok

		Boolean managerLogado = this.managerService.isManagerLogged();
		if (!managerLogado) {
			Client client = this.clientService.findByPrincipal();
			Assert.isTrue(client.getId() == incidence.getReservation().getClient().getId());
		}
		List<Message> result = new ArrayList<Message>();

		List<Integer> listaIdPadres = new ArrayList<Integer>();

		/*
		 * Lista de ids de los mensajes que son padres de otros
		 */
		for (Message message : incidence.getMessages())
			if (message.getRelatedTo() != null)
				listaIdPadres.add(message.getRelatedTo().getId());

		/**
		 * Se busca el mensaje que no es padre de nadie y desde el que hay que partir
		 */
		for (Message message : incidence.getMessages())
			if (!listaIdPadres.contains(message.getId())) {
				/*
				 * Este es el ultimo mensaje, y desde este creamos la lista
				 * desde el ultimo hasta el primero
				 */

				Message siguienteMensaje = message;
				while (siguienteMensaje != null) {
					result.add(siguienteMensaje);
					siguienteMensaje = siguienteMensaje.getRelatedTo();
				}
				break;
			}

		//Invertimos la lista para ordenar del primero al ultimo
		Collections.reverse(result);

		return result;
	}

	public void changeStatusIncidence(Integer incidenceId, Boolean statusIncidence) {

		Incidence incidence = findOne(incidenceId);
		Assert.notNull(incidence);

		if (incidence.getStatus().equals("open") && statusIncidence == true) {
			incidence.setStatus("solved");
			save(incidence);
		} else if (statusIncidence == false) {
			incidence.setStatus("closed");
			save(incidence);
		}
	}
	/*
	 * private String title;
	 * private String description;
	 * private String status;
	 */
	public Incidence create(String title, String description, Integer reservationId) {
		//Control de roles: Incidencia debe ser creada por un manager

		Assert.hasText(title);
		Assert.hasText(description);

		Manager manager = managerService.findByPrincipal();
		Assert.notNull(manager);

		Incidence incidence = new Incidence();

		Reservation reservation = this.reservationService.findOne(reservationId);
		Assert.notNull(reservation);
		incidence.setReservation(reservation);

		incidence.setStatus("open");
		incidence.setManager(manager);
		incidence.setTitle(title);
		incidence.setDescription(description);



		incidence.setStatus("open");

		return saveAndFlush(incidence);

	}

	// Other bussines methods -----------------------------------------------------

}
