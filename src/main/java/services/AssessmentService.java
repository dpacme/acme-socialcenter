
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Assessment;
import domain.Client;
import domain.Like;
import repositories.AssessmentRepository;

@Service
@Transactional
public class AssessmentService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AssessmentRepository	assessmentRepository;

	// Services --------------------------------------------------------------
	@Autowired
	private ClientService			clientService;

	@Autowired
	private LikeService				likeService;


	// Constructor methods --------------------------------------------------------------
	public AssessmentService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Assessment findOne(final int assessmentId) {
		Assert.isTrue(assessmentId != 0);
		Assessment result;

		result = assessmentRepository.findOne(assessmentId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Assessment> findAll() {
		Collection<Assessment> result;

		result = assessmentRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Assessment assessment) {
		Assert.notNull(assessment);

		Client client = clientService.findByPrincipal();
		assessment.setClient(client);
		assessmentRepository.save(assessment);
	}

	public Assessment create() {
		Assessment assessment = new Assessment();

		return assessment;
	}

	public void delete(final Assessment assessment) {
		Assert.notNull(assessment);

		assessmentRepository.delete(assessment);
	}

	public Assessment saveAndFlush(final Assessment assessment) {
		Assert.notNull(assessment);

		return assessmentRepository.saveAndFlush(assessment);
	}

	public boolean likeAssessment(Integer assessmentId) {
		boolean exito = false;

		try {
			Assessment assessment = findOne(assessmentId);
			Assert.notNull(assessment);

			Client client = clientService.findByPrincipal();
			Assert.notNull(client);

			Like like = assessmentRepository.getLikeFromAssessmentAndClient(assessmentId, client.getId());
			if (like != null) {
				like.setLikeMe(true);
				likeService.save(like);
			} else {
				Like newLike = likeService.create();
				newLike.setLikeMe(true);
				newLike.setAssessment(assessment);
				newLike.setClient(client);
				likeService.save(newLike);
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}

	public boolean dislikeAssessment(Integer assessmentId) {
		boolean exito = false;

		try {
			Assessment assessment = findOne(assessmentId);
			Assert.notNull(assessment);

			Client client = clientService.findByPrincipal();
			Assert.notNull(client);

			Like like = assessmentRepository.getLikeFromAssessmentAndClient(assessmentId, client.getId());
			if (like != null) {
				like.setLikeMe(false);
				likeService.save(like);
			} else {
				Like newLike = likeService.create();
				newLike.setLikeMe(false);
				newLike.setAssessment(assessment);
				newLike.setClient(client);
				likeService.save(newLike);
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}
	// Other bussines methods -----------------------------------------------------

}
