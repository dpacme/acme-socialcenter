
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Penalty;
import domain.PenaltyParameter;
import repositories.PenaltyParameterRepository;
import repositories.PenaltyRepository;

@Service
@Transactional
public class PenaltyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private PenaltyRepository	penaltyRepository;

	// Services


	// Constructor methods --------------------------------------------------------------
	public PenaltyService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Penalty findOne(int penaltyId) {
		Assert.isTrue(penaltyId != 0);
		Penalty result;

		result = penaltyRepository.findOne(penaltyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Penalty> findAll() {
		Collection<Penalty> result;

		result = penaltyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Penalty penalty) {
		Assert.notNull(penalty);

		penaltyRepository.save(penalty);
	}

	public Penalty create() {
		Penalty penalty = new Penalty();

		return penalty;
	}

	public void delete(Penalty penalty) {
		Assert.notNull(penalty);

		penaltyRepository.delete(penalty);
	}

	public Penalty saveAndFlush(Penalty penalty) {
		Assert.notNull(penalty);

		return penaltyRepository.saveAndFlush(penalty);
	}

	// Other bussines methods -----------------------------------------------------

}
