
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.ActivityReservation;
import domain.Comment;
import domain.Reservation;
import domain.Room;
import domain.SocialCenter;
import repositories.RoomRepository;

@Service
@Transactional
public class RoomService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private RoomRepository roomRepository;


	// Constructor methods --------------------------------------------------------------
	public RoomService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private ReservationService	reservationService;

	@Autowired
	private ActivityReservationService	activityReservationService;

	// Simple CRUD methods --------------------------------------------------------------

	public Room findOne(final int roomId) {
		Assert.isTrue(roomId != 0);
		Room result;

		result = this.roomRepository.findOne(roomId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Room> findAll() {
		Collection<Room> result;

		result = this.roomRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Room room) {
		Assert.notNull(room);

		this.roomRepository.save(room);
	}

	public Room create(SocialCenter socialCenter) {
		Room room = new Room();
		room.setSocialCenter(socialCenter);
		room.setActivityReservations(new ArrayList<ActivityReservation>());
		room.setComments(new ArrayList<Comment>());
		room.setReservations(new ArrayList<Reservation>());
		room.setTicker(this.resourceService.createTicker());

		return room;
	}

	public void delete(final Room room) {
		Assert.notNull(room);

		for (Reservation r : room.getReservations())
			this.reservationService.delete(r);

		for (ActivityReservation r : room.getActivityReservations())
			this.activityReservationService.delete(r);

		this.roomRepository.delete(room);
	}

	public Room saveAndFlush(final Room room) {
		Assert.notNull(room);

		return this.roomRepository.saveAndFlush(room);
	}

	// Other bussines methods -----------------------------------------------------

	public Boolean compruebaRoom(Room room) {
		Boolean res = true;
		Date date = new Date();

		for (ActivityReservation a : room.getActivityReservations())
			if (a.getEndDate().after(date))
				res = false;

		return res;
	}

	public void comprobacion(Room room) {
		Assert.isTrue(room.getCapacity() != null);
		Assert.isTrue(room.getBoard() != null);
		Assert.isTrue(room.getTv() != null);
	}

}
