
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Client;
import domain.Incidence;
import domain.Reservation;
import domain.Resource;
import domain.SocialCenter;
import repositories.ResourceRepository;

@Service
@Transactional
public class ResourceService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ResourceRepository resourceRepository;


	// Constructor methods --------------------------------------------------------------
	public ResourceService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ReservationService	reservationService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ManagerService		managerService;

	@Autowired
	private SocialCenterService	socialCenterService;


	// Simple CRUD methods --------------------------------------------------------------

	public Resource findOne(final int resourceId) {
		Assert.isTrue(resourceId != 0);
		Resource result;

		result = resourceRepository.findOne(resourceId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Resource> findAll() {
		Collection<Resource> result;

		result = resourceRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Resource resource) {
		Assert.notNull(resource);

		resourceRepository.save(resource);
	}

	public void delete(final Resource resource) {
		Assert.notNull(resource);

		resourceRepository.delete(resource);
	}

	public Resource saveAndFlush(final Resource resource) {
		Assert.notNull(resource);

		return resourceRepository.saveAndFlush(resource);
	}

	// Other bussines methods -----------------------------------------------------

	public String createTicker() {
		final String abecedario = "QWERTYUIOPASDFGHJKLZXCVBNM";
		final String numeracion = "1234567890";
		final String res = RandomStringUtils.random(5, abecedario) + "-" + RandomStringUtils.random(5, numeracion);
		return res;
	}

	public Boolean compruebaRecurso(Resource resource) {
		Boolean res = true;

		for (Reservation reservation : resource.getReservations())
			if (reservation.getReturnDate() == null)
				res = false;

		return res;
	}

	public Collection<Resource> searchResources(String keyword, Integer socialCenterId, Boolean loanResources) {
		Collection<Resource> result = new ArrayList<Resource>();
		Collection<Resource> aux = new ArrayList<Resource>();

		if (keyword == null)
			keyword = "";

		try {
			if (socialCenterId != null) {
				SocialCenter socialCenter = socialCenterService.findOne(socialCenterId);
				Assert.notNull(socialCenter);
				aux = resourceRepository.getElectronicDevicesByKeyword("%" + keyword + "%", socialCenterId);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
				aux = resourceRepository.getBooksByKeyword("%" + keyword + "%", socialCenterId);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
				aux = resourceRepository.getRoomByTicker("%" + keyword + "%", socialCenterId);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
			} else {
				aux = resourceRepository.getElectronicDevicesByKeyword("%" + keyword + "%", null);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
				aux = resourceRepository.getBooksByKeyword("%" + keyword + "%", null);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
				aux = resourceRepository.getRoomByTicker("%" + keyword + "%", null);
				if (aux != null && !aux.isEmpty())
					result.addAll(aux);
			}
			if (loanResources != null && loanResources) {
				Collection<Resource> resources = findLoanResources(managerService.findByPrincipal().getSocialCenter());
				aux = new ArrayList<Resource>();
				for (Resource r : result) {
					if (resources.contains(r)) {
						aux.add(r);
					}
				}
				result = aux;
			}
		} catch (Exception e) {
			return new ArrayList<Resource>();
		}
		return result;
	}

	public Reservation reserveResource(int resourceId) {

		Assert.isTrue(resourceIsAvailable(resourceId));

		Collection<Incidence> incidences = new ArrayList<>();

		Client client = clientService.findByPrincipal();
		Assert.notNull(client);
		Assert.isTrue(!client.getBanned());
		Assert.isTrue(!clientService.hasPenalty(client.getId()));
		Assert.isTrue(reservationService.activeReservations(client.getId()).size() < 3);

		Resource resource = findOne(resourceId);
		Assert.notNull(resource);

		String name = resource.getClass().getName();

		Reservation reservation = reservationService.create();
		Date date = new Date();
		Date endDate = new Date();
		reservation.setStartDate(date);
		reservation.setResource(resource);

		reservation = reservationService.create();
		reservation.setClient(client);
		reservation.setResource(resource);
		reservation.setIncidences(incidences);

		endDate = returnEndDate(name);

		Assert.isTrue(date.before(endDate));

		reservation.setStartDate(date);
		reservation.setReturnDate(null);
		reservation.setEndDate(endDate);

		Reservation reservationSaved = reservationService.saveAndFlush(reservation);

		return reservationSaved;
	}

	private Date returnEndDate(String name) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();

		if (name.equals("domain.Book")) {
			calendar.add(Calendar.DAY_OF_YEAR, 14);
			date = calendar.getTime();
		} else if (name.equals("domain.ElectronicDevice")) {
			int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
			calendar.add(Calendar.HOUR, 4);
			if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 || calendar.get(Calendar.DAY_OF_YEAR) > dayOfYear) {
				calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
				calendar.set(Calendar.HOUR_OF_DAY, 22);
				calendar.set(Calendar.MINUTE, 00);
				calendar.set(Calendar.SECOND, 00);
				calendar.set(Calendar.MILLISECOND, 00);
			}
			date = calendar.getTime();


		}
		return date;
	}

	public Boolean resourceIsAvailable(Integer resourceId) {
		Boolean res = true;

		Resource resource = findOne(resourceId);

		String name = resource.getClass().getName();

		if (name.equals("domain.Room")) {
			if (hasRoomReservationActive())
				res = false;
			else
				res = true;
		} else {
			int size = resourceRepository.resourceIsAvailable(resourceId);
			if (size > 0)
				res = false;

		}

		return res;
	}

	public Collection<Resource> findLoanResources(SocialCenter socialCenter) {
		Collection<Resource> all = socialCenter.getResources();
		Collection<Resource> res = new ArrayList<Resource>();

		for (Resource r : all)
			for (Reservation reservation : r.getReservations())
				if (reservation.getReturnDate() == null) {
					res.add(r);
					break;
				}

		return res;
	}

	public Collection<Resource> getResourcesWithReservationsUsedByClient(Integer clientId) {
		try {
			return resourceRepository.getResourcesWithReservationsUsedByClient(clientId);
		} catch (Exception e) {
			return new ArrayList<Resource>();
		}
	}

	public Collection<Resource> getResourcesOrderByNumReservations() {
		return resourceRepository.getResourcesOrderByNumReservations();

	}

	//QUERY 2 M�nimo, m�ximo y media del n�mero de reservas por centro social.
	public Collection<Object> getMaxMinAvgReservationsForSocialCenter() {
		try {
			List<Long> countsReservesForSocialCenter = resourceRepository.getCountReservationsForSocialCenter();
			Long max = (long) 0;
			Long min = (long) 0;
			Long sum = (long) 0;
			for (Integer i = 0; i < countsReservesForSocialCenter.size(); i++) {
				if (countsReservesForSocialCenter.get(i) > max)
					max = countsReservesForSocialCenter.get(i);
				if (countsReservesForSocialCenter.get(i) < min)
					min = countsReservesForSocialCenter.get(i);
				sum = sum + countsReservesForSocialCenter.get(i);
			}

			Double avg = 0.0;
			if (countsReservesForSocialCenter.size() != 0)
				avg = (sum.doubleValue() / countsReservesForSocialCenter.size()) * 1.0;

			List<Object> maxMinAvgReservationsForSocialCenter = new ArrayList<Object>();
			maxMinAvgReservationsForSocialCenter.add(max);
			maxMinAvgReservationsForSocialCenter.add(min);
			maxMinAvgReservationsForSocialCenter.add(avg);

			return maxMinAvgReservationsForSocialCenter;
		} catch (Exception e) {
			return new ArrayList<Object>();
		}
	}

	public Boolean hasRoomReservationActive() {
		Boolean res = false;

		Client client = clientService.findByPrincipal();
		Assert.notNull(client);

		for (Reservation reservation : client.getReservations())
			if (reservation.getResource().getClass().getName().equals("domain.Room") && reservation.getReturnDate() == null)
				res = true;

		return res;
	}
	//QUERY 2 M�nimo, m�ximo y media del n�mero de reservas pendientes por centro social.
	public Collection<Object> getMaxMinAvgPendingReservationsForSocialCenter() {
		try {
			List<Long> countsPendingReservesForSocialCenter = resourceRepository.getCountPendingReservationsForSocialCenter();
			Long max = (long) 0;
			Long min = (long) 0;
			Long sum = (long) 0;
			for (Integer i = 0; i < countsPendingReservesForSocialCenter.size(); i++) {
				if (countsPendingReservesForSocialCenter.get(i) > max)
					max = countsPendingReservesForSocialCenter.get(i);
				if (countsPendingReservesForSocialCenter.get(i) < min)
					min = countsPendingReservesForSocialCenter.get(i);
				sum = sum + countsPendingReservesForSocialCenter.get(i);
			}
			Double avg = 0.0;
			if (countsPendingReservesForSocialCenter.size() != 0)
				avg = (sum.doubleValue() / countsPendingReservesForSocialCenter.size()) * 1.0;

			List<Object> maxMinAvgPendingReservationsForSocialCenter = new ArrayList<Object>();
			maxMinAvgPendingReservationsForSocialCenter.add(max);
			maxMinAvgPendingReservationsForSocialCenter.add(min);
			maxMinAvgPendingReservationsForSocialCenter.add(avg);

			return maxMinAvgPendingReservationsForSocialCenter;
		} catch (Exception e) {
			return new ArrayList<Object>();
		}

	}

	public List<Resource> orderedResources() {
		return resourceRepository.orderedResources();
	}

}
