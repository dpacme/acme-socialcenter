
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Monitor;
import forms.MonitorForm;
import repositories.MonitorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class MonitorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private MonitorRepository monitorRepository;


	// Constructor methods --------------------------------------------------------------
	public MonitorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService actorService;

	// Simple CRUD methods --------------------------------------------------------------

	public Monitor findOne(final int monitorId) {
		Assert.isTrue(monitorId != 0);
		Monitor result;

		result = this.monitorRepository.findOne(monitorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Monitor> findAll() {
		Collection<Monitor> result;

		result = this.monitorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Monitor monitor) {
		Assert.notNull(monitor);
		this.monitorRepository.save(monitor);
	}

	public Monitor create() {
		final Monitor monitor = new Monitor();
		return monitor;
	}

	public void delete(final Monitor monitor) {
		Assert.notNull(monitor);
		this.monitorRepository.delete(monitor);
	}

	public Monitor saveAndFlush(final Monitor monitor) {
		Assert.notNull(monitor);

		return this.monitorRepository.saveAndFlush(monitor);
	}

	public Monitor findByPrincipal() {
		Monitor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.monitorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Other bussines methods -----------------------------------------------------

	public List<String> comprobacionEditarListErrores(Monitor monitor) {
		List<String> listaErrores = new ArrayList<String>();
		if (monitor.getPostalAddress() != "" && !monitor.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (monitor.getPhone() != "" && !monitor.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public Monitor reconstruct(MonitorForm a) {
		Monitor monitor = this.create();

		UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		monitor.setId(0);
		monitor.setVersion(0);
		monitor.setName(a.getName());
		monitor.setSurname(a.getSurname());
		monitor.setEmail(a.getEmail());
		monitor.setPhone(a.getPhone());
		monitor.setPostalAddress(a.getPostalAddress());
		monitor.setIdentifier(a.getDni());

		monitor.setUserAccount(account);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return monitor;
	}

	public void saveForm(Monitor monitor) {

		Assert.notNull(monitor);
		String password;
		Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		Collection<Authority> auths = new ArrayList<>();
		Authority auth = new Authority();
		auth.setAuthority("MONITOR");
		auths.add(auth);

		password = monitor.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		monitor.getUserAccount().setPassword(password);

		monitor.getUserAccount().setAuthorities(auths);

		monitor = this.monitorRepository.saveAndFlush(monitor);
	}

	public void comprobacion(Monitor monitor) {
		if (monitor.getPostalAddress() != "")
			Assert.isTrue(monitor.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (monitor.getPhone() != "")
			Assert.isTrue(monitor.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(monitor.getUserAccount().getUsername()));
		for (Actor actor : this.findAll())
			Assert.isTrue(!actor.getIdentifier().equals(monitor.getIdentifier()), "Debe ser unico");
	}

	public void comprobacion2(Monitor monitor) {
		if (monitor.getPostalAddress() != "")
			Assert.isTrue(monitor.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (monitor.getPhone() != "")
			Assert.isTrue(monitor.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

	public void checkIfMonitor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.MONITOR))
				res = true;
		Assert.isTrue(res);
	}

}
