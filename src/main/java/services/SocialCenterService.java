
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Assessment;
import domain.Book;
import domain.Client;
import domain.ElectronicDevice;
import domain.Like;
import domain.Manager;
import domain.Request;
import domain.Resource;
import domain.Room;
import domain.SocialCenter;
import repositories.SocialCenterRepository;

@Service
@Transactional
public class SocialCenterService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private SocialCenterRepository socialCenterRepository;


	// Constructor methods --------------------------------------------------------------
	public SocialCenterService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	ManagerService managerService;

	@Autowired
	LikeService			likeService;

	@Autowired
	AssessmentService	assessmentService;

	@Autowired
	ClientService		clientService;

	@Autowired
	RequestService		requestService;

	@Autowired
	AdministratorService	administratorService;


	// Simple CRUD methods --------------------------------------------------------------

	public SocialCenter findOne(int socialCenterId) {
		Assert.isTrue(socialCenterId != 0);
		SocialCenter result;

		result = socialCenterRepository.findOne(socialCenterId);
		Assert.notNull(result);

		return result;
	}

	public Collection<SocialCenter> findAll() {
		Collection<SocialCenter> result;

		result = socialCenterRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(SocialCenter socialCenter) {
		Assert.notNull(socialCenter);
		administratorService.checkIfAdministrator();
		Assert.isTrue(socialCenter.getLatitude() != null && socialCenter.getLongitude() != null);
		Assert.isTrue(socialCenter.getLatitude() <= 90 && socialCenter.getLatitude() >= -90 && socialCenter.getLongitude() <= 90 && socialCenter.getLongitude() >= -90);
		updateManagers(socialCenter);


		socialCenterRepository.save(socialCenter);
	}

	public SocialCenter create() {
		SocialCenter socialCenter = new SocialCenter();

		Collection<Manager> managers = new ArrayList<Manager>();
		socialCenter.setManagers(managers);

		Collection<Resource> resources = new ArrayList<Resource>();
		Collection<Assessment> assessments = new ArrayList<Assessment>();
		Collection<Request> requests = new ArrayList<Request>();
		socialCenter.setResources(resources);
		socialCenter.setAssessments(assessments);
		socialCenter.setRequests(requests);

		return socialCenter;
	}

	public void delete(SocialCenter socialCenter) {
		Assert.notNull(socialCenter);
		administratorService.checkIfAdministrator();

		Collection<Resource> resources = socialCenter.getResources();

		Assert.isTrue(resources == null || resources.isEmpty());

		for (Manager manager : socialCenter.getManagers()) {
			manager.setSocialCenter(null);
			managerService.save(manager);
		}

		for (Assessment assessment : socialCenter.getAssessments()) {
			for (Like like : assessment.getLikes()) {
				Client client = like.getClient();
				Collection<Like> likes = client.getLikes();
				likes.remove(like);
				client.setLikes(likes);
				clientService.save(client);
				likeService.delete(like);
			}
			Client client = assessment.getClient();
			Collection<Assessment> assessments = client.getAssessments();
			assessments.remove(assessment);
			client.setAssessments(assessments);
			clientService.save(client);
			assessmentService.delete(assessment);
		}

		for (Request request : socialCenter.getRequests()) {
			Client client = request.getClient();
			Collection<Request> requests = client.getRequests();
			requests.remove(request);
			client.setRequests(requests);
			clientService.save(client);
			requestService.delete(request);
		}
		socialCenter.setManagers(null);
		socialCenterRepository.delete(socialCenter);

	}

	public SocialCenter saveAndFlush(SocialCenter socialCenter) {
		Assert.notNull(socialCenter);
		Assert.isTrue(socialCenter.getLatitude() <= 90 && socialCenter.getLatitude() >= -90 && socialCenter.getLongitude() <= 90 && socialCenter.getLongitude() >= -90);
		administratorService.checkIfAdministrator();
		updateManagers(socialCenter);

		return socialCenterRepository.saveAndFlush(socialCenter);

	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Book> findBooksResources(SocialCenter socialCenter) {

		return socialCenterRepository.findBooksResources(socialCenter.getId());
	}

	public Collection<ElectronicDevice> findElectronicDevicesResources(SocialCenter socialCenter) {

		return socialCenterRepository.findElectronicDevicesResources(socialCenter.getId());
	}

	public Collection<Room> findRoomsResources(SocialCenter socialCenter) {

		return socialCenterRepository.findRoomsResources(socialCenter.getId());
	}

	public Collection<SocialCenter> findAllOrderByRatioLikesDislikes() {

		return socialCenterRepository.findAllOrderByRatioLikesDislikes();
	}

	public Collection<Assessment> getAssessmentsWithoutClientBanned(Integer socialCenterId) {
		return socialCenterRepository.getAssessmentsWithoutClientBanned(socialCenterId);
	}

	//QUERY1:Lista de los centros sociales ordenados por el n�mero de recursos del que dispone descendentemente.
	public Collection<SocialCenter> getSocialCentersWithResources() {
		try {
			return socialCenterRepository.getSocialCentersWithResources();
		} catch (Exception e) {
			return new ArrayList<SocialCenter>();
		}
	}

	public Collection<SocialCenter> searchSocialCenters(String keyword) {
		Collection<SocialCenter> result = new ArrayList<SocialCenter>();
		try {
			result = socialCenterRepository.getSocialCentersByKeyword("%" + keyword + "%");
		} catch (Exception e) {
			return new ArrayList<SocialCenter>();
		}
		return result;
	}

	private void updateManagers(SocialCenter socialCenter) {
		Collection<Manager> managers = socialCenter.getManagers();

		Collection<Manager> oldManagers = managerService.managerWithoutSocialCenter(socialCenter.getId());

		if (oldManagers != null) {
			for (Manager manager : oldManagers) {
				manager.setSocialCenter(null);
				managerService.save(manager);
			}
		}

		if (managers != null) {
			for (Manager manager : managers) {
				manager.setSocialCenter(socialCenter);
				managerService.save(manager);
			}
		}
	}

	//QUERY: El centro social m�s valorado.
	public Collection<SocialCenter> getSocialCenterMoreValorate() {
		try {
			return socialCenterRepository.getSocialCenterMoreValorate();
		} catch (Exception e) {
			return new ArrayList<SocialCenter>();
		}
	}

}
