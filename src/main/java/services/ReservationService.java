
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import auxiliar.Horario;
import domain.Client;
import domain.Incidence;
import domain.Manager;
import domain.Reservation;
import domain.Resource;
import repositories.ReservationRepository;

@Service
@Transactional
public class ReservationService {

	public String[]					horasPredeterminadas	= {
		"10:00 - 12:00", "12:00 - 14:00", "14:00 - 16:00", "16:00 - 18:00", "18:00 - 20:00", "20:00 - 22:00",
	};

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ReservationRepository	reservationRepository;


	// Constructor methods --------------------------------------------------------------
	public ReservationService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private IncidenceService incidenceService;

	@Autowired
	private ResourceService		resourceService;

	@Autowired
	private ClientService		clientService;

	@Autowired
	private ManagerService		managerService;


	// Simple CRUD methods --------------------------------------------------------------

	public Reservation findOne(int reservationId) {
		Assert.isTrue(reservationId != 0);
		Reservation result;

		result = reservationRepository.findOne(reservationId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Reservation> findAll() {
		Collection<Reservation> result;

		result = reservationRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Reservation reservation) {
		Assert.notNull(reservation);

		reservationRepository.save(reservation);
	}

	public Reservation create() {
		Reservation reservation = new Reservation();

		return reservation;
	}

	public void delete(Reservation reservation) {
		Assert.notNull(reservation);

		Client client = clientService.findByPrincipal();
		Assert.notNull(client);
		Assert.isTrue(client.getId() == reservation.getClient().getId());

		for (Incidence i : reservation.getIncidences())
			incidenceService.delete(i);

		reservationRepository.delete(reservation);
	}

	public Reservation saveAndFlush(Reservation reservation) {
		Assert.notNull(reservation);

		return reservationRepository.saveAndFlush(reservation);
	}

	public Boolean findClientOk(Integer clientId, Integer resourceId) {
		Reservation reservation = reservationRepository.findReservation(clientId, resourceId);
		if (reservation != null)
			return true;
		else
			return false;
	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Reservation> activeReservations(Integer clientId) {
		return reservationRepository.activeReservation(clientId);
	}

	public Collection<Reservation> inactiveReservations(Integer clientId) {
		return reservationRepository.inactiveReservation(clientId);
	}

	//DOMINGO: 1, LUNES: 2, MARTES: 3, X: 4, JUEVES: 5, VIERNES: 6, SABADO: 7
	public Collection<Horario> horariosPorDía(int dia, int resourceId) {
		Collection<Horario> res = new ArrayList<>();
		Calendar now = Calendar.getInstance();
		int currentDay = now.get(Calendar.DAY_OF_WEEK);
		if (currentDay == 1) {
			currentDay = 8;
		}

		if (currentDay < dia) {
			res = allAvailable(dia, resourceId);
		} else if (currentDay == dia) {
			res = sameDay(dia, resourceId);
		} else {
			res = allNotAvailable();
		}

		return res;
	}

	private Collection<Horario> allNotAvailable() {
		Collection<Horario> res = new ArrayList<>();

		for (String hora : horasPredeterminadas) {
			Horario horario = new Horario();
			horario.setHora(hora);
			horario.setDisponible(false);
			res.add(horario);
		}

		return res;
	}

	private Collection<Horario> allAvailable(int dia, int resourceId) {
		if (dia == 8) {
			dia = 1;
		}
		Collection<Horario> res = new ArrayList<>();
		Calendar now = Calendar.getInstance();

		now.set(Calendar.MINUTE, 00);
		now.set(Calendar.SECOND, 00);
		now.set(Calendar.DAY_OF_WEEK, dia);
		for (String hora : horasPredeterminadas) {
			int horaInicio = Integer.parseInt(hora.substring(0, 2));
			now.set(Calendar.HOUR_OF_DAY, horaInicio);
			Date date = now.getTime();
			Horario horario = new Horario();
			horario.setHora(hora);
			Collection<Reservation> reservations = findReservationByDate(date, resourceId);
			if (reservations == null || reservations.isEmpty()) {
				horario.setDisponible(true);
			} else {
				horario.setDisponible(false);
			}
			res.add(horario);
		}

		return res;
	}

	private Collection<Horario> sameDay(int dia, int resourceId) {
		if (dia == 8) {
			dia = 1;
		}
		Collection<Horario> res = new ArrayList<>();
		Calendar now = Calendar.getInstance();
		int currentHour = now.get(Calendar.HOUR_OF_DAY);

		now.set(Calendar.MINUTE, 00);
		now.set(Calendar.SECOND, 00);
		for (String hora : horasPredeterminadas) {
			int horaInicio = Integer.parseInt(hora.substring(0, 2));
			now.set(Calendar.HOUR_OF_DAY, horaInicio);
			Date date = now.getTime();
			Horario horario = new Horario();
			horario.setHora(hora);
			if (horaInicio > currentHour) {
				Collection<Reservation> reservations = findReservationByDate(date, resourceId);
				if (reservations == null || reservations.isEmpty()) {
					horario.setDisponible(true);
				} else {
					horario.setDisponible(false);
				}
			} else {
				horario.setDisponible(false);
			}
			res.add(horario);
		}

		return res;
	}

	public Collection<Reservation> findReservationByDate(Date date, int resourceId) {
		return reservationRepository.findReservationByDate(date, resourceId);
	}

	public Reservation createReservationRoom(String codigo, int resourceId) {

		Client client = clientService.findByPrincipal();
		Assert.notNull(client);
		Assert.isTrue(!client.getBanned());
		Assert.isTrue(!clientService.hasPenalty(client.getId()));
		Assert.isTrue(activeReservations(client.getId()).size() < 3);

		Assert.isTrue(resourceId != 0);
		Assert.notNull(codigo);
		Assert.isTrue(!resourceService.hasRoomReservationActive());

		String[] strings = codigo.split("-");

		Calendar start = Calendar.getInstance();

		int horaInicio = Integer.parseInt(strings[1].substring(0, 2));
		int horaFin = Integer.parseInt(strings[2].substring(1, 3));
		start.set(Calendar.HOUR_OF_DAY, horaInicio);
		start.set(Calendar.MINUTE, 00);
		start.set(Calendar.SECOND, 00);

		switch (strings[0]) {
		case "L":
			start.set(Calendar.DAY_OF_WEEK, 2);
			break;
		case "M":
			start.set(Calendar.DAY_OF_WEEK, 3);
			break;
		case "X":
			start.set(Calendar.DAY_OF_WEEK, 4);
			break;
		case "J":
			start.set(Calendar.DAY_OF_WEEK, 5);
			break;
		case "V":
			start.set(Calendar.DAY_OF_WEEK, 6);
			break;
		case "S":
			start.set(Calendar.DAY_OF_WEEK, 7);
			break;
		case "D":
			start.set(Calendar.DAY_OF_WEEK, 1);
			break;
		}
		Date startDate = start.getTime();

		start.set(Calendar.HOUR_OF_DAY, horaFin);
		Date endDate = start.getTime();


		Resource resource = resourceService.findOne(resourceId);
		Collection<Incidence> incidences = new ArrayList<>();

		Reservation reservation = create();
		reservation.setStartDate(startDate);
		reservation.setEndDate(endDate);
		reservation.setClient(client);
		reservation.setResource(resource);
		reservation.setIncidences(incidences);

		return saveAndFlush(reservation);
	}

	public Collection<Reservation> allActiveReservations() {
		return reservationRepository.allActiveReservation();
	}

	public void updateRoomReservation() {
		Date date = new Date();
		for (Reservation reservation : allActiveReservations()) {
			if (reservation.getResource().getClass().getName().equals("domain.Room") && reservation.getReturnDate() == null && reservation.getEndDate().before(date)) {
				reservation.setReturnDate(reservation.getEndDate());
				save(reservation);
			}
		}
	}

	public void finalizeReservation(int reservationId) {
		Reservation reservation = findOne(reservationId);
		Assert.isTrue(managerService.isManagerLogged());
		Manager manager = managerService.findByPrincipal();
		Assert.notNull(reservation);
		Assert.isTrue(reservation.getResource().getSocialCenter().getId() == manager.getSocialCenter().getId());
		Date date = new Date();
		reservation.setReturnDate(date);
		save(reservation);
		clientService.compruebaPenalizaciones(reservation.getClient().getId());
	}

	public Collection<Reservation> activeReservationBySocialCenter(int socialCenterId) {
		return reservationRepository.activeReservationBySocialCenter(socialCenterId);
	}

	public Collection<Reservation> allReservationBySocialCenter(int socialCenterId) {
		return reservationRepository.allReservationBySocialCenter(socialCenterId);
	}

	public Boolean timeIsCorrect() {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return (hour >= 10 && hour < 22);
	}

}
