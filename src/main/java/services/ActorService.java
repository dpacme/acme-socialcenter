package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import repositories.ActorRepository;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class ActorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ActorRepository actorRepository;


	// Constructor methods --------------------------------------------------------------
	public ActorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Actor findOne(int actorId) {
		Assert.isTrue(actorId != 0);
		Actor result;

		result = this.actorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Actor> findAll() {
		Collection<Actor> result;

		result = this.actorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Actor actor) {
		Assert.notNull(actor);
		this.actorRepository.save(actor);
	}

	public void delete(Actor actor) {
		Assert.notNull(actor);
		this.actorRepository.delete(actor);
	}

	public Actor saveAndFlush(Actor actor) {
		Assert.notNull(actor);

		return this.actorRepository.saveAndFlush(actor);
	}

	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.actorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Other bussines methods -----------------------------------------------------

	public String createTicker() {
		final String abecedario = "TRWAGMYFPDXBNJZSQVHLCKE";
		final String numeracion = "1234567890";
		final String res = RandomStringUtils.random(4, numeracion) + "-" + RandomStringUtils.random(4, abecedario);
		return res;
	}

}
