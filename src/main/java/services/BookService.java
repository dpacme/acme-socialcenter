
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Book;
import domain.Category;
import domain.Comment;
import domain.Reservation;
import domain.SocialCenter;
import repositories.BookRepository;

@Service
@Transactional
public class BookService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private BookRepository bookRepository;


	// Constructor methods --------------------------------------------------------------
	public BookService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private CategoryService	categoryService;

	@Autowired
	private ReservationService	reservationService;

	// Simple CRUD methods --------------------------------------------------------------

	public Book findOne(final int bookId) {
		Assert.isTrue(bookId != 0);
		Book result;

		result = this.bookRepository.findOne(bookId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Book> findAll() {
		Collection<Book> result;

		result = this.bookRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Book book) {
		Assert.notNull(book);

		if (book.getCategories() == null)
			book.setCategories(new ArrayList<Category>());
		Book res = this.bookRepository.save(book);
		if (book.getCategories() != null)
			for (Category c : res.getCategories()) {
				c.getBooks().add(res);
				this.categoryService.save(c);
			}
	}

	public Book create(SocialCenter socialCenter) {
		Book book = new Book();
		book.setSocialCenter(socialCenter);
		book.setComments(new ArrayList<Comment>());
		book.setReservations(new ArrayList<Reservation>());
		book.setTicker(this.resourceService.createTicker());

		return book;
	}

	public void delete(final Book book) {
		Assert.notNull(book);

		for (Category c : book.getCategories()) {
			c.getBooks().remove(book);
			this.categoryService.save(c);
		}

		for (Reservation r : book.getReservations())
			this.reservationService.delete(r);

		this.bookRepository.delete(book);
	}

	public Book saveAndFlush(final Book book) {
		Assert.notNull(book);


		return this.bookRepository.saveAndFlush(book);
	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Book> getBooksPublishedLastMonth() {
		return this.bookRepository.getBooksPublishedLastMonth();
	}

	public void comprobacion(Book book) {
		Assert.isTrue(book.getIsbn() != "");
		Assert.isTrue(book.getTitle() != "");
		Assert.isTrue(book.getAuthor() != "");
		Assert.isTrue(book.getEditorial() != "");
		Assert.isTrue(book.getDescription() != "");
		Assert.isTrue(book.getPublicationDate() != null);
		Assert.isTrue(book.getPublicationDate().before(new Date()));
	}

}
