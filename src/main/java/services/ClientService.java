
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Actor;
import domain.Assessment;
import domain.Client;
import domain.Comment;
import domain.Like;
import domain.Penalty;
import domain.Request;
import domain.Reservation;
import forms.ClientForm;
import repositories.ClientRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class ClientService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ClientRepository clientRepository;


	// Constructor methods --------------------------------------------------------------
	public ClientService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService		actorService;

	@Autowired
	private ReservationService	reservationService;

	@Autowired
	private PenaltyService		penaltyService;


	// Simple CRUD methods --------------------------------------------------------------

	public Client findOne(int clientId) {
		Assert.isTrue(clientId != 0);
		Client result;

		result = this.clientRepository.findOne(clientId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Client> findAll() {
		Collection<Client> result;

		result = this.clientRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Client client) {
		Assert.notNull(client);
		this.clientRepository.save(client);
	}

	public Client create() {
		Client client = new Client();
		client.setBanned(false);
		client.setLikes(new ArrayList<Like>());
		client.setAssessments(new ArrayList<Assessment>());
		client.setRequests(new ArrayList<Request>());
		client.setComments(new ArrayList<Comment>());
		client.setReservations(new ArrayList<Reservation>());
		client.setActivities(new ArrayList<Activity>());
		client.setPenalties(new ArrayList<Penalty>());
		return client;
	}

	public void delete(Client client) {
		Assert.notNull(client);
		this.clientRepository.delete(client);
	}

	public Client saveAndFlush(Client client) {
		Assert.notNull(client);

		return this.clientRepository.saveAndFlush(client);
	}

	public Client findByPrincipal() {
		Client result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.clientRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/**
	 * M�todo que comprueba si existe un cliente logado
	 */
	public boolean isClientLogged() {
		Client result;
		boolean logado = false;
		UserAccount userAccount;
		try {
			userAccount = LoginService.getPrincipal();
			Assert.notNull(userAccount);
			result = this.clientRepository.findByUserAccount(userAccount);
			logado = result != null;
		} catch (final Exception e) {
			logado = false;
		}
		return logado;
	}

	// Other bussines methods -----------------------------------------------------

	public Client reconstruct(ClientForm a) {
		Client client = this.create();

		UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		client.setId(0);
		client.setVersion(0);
		client.setName(a.getName());
		client.setSurname(a.getSurname());
		client.setEmail(a.getEmail());
		client.setPhone(a.getPhone());
		client.setPostalAddress(a.getPostalAddress());
		client.setPicture(a.getPicture());
		client.setIdentifier(a.getDni());

		client.setUserAccount(account);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return client;
	}

	public List<String> comprobacionEditarListErrores(Client client) {
		List<String> listaErrores = new ArrayList<String>();
		if (client.getPostalAddress() != "" && !client.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (client.getPhone() != "" && !client.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public void saveForm(Client client) {

		Assert.notNull(client);
		String password;
		Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		Collection<Authority> auths = new ArrayList<>();
		Authority auth = new Authority();
		auth.setAuthority("CLIENT");
		auths.add(auth);

		password = client.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		client.getUserAccount().setPassword(password);

		client.getUserAccount().setAuthorities(auths);

		client = this.clientRepository.saveAndFlush(client);
	}

	//QUERY4 Lista de los clientes ordenado por el n�mero de reservas vigentes.
	public Collection<Client> getClientsOrderByReservations() {
		try {
			return this.clientRepository.getClientsOrderByReservations();
		} catch (Exception e) {
			return new ArrayList<Client>();
		}
	}

	public Collection<Client> getClientsByActivity(Activity activity) {
		try {
			return this.clientRepository.getClientsByActivity(activity);
		} catch (Exception e) {
			return new ArrayList<Client>();
		}
	}

	public Integer numClientsBanned() {
		return this.clientRepository.numClientsBanned();
	}

	//QUERY El cliente que tenga m�s penalizaciones.
	public Collection<Client> getClientWithMorePenalties() {
		try {
			return this.clientRepository.getClientWithMorePenalties();
		} catch (Exception e) {
			return new ArrayList<Client>();
		}

	}

	//QUERY Lista de clientes bloqueados actualmente en el sistema.
	public Collection<Client> findBannedClients() {
		try {
			return this.clientRepository.findBannedClients();
		} catch (Exception e) {
			return new ArrayList<Client>();
		}
	}

	public Boolean hasPenalty(int clientId) {
		Date date = new Date();
		Collection<Penalty> penalties = this.clientRepository.hasPenalty(date, clientId);
		return (penalties != null && !penalties.isEmpty());
	}
	public void compruebaPenalizaciones(Integer clientId) {
		Client c = this.findOne(clientId);
		Date fechaActual = new Date();

		try {

			//Si no tiene penalizacion activa, se comprueba para penalizar si es necesario

			boolean penalizado = false;

			for(Penalty p:c.getPenalties())
				if(p.getStartDate().before(fechaActual) && p.getEndDate().after(fechaActual))
					penalizado = true;


			if(!penalizado) {

				boolean necesarioPenalizar = false;

				for (Reservation r : c.getReservations()) {
					if (!r.isChecked())
						//Se comprueban las reservas que ya hayan vencido
						if (r.getEndDate().before(fechaActual))
							if (r.getReturnDate() == null || r.getEndDate().before(r.getReturnDate()))
								necesarioPenalizar = true;
					//Se marca la reserva como comprobada
					r.setChecked(true);
					this.reservationService.save(r);
				}

				if (necesarioPenalizar) {
					Penalty p = this.penaltyService.create();

					p.setClient(c);

					p.setStartDate(fechaActual);

					//La penalizaci�n es de 7 d�as

					Calendar calendar = Calendar.getInstance();
					calendar.setTime(fechaActual);

					calendar.add(Calendar.DAY_OF_YEAR, 7);

					p.setEndDate(calendar.getTime());

					this.penaltyService.save(p);

				}
			}
		} catch (Exception e) {

		}

	}

	public void comprobacion(Client client) {
		if (client.getPostalAddress() != "")
			Assert.isTrue(client.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (client.getPhone() != "")
			Assert.isTrue(client.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(client.getUserAccount().getUsername()));
		for (Actor actor : this.findAll())
			Assert.isTrue(!actor.getIdentifier().equals(client.getIdentifier()), "Debe ser unico");
	}

	public void comprobacion2(Client client) {
		if (client.getPostalAddress() != "")
			Assert.isTrue(client.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (client.getPhone() != "")
			Assert.isTrue(client.getPhone().matches("^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
	}

}
