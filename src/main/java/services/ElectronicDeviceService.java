
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Comment;
import domain.ElectronicDevice;
import domain.Reservation;
import domain.SocialCenter;
import repositories.ElectronicDeviceRepository;

@Service
@Transactional
public class ElectronicDeviceService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ElectronicDeviceRepository electronicDeviceRepository;


	// Constructor methods --------------------------------------------------------------
	public ElectronicDeviceService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private ReservationService	reservationService;


	// Simple CRUD methods --------------------------------------------------------------

	public ElectronicDevice findOne(final int electronicDeviceId) {
		Assert.isTrue(electronicDeviceId != 0);
		ElectronicDevice result;

		result = this.electronicDeviceRepository.findOne(electronicDeviceId);
		Assert.notNull(result);

		return result;
	}

	public Collection<ElectronicDevice> findAll() {
		Collection<ElectronicDevice> result;

		result = this.electronicDeviceRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final ElectronicDevice electronicDevice) {
		Assert.notNull(electronicDevice);

		this.electronicDeviceRepository.save(electronicDevice);
	}

	public ElectronicDevice create(SocialCenter socialCenter) {
		ElectronicDevice electronicDevice = new ElectronicDevice();
		electronicDevice.setSocialCenter(socialCenter);
		electronicDevice.setComments(new ArrayList<Comment>());
		electronicDevice.setReservations(new ArrayList<Reservation>());
		electronicDevice.setTicker(this.resourceService.createTicker());

		return electronicDevice;
	}

	public void delete(final ElectronicDevice electronicDevice) {
		Assert.notNull(electronicDevice);

		for (Reservation r : electronicDevice.getReservations())
			this.reservationService.delete(r);

		this.electronicDeviceRepository.delete(electronicDevice);
	}

	public ElectronicDevice saveAndFlush(final ElectronicDevice electronicDevice) {
		Assert.notNull(electronicDevice);

		return this.electronicDeviceRepository.saveAndFlush(electronicDevice);
	}

	// Other bussines methods -----------------------------------------------------

	public void comprobacion(ElectronicDevice electronicDevice) {
		Assert.isTrue(electronicDevice.getBrand() != "");
		Assert.isTrue(electronicDevice.getModel() != "");
		Assert.isTrue(electronicDevice.getTypeDevice() != null);
	}

}
