
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Category;
import repositories.CategoryRepository;

@Service
@Transactional
public class CategoryService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CategoryRepository categoryRepository;


	// Constructor methods --------------------------------------------------------------
	public CategoryService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Category findOne(final int categoryId) {
		Assert.isTrue(categoryId != 0);
		Category result;

		result = categoryRepository.findOne(categoryId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Category> findAll() {
		Collection<Category> result;

		result = categoryRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Category category) {
		Assert.notNull(category);

		categoryRepository.save(category);
	}

	public Category create() {
		Category category = new Category();

		return category;
	}

	public void delete(final Category category) {
		Assert.notNull(category);

		categoryRepository.delete(category);
	}

	public Category saveAndFlush(final Category category) {
		Assert.notNull(category);

		return categoryRepository.saveAndFlush(category);
	}

	// Other bussines methods -----------------------------------------------------

}
