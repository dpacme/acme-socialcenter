package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Room extends Resource {

	private Integer	capacity;
	private Boolean	board;
	private Boolean	tv;


	@NotNull
	@Min(1)
	public Integer getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Boolean getBoard() {
		return this.board;
	}

	public void setBoard(Boolean board) {
		this.board = board;
	}

	public Boolean getTv() {
		return this.tv;
	}

	public void setTv(Boolean tv) {
		this.tv = tv;
	}


	// Relationships -------------------------------------------

	private Collection<ActivityReservation> activityReservations;


	@Valid
	@OneToMany(mappedBy = "room")
	public Collection<ActivityReservation> getActivityReservations() {
		return this.activityReservations;
	}

	public void setActivityReservations(final Collection<ActivityReservation> activityReservations) {
		this.activityReservations = activityReservations;
	}

}
