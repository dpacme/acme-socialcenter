package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import enums.TypeDevice;

@Entity
@Access(AccessType.PROPERTY)
public class ElectronicDevice extends Resource {

	private String		brand;
	private String		model;
	private TypeDevice	typeDevice;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public TypeDevice getTypeDevice() {
		return this.typeDevice;
	}

	public void setTypeDevice(TypeDevice typeDevice) {
		this.typeDevice = typeDevice;
	}

}
