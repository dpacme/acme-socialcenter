
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	// Relationships -------------------------------------------

	private SocialCenter			socialCenter;
	private Collection<Incidence>	incidences;


	@Valid
	@ManyToOne(optional = true)
	public SocialCenter getSocialCenter() {
		return socialCenter;
	}

	public void setSocialCenter(SocialCenter socialCenter) {
		this.socialCenter = socialCenter;
	}

	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Incidence> getIncidences() {
		return incidences;
	}

	public void setIncidences(final Collection<Incidence> incidences) {
		this.incidences = incidences;
	}

}
