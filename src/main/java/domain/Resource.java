
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Resource extends DomainEntity {

	private String	ticker;
	private String	picture;


	@NotBlank
	@Column(unique = true)
	@Pattern(regexp = "^(\\w{5}\\-\\d{5}$)")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(final String ticker) {
		this.ticker = ticker;
	}

	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}


	// Relationships -------------------------------------------

	private Collection<Reservation>	reservations;
	private Collection<Comment>		comments;
	private SocialCenter			socialCenter;


	@Valid
	@OneToMany(mappedBy = "resource")
	public Collection<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(final Collection<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Valid
	@OneToMany(mappedBy = "resource")
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@Valid
	@ManyToOne(optional = false)
	public SocialCenter getSocialCenter() {
		return this.socialCenter;
	}

	public void setSocialCenter(final SocialCenter socialCenter) {
		this.socialCenter = socialCenter;
	}

}
