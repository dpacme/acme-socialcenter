
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Client extends Actor {

	private String	picture;
	private Boolean	banned;


	@NotBlank
	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@NotNull
	public Boolean getBanned() {
		return this.banned;
	}

	public void setBanned(final Boolean banned) {
		this.banned = banned;
	}


	// Relationships -------------------------------------------

	private Collection<Like>		likes;
	private Collection<Assessment>	assessments;
	private Collection<Request>		requests;
	private Collection<Comment>		comments;
	private Collection<Reservation>	reservations;
	private Collection<Activity>	activities;
	private Collection<Penalty>		penalties;


	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Like> getLikes() {
		return this.likes;
	}

	public void setLikes(final Collection<Like> likes) {
		this.likes = likes;
	}

	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Assessment> getAssessments() {
		return this.assessments;
	}

	public void setAssessments(final Collection<Assessment> assessments) {
		this.assessments = assessments;
	}

	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(final Collection<Request> requests) {
		this.requests = requests;
	}

	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(final Collection<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Valid
	@ManyToMany
	public Collection<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(final Collection<Activity> activities) {
		this.activities = activities;
	}

	@Valid
	@OneToMany(mappedBy = "client")
	public Collection<Penalty> getPenalties() {
		return this.penalties;
	}

	public void setPenalties(final Collection<Penalty> penalties) {
		this.penalties = penalties;
	}

}
