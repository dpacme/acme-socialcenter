
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class SocialCenter extends DomainEntity {

	private String	name;
	private String	city;
	private Double	latitude;
	private Double	longitude;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	@NotNull
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(final Double latitude) {
		this.latitude = latitude;
	}

	@NotNull
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}


	// Relationships -------------------------------------------

	private Collection<Manager>		managers;
	private Collection<Assessment>	assessments;
	private Collection<Request>		requests;
	private Collection<Resource>	resources;


	@Valid
	@NotNull
	@ManyToMany(cascade = CascadeType.ALL)
	public Collection<Manager> getManagers() {
		return managers;
	}

	public void setManagers(final Collection<Manager> managers) {
		this.managers = managers;
	}

	@Valid
	@OneToMany(mappedBy = "socialCenter")
	public Collection<Assessment> getAssessments() {
		return assessments;
	}

	public void setAssessments(final Collection<Assessment> assessments) {
		this.assessments = assessments;
	}

	@Valid
	@OneToMany(mappedBy = "socialCenter")
	public Collection<Request> getRequests() {
		return requests;
	}

	public void setRequests(final Collection<Request> requests) {
		this.requests = requests;
	}

	@Valid
	@OneToMany(mappedBy = "socialCenter")
	public Collection<Resource> getResources() {
		return resources;
	}

	public void setResources(final Collection<Resource> resources) {
		this.resources = resources;
	}

}
