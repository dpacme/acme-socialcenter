
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Likes")
public class Like extends DomainEntity {

	private Boolean likeMe;


	@NotNull
	public Boolean getLikeMe() {
		return likeMe;
	}

	public void setLikeMe(final Boolean likeMe) {
		this.likeMe = likeMe;
	}


	// Relationships -------------------------------------------

	private Client		client;
	private Assessment	assessment;


	@Valid
	@ManyToOne(optional = false)
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Valid
	@ManyToOne(optional = false)
	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}

}
