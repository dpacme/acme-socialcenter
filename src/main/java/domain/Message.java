
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Message extends DomainEntity {

	private String	text;
	private Date	mommentCreate;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMommentCreate() {
		return this.mommentCreate;
	}

	public void setMommentCreate(final Date mommentCreate) {
		this.mommentCreate = mommentCreate;
	}

	// Relationships -------------------------------------------


	private Incidence	incidence;
	private Actor		actorSend;
	private Actor		actorRecived;
	private Message		relatedTo;


	@Valid
	@ManyToOne(optional = false)
	public Incidence getIncidence() {
		return this.incidence;
	}

	public void setIncidence(final Incidence incidence) {
		this.incidence = incidence;
	}

	@Valid
	@ManyToOne(optional = false)
	public Actor getActorSend() {
		return this.actorSend;
	}

	public void setActorSend(final Actor actorSend) {
		this.actorSend = actorSend;
	}

	@Valid
	@ManyToOne(optional = false)
	public Actor getActorRecived() {
		return this.actorRecived;
	}

	public void setActorRecived(final Actor actorRecived) {
		this.actorRecived = actorRecived;
	}

	@Valid
	@OneToOne(optional = true)
	public Message getRelatedTo() {
		return this.relatedTo;
	}

	public void setRelatedTo(final Message relatedTo) {
		this.relatedTo = relatedTo;
	}

}
