
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Reservation extends DomainEntity {

	private Date	startDate;
	private Date	endDate;
	private Date	returnDate;
	private boolean	checked;


	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(final Date returnDate) {
		this.returnDate = returnDate;
	}


	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}



	// Relationships -------------------------------------------

	private Collection<Incidence>	incidences;
	private Client					client;
	private Resource				resource;


	@Valid
	@OneToMany(mappedBy = "reservation")
	public Collection<Incidence> getIncidences() {
		return incidences;
	}

	public void setIncidences(final Collection<Incidence> incidences) {
		this.incidences = incidences;
	}

	@Valid
	@ManyToOne(optional = false)
	public Client getClient() {
		return client;
	}

	public void setClient(final Client client) {
		this.client = client;
	}

	@Valid
	@ManyToOne(optional = false)
	public Resource getResource() {
		return resource;
	}

	public void setResource(final Resource resource) {
		this.resource = resource;
	}

}
