
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Activity extends DomainEntity {

	private Date	startDate;
	private Date	endDate;
	private String	title;
	private String	description;
	private Integer	seats;


	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@NotNull
	@Min(1)
	public Integer getSeats() {
		return this.seats;
	}

	public void setSeats(final Integer seats) {
		this.seats = seats;
	}


	// Relationships -------------------------------------------

	private Collection<Client>	clients;
	private Monitor				monitor;
	private ActivityReservation	activityReservation;


	@Valid
	@ManyToMany
	public Collection<Client> getClients() {
		return this.clients;
	}

	public void setClients(final Collection<Client> clients) {
		this.clients = clients;
	}

	@Valid
	@ManyToOne(optional = false)
	public Monitor getMonitor() {
		return this.monitor;
	}

	public void setMonitor(final Monitor monitor) {
		this.monitor = monitor;
	}

	@Valid
	@OneToOne(optional = true)
	public ActivityReservation getActivityReservation() {
		return this.activityReservation;
	}

	public void setActivityReservation(final ActivityReservation activityReservation) {
		this.activityReservation = activityReservation;
	}

}
