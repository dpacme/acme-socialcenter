
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Assessment extends DomainEntity {

	private String	title;
	private String	comment;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}


	// Relationships -------------------------------------------

	private Client				client;
	private SocialCenter		socialCenter;
	private Collection<Like>	likes;


	@Valid
	@ManyToOne(optional = false)
	public Client getClient() {
		return this.client;
	}

	public void setClient(final Client client) {
		this.client = client;
	}

	@Valid
	@ManyToOne(optional = false)
	public SocialCenter getSocialCenter() {
		return this.socialCenter;
	}

	public void setSocialCenter(final SocialCenter socialCenter) {
		this.socialCenter = socialCenter;
	}

	@Valid
	@OneToMany(mappedBy = "assessment")
	public Collection<Like> getLikes() {
		return this.likes;
	}

	public void setLikes(final Collection<Like> likes) {
		this.likes = likes;
	}

}
