
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Resource;
import repositories.ResourceRepository;

@Component
@Transactional
public class StringToResourceConverter implements Converter<String, Resource> {

	@Autowired
	ResourceRepository resourceRepository;


	@Override
	public Resource convert(final String text) {
		Resource res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.resourceRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}

