
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Client;

@Component
@Transactional
public class ClientToStringConverter implements Converter<Client, String> {

	@Override
	public String convert(final Client client) {
		String res;

		if (client == null)
			res = null;
		else
			res = String.valueOf(client.getId());

		return res;

	}
}
