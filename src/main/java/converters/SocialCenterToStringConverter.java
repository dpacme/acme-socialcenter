
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.SocialCenter;

@Component
@Transactional
public class SocialCenterToStringConverter implements Converter<SocialCenter, String> {

	@Override
	public String convert(final SocialCenter socialCenter) {
		String res;

		if (socialCenter == null)
			res = null;
		else
			res = String.valueOf(socialCenter.getId());

		return res;

	}
}
