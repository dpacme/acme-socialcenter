
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Penalty;

@Component
@Transactional
public class PenaltyToStringConverter implements Converter<Penalty, String> {

	@Override
	public String convert(final Penalty penalty) {
		String res;

		if (penalty == null)
			res = null;
		else
			res = String.valueOf(penalty.getId());

		return res;

	}
}
