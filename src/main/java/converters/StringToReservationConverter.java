
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Reservation;
import repositories.ReservationRepository;

@Component
@Transactional
public class StringToReservationConverter implements Converter<String, Reservation> {

	@Autowired
	ReservationRepository reservationRepository;


	@Override
	public Reservation convert(final String text) {
		Reservation res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.reservationRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
