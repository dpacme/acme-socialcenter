
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.ElectronicDevice;

@Component
@Transactional
public class ElectronicDeviceToStringConverter implements Converter<ElectronicDevice, String> {

	@Override
	public String convert(final ElectronicDevice electronicDevice) {
		String res;

		if (electronicDevice == null)
			res = null;
		else
			res = String.valueOf(electronicDevice.getId());

		return res;

	}
}
