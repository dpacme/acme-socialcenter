
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Resource;

@Component
@Transactional
public class ResourceToStringConverter implements Converter<Resource, String> {

	@Override
	public String convert(final Resource resource) {
		String res;

		if (resource == null)
			res = null;
		else
			res = String.valueOf(resource.getId());

		return res;

	}
}
