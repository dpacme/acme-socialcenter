
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Monitor;
import repositories.MonitorRepository;

@Component
@Transactional
public class StringToMonitorConverter implements Converter<String, Monitor> {

	@Autowired
	MonitorRepository monitorRepository;


	@Override
	public Monitor convert(final String text) {
		Monitor res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.monitorRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
