
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Incidence;

@Component
@Transactional
public class IncidenceToStringConverter implements Converter<Incidence, String> {

	@Override
	public String convert(final Incidence incidence) {
		String res;

		if (incidence == null)
			res = null;
		else
			res = String.valueOf(incidence.getId());

		return res;

	}
}
