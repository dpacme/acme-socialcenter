
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Book;

@Component
@Transactional
public class BookToStringConverter implements Converter<Book, String> {

	@Override
	public String convert(final Book book) {
		String res;

		if (book == null)
			res = null;
		else
			res = String.valueOf(book.getId());

		return res;

	}
}
