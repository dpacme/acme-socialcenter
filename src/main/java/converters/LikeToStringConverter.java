
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Like;

@Component
@Transactional
public class LikeToStringConverter implements Converter<Like, String> {

	@Override
	public String convert(final Like like) {
		String res;

		if (like == null)
			res = null;
		else
			res = String.valueOf(like.getId());

		return res;

	}
}
