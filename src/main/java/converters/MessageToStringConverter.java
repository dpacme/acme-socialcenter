
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Monitor;

@Component
@Transactional
public class MessageToStringConverter implements Converter<Monitor, String> {

	@Override
	public String convert(final Monitor monitor) {
		String res;

		if (monitor == null)
			res = null;
		else
			res = String.valueOf(monitor.getId());

		return res;

	}
}
