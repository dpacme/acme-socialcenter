
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.ActivityReservation;

@Component
@Transactional
public class ActivityReservationToStringConverter implements Converter<ActivityReservation, String> {

	@Override
	public String convert(final ActivityReservation activityReservation) {
		String res;

		if (activityReservation == null)
			res = null;
		else
			res = String.valueOf(activityReservation.getId());

		return res;

	}
}
