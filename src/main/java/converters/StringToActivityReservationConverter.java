
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.ActivityReservation;
import repositories.ActivityReservationRepository;

@Component
@Transactional
public class StringToActivityReservationConverter implements Converter<String, ActivityReservation> {

	@Autowired
	ActivityReservationRepository activityReservationRepository;


	@Override
	public ActivityReservation convert(final String text) {
		ActivityReservation res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.activityReservationRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
