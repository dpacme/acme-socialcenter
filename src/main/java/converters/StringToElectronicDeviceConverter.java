
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.ElectronicDevice;
import repositories.ElectronicDeviceRepository;

@Component
@Transactional
public class StringToElectronicDeviceConverter implements Converter<String, ElectronicDevice> {

	@Autowired
	ElectronicDeviceRepository electronicDeviceRepository;


	@Override
	public ElectronicDevice convert(final String text) {
		ElectronicDevice res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.electronicDeviceRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
