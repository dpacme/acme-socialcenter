
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.SocialCenter;
import repositories.SocialCenterRepository;

@Component
@Transactional
public class StringToSocialCenterConverter implements Converter<String, SocialCenter> {

	@Autowired
	SocialCenterRepository socialCenterRepository;


	@Override
	public SocialCenter convert(final String text) {
		SocialCenter res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.socialCenterRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
