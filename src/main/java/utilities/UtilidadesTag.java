
package utilities;

import java.util.Collection;

import javax.servlet.jsp.tagext.SimpleTagSupport;

import domain.Like;
import security.UserAccount;

public class UtilidadesTag extends SimpleTagSupport {

	public static boolean contains(Collection<?> coll, Object o) {
		return coll.contains(o);
	}

	/**
	 * M�todo que recibe una collection de Likes y un UserAccount. Comprueba si alguno de los Likes de la collection tiene
	 * esa UserAccount y ve si esta a true o false
	 *
	 * @param coll
	 * @param o
	 * @return
	 */
	public static Boolean listLikesUserAccount(Collection<Like> coll, UserAccount o) {
		Boolean contiene = null;
		for (Like c : coll)
			if (c.getClient().getUserAccount().getId() == o.getId())

				if (c.getLikeMe() == true) {
					contiene = true;
					break;
				} else {
					contiene = false;
					break;
				}
		return contiene;
	}

}
