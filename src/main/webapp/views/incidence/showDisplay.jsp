<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>

	function seleccionarMensaje(idDiv,actorReceivedId,actorReceivedUsername){
		var elems = document.getElementsByClassName("chatmessageleft chatmessageleft_selected");
		while (elems.length)
			elems[0].className = elems[0].className.replace(/\bchatmessageleft_selected\b/g, "colorMessage");
		document.getElementById(idDiv).className = "chatmessageleft chatmessageleft_selected";
		
		document.getElementById("actorReceivedId").value = actorReceivedId;
		
		document.getElementById("messageFor").innerHTML = actorReceivedUsername;
		
		setSelectedValue(document.getElementById('actorReceivedSelect'),actorReceivedUsername);
	}
	
	function seleccionaManager(){
		var selector = document.getElementById('actorReceivedSelect');
		var labelManager = selector.options[selector.selectedIndex].innerHTML;
		
		document.getElementById("actorReceivedId").value =  selector.value;
		
		document.getElementById("messageFor").innerHTML = labelManager;
		
		var elems = document.getElementsByClassName("chatmessageleft chatmessageleft_selected");
		while (elems.length)
			elems[0].className = elems[0].className.replace(/\bchatmessageleft_selected\b/g, "colorMessage");	
		
	}
	
	function setSelectedValue(selectObj, valueToSet) {
	    for (var i = 0; i < selectObj.options.length; i++) {
	        if (selectObj.options[i].text== valueToSet) {
	            selectObj.options[i].selected = true;
	            return;
	        }
	    }
	}

</script>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

	<div>
		<ul>
			<li><b><spring:message code="incidence.title" />:</b>
				${incidence.title}</li>
			<li><b><spring:message code="incidence.description" />:</b>
				${incidence.description}</li>
			<li><b><spring:message code="incidence.status" />:</b>
				<jstl:if test="${incidence.status == 'open'}">
					<spring:message code="incidence.status.open"/>
				</jstl:if>
				<jstl:if test="${incidence.status == 'closed'}">
					<spring:message code="incidence.status.closed"/>
				</jstl:if>
				<jstl:if test="${incidence.status == 'solved'}">
					<spring:message code="incidence.status.solved"/>
				</jstl:if>			
			</li>
			<li><b><spring:message code="incidence.client" />:</b>
				${incidence.reservation.client.name} ${incidence.reservation.client.surname}</li>
		</ul>
	</div>
	
	<br>
	<fieldset style="width: 20%">
		<legend style="font-size: x-large" align="center"><b><spring:message code="incidence.reservation.data" /></b></legend>
		
		<ul>
			<li><b><spring:message code="reservation.startDate" />:</b>
				<fmt:formatDate value="${incidence.reservation.startDate}" pattern="dd/MM/YYYY" /></li>
			<li><b><spring:message code="reservation.endDate" />:</b>
				<fmt:formatDate value="${incidence.reservation.endDate}" pattern="dd/MM/YYYY" /></li>
			<li><b><spring:message code="reservation.returnDate" />:</b>
				<fmt:formatDate value="${incidence.reservation.returnDate}" pattern="dd/MM/YYYY" /></li>
		</ul>
		<acme:button href="reservation/showDisplay.do?reservationId=${incidence.reservation.id}"
			name="see" code="incidence.reservation.see" />
	</fieldset>
	
<br>
<fieldset style="width: 50%">
	<legend style="font-size: x-large" align="center"><b><spring:message code="incidence.messages" /></b></legend>
	<div>
		<div style="font-family: verdana" class="inline chatmessageleft">Manager</div>
		<div style="font-family: verdana" class="chatmessageright">Cliente</div>
	</div>
	<br>
	<div id="chatbox">
	    <div id="chatmessages">
	    	<jstl:forEach items="${messages}" var="message">
	    		<security:authorize access="hasRole('CLIENT')">
	    			<jstl:if test="${message.getActorSend().getClass().getName() == 'domain.Manager'}">
	    					    		
		    			<div id="divMessage${message.id}" class="chatmessageleft colorMessage" 
		    				onclick="seleccionarMensaje('divMessage${message.id}','${message.getActorSend().getId()}','${message.getActorSend().getUserAccount().getUsername()}')">
		    				<i><b>(<spring:message code="incidence.message.written.from" /> ${message.getActorSend().getUserAccount().getUsername()})</b></i>
		    				<br>
		    				${message.text}</div>
		    				
		    		</jstl:if>
		    		<jstl:if test="${message.getActorSend().getClass().getName() == 'domain.Client'}">
		    		
		    			<div class="chatmessageright">	    			
						<i><b>(<spring:message code="incidence.message.for.user" /> ${message.getActorRecived().getUserAccount().getUsername()})</b></i>
						<br>
		    			${message.text}</div>
		    			
		    		</jstl:if>
	    		</security:authorize>
	    		<security:authorize access="!hasRole('CLIENT')">
	    			<jstl:if test="${message.getActorSend().getClass().getName() == 'domain.Manager'}">
	    					    		
		    			<div id="divMessage${message.id}" class="chatmessageleft_nohover colorMessage">
		    				<i><b>(<spring:message code="incidence.message.written.from" /> ${message.getActorSend().getUserAccount().getUsername()})</b></i>
		    				<br>
		    				${message.text}</div>
		    				
		    		</jstl:if>
		    		<jstl:if test="${message.getActorSend().getClass().getName() == 'domain.Client'}">
		    		
		    			<div class="chatmessageright">	    			
						<i><b>(<spring:message code="incidence.message.for.user" /> ${message.getActorRecived().getUserAccount().getUsername()})</b></i>
						<br>
		    			${message.text}</div>
		    			
		    		</jstl:if>
	    		</security:authorize>
	    		
	    		
	    	</jstl:forEach>
	    </div>
	</div>
	<br>
	<security:authorize access="hasRole('CLIENT')">
	<jstl:if test="${incidence.status == 'open' and not empty managers}">
		<fieldset>
			<legend>
				<spring:message code="incidence.message" />
			</legend>
			<form method="POST" action="incidence/sendMessage.do">

				<spring:message code="incidence.text" />
				<br>
				<spring:message code="incidence.text" var="textPlaceholder" />
				<input type="text" id="text" name="text" maxlength="255" style="width: 60%;"
					placeholder="${textPlaceholder}" required="required"/> <br />
				
				<select id="actorReceivedSelect" onchange="seleccionaManager()">
					<jstl:forEach var="r" items="${managers}">
						<jstl:choose>
							<jstl:when test="${r.id == actorReceivedId}">
								<option selected="selected" value="${r.id}">${r.userAccount.username}</option>
							</jstl:when>
							<jstl:otherwise>
								<option value="${r.id}">${r.userAccount.username}</option>
							</jstl:otherwise>
						</jstl:choose>
						
					</jstl:forEach>
				</select>

				<input type="hidden" id="incidenceId" name="incidenceId" value="${incidence.id}" readonly="readonly" />
				<input type="hidden" id="actorReceivedId" name="actorReceivedId" value="${actorReceivedId}" readonly="readonly" />
				<input type="hidden" id="parentMessageId" name="parentMessageId" value="${parentMessageId}" readonly="readonly" />
				<br>
				<b>* <spring:message code="incidence.message.sendTo.info" /></b>
				<br>
				
				<input type="submit" name="sendMessage"
					value="<spring:message code="incidence.message.send"/>" />
			</form>
			
			<spring:message code="incidence.message.for.user" />: <div class="messageFor" id="messageFor">${messageFor}</div>
		</fieldset>
	</jstl:if>
	</security:authorize>
	<security:authorize access="hasRole('MANAGER')">
	<jstl:if test="${incidence.status == 'open'}">
		<fieldset>
			<legend>
				<spring:message code="incidence.message" />
			</legend>
			<form method="POST" action="incidence/sendMessage.do">

				<spring:message code="incidence.text" />
				<br>
				<spring:message code="incidence.text" var="textPlaceholder" />
				<input type="text" id="text" name="text" maxlength="255" style="width: 60%;"
					placeholder="${textPlaceholder}" required="required"/> <br />
				
				<input type="hidden" id="incidenceId" name="incidenceId" value="${incidence.id}" readonly="readonly" />
				<input type="hidden" id="actorReceivedId" name="actorReceivedId" value="${actorReceivedId}" readonly="readonly" />
				<input type="hidden" id="parentMessageId" name="parentMessageId" value="${parentMessageId}" readonly="readonly" />
				<br>
				<br>
				
				<input type="submit" name="sendMessage"
					value="<spring:message code="incidence.message.send"/>" />
			</form>
			
			<spring:message code="incidence.message.for.user" />: <div class="messageFor" id="messageFor">${messageFor}</div>
		</fieldset>
	</jstl:if>
	</security:authorize>
</fieldset>

<br/>
<security:authorize access="hasRole('MANAGER')">
<h2><b><spring:message code="incidence.statusChange" /></b></h2>
<br/>
			<jstl:choose>
				<jstl:when test="${incidence.status == 'open'}">
					<div class="inline">
							<acme:button href="incidence/manager/solvedIncidence.do?incidenceId=${incidence.id}"
							name="incidence" code="incidence.status.solved" />
					</div>
					<div class="inline">
							<acme:button href="incidence/manager/closedIncidence.do?incidenceId=${incidence.id}"
							name="incidence" code="incidence.status.closed" />
					</div>
				</jstl:when>
				<jstl:otherwise>
					<jstl:choose>
						<jstl:when test="${incidence.status == 'solved'}">
							<spring:message code="incidence.status.solved"/>
						</jstl:when>
						<jstl:otherwise>
							<spring:message code="incidence.status.closed"/>
						</jstl:otherwise>
					</jstl:choose>
				</jstl:otherwise>
			</jstl:choose>	
			
<br/>
</security:authorize>