<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />

<display:table name="incidences" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="incidence.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="incidence.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="true" />

	<spring:message code="incidence.status" var="statusHeader" />
	<display:column title="${statusHeader}" sortable="true">
		<jstl:if test="${row.status == 'open'}">
			<spring:message code="incidence.status.open"/>
		</jstl:if>
		<jstl:if test="${row.status == 'closed'}">
			<spring:message code="incidence.status.closed"/>
		</jstl:if>
		<jstl:if test="${row.status == 'solved'}">
			<spring:message code="incidence.status.solved"/>
		</jstl:if>
	</display:column>
	
	<spring:message code="incidence.manager" var="managerHeader" />
	<display:column title="${managerHeader}" sortable="true" >
		${row.manager.name} ${row.manager.surname}
	</display:column>
	
	<spring:message code="incidence.client" var="clientHeader" />
	<display:column title="${clientHeader}" sortable="true" >
		${row.reservation.client.name} ${row.reservation.client.surname}
	</display:column>
	
	<spring:message code="incidence.details" var="detailsHeader" />
	<display:column title="${detailsHeader}" sortable="true" >
		<acme:button href="incidence/showDisplay.do?incidenceId=${row.id}"
			name="see" code="incidence.details.see" />
	</display:column>

	<spring:message code="incidence.reservation" var="reservationDetailsHeader" />
	<display:column title="${reservationDetailsHeader}" sortable="true" >
		<acme:button href="reservation/showDisplay.do?reservationId=${row.reservation.id}"
			name="see" code="incidence.reservation.see" />
	</display:column>

</display:table>

<security:authorize access="hasRole('MANAGER')">
	<h2 style="color: blue;"><spring:message code="incidence.canCreate" /></h2>    
</security:authorize>