<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="incidence/manager/edit.do" modelAttribute="incidence">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="manager" />
	<form:hidden path="reservation" />
	<form:hidden path="status" />

	<!-- Campos obligatorios a rellenar -->

	<acme:textbox code="incidence.title" path="title" />
	
	<acme:textbox code="incidence.description" path="description" />

	<!-- Acciones -->

	<acme:submit name="save" code="incidence.save" />

	<acme:cancel url="reservation/showDisplay.do?reservationId=${incidence.reservation.id}"
		code="incidence.cancel" />

</form:form>

<br>
