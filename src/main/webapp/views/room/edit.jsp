<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="room">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="activityReservations" />
	<form:hidden path="reservations" />
	<form:hidden path="comments" />
	<form:hidden path="socialCenter" />
	<form:hidden path="ticker" />

	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="room.picture" path="picture" />

	<spring:message code="room.capacity" />
	<form:input type="number" min="0" step="1" path="capacity"/>
	<form:errors cssClass="error" path="capacity" />
	<br/>

	<form:label path="board">
        <spring:message code="room.board" />
    </form:label><b></b>
	<form:select path="board" style="margin-bottom: 10px">
		<form:option value="true"><spring:message code="room.yes"/></form:option>
		<form:option value="false"><spring:message code="room.no"/></form:option>
	</form:select>
	<form:errors class="error" path="board" />
	<br/>
	
	<form:label path="tv">
        <spring:message code="room.tv" />
    </form:label><b></b>
	<form:select path="tv" style="margin-bottom: 10px">
		<form:option value="true"><spring:message code="room.yes"/></form:option>
		<form:option value="false"><spring:message code="room.no"/></form:option>
	</form:select>
	<form:errors class="error" path="tv" />
	<br/>
	
	<!-- Acciones -->
	
	<acme:submit name="save" code="room.save"/>
	
	<acme:cancel url="socialCenter/manager/mySocialCenter.do" code="room.cancel"/>

</form:form>

<br>
