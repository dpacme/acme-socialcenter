<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="electronicDevice">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="reservations" />
	<form:hidden path="comments" />
	<form:hidden path="socialCenter" />
	<form:hidden path="ticker" />

	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="electronicDevice.picture" path="picture" />

	<acme:textbox code="electronicDevice.brand" path="brand" />
	
	<acme:textbox code="electronicDevice.model" path="model" />

	<form:label path="typeDevice">
        <spring:message code="electronicDevice.typeDevice" />
    </form:label><b></b>
	<form:select path="typeDevice" style="margin-bottom: 10px">
		<form:option value="COMPUTER"><spring:message code="electronicDevice.computer"/></form:option>
		<form:option value="TABLET"><spring:message code="electronicDevice.tablet"/></form:option>
		<form:option value="EBOOK"><spring:message code="electronicDevice.ebook"/></form:option>
	</form:select>
	<form:errors class="error" path="typeDevice" />
	<br/>
	
	<!-- Acciones -->
	
	<acme:submit name="save" code="electronicDevice.save"/>
	
	<acme:cancel url="socialCenter/manager/mySocialCenter.do" code="electronicDevice.cancel"/>

</form:form>

<br>
