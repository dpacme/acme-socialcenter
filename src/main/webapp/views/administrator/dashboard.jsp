<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<h2>
	<spring:message code="administrator.getSocialCentersWithResources" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="getSocialCentersWithResources" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="socialCenter.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="socialCenter.city" var="city" />
	<display:column property="city" title="${city}" sortable="true" />
	
	<spring:message code="socialCenter.latitude" var="latitude" />
	<display:column property="latitude" title="${latitude}" sortable="true" />
	
	<spring:message code="socialCenter.longitude" var="longitude" />
	<display:column property="longitude" title="${longitude}" sortable="true" />
	
	<spring:message code="socialCenter.resources" var="resources" />
	<display:column title="${resources}">
		${fn:length(row.resources)}
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.getMaxMinAvgReservationsForSocialCenter" />
</h2>

<jstl:out value="${minReservationsForSocialCenter}" />
<br>
<jstl:out value="${maxReservationsForSocialCenter}" />
<br>
<jstl:out value="${avgReservationsForSocialCenter}" />
<br>


<h2>
	<spring:message code="administrator.getMaxMinAvgPendingReservationsForSocialCenter" />
</h2>

<jstl:out value="${minPendingReservationsForSocialCenter}" />
<br>
<jstl:out value="${maxPendingReservationsForSocialCenter}" />
<br>
<jstl:out value="${avgPendingReservationsForSocialCenter}" />
<br>


<h2>
	<spring:message code="administrator.listClientsOrderByNumberCurrentReserves" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="listClientsOrderByNumberCurrentReserves" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="client.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="client.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true" />
	
	<spring:message code="client.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	
	<spring:message code="client.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true" />
	
	<spring:message code="client.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
	
	<spring:message code="client.picture" var="picture" />
	<display:column title="${picture}" sortable="true">
		<img src="${row.picture}" height="64" width="64"/>
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.getResourcesOrderByNumReservations" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="getResourcesOrderByNumReservations" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="resource.ticker" var="ticker" />
	<display:column property="ticker" title="${ticker}" sortable="true" />
	
	<spring:message code="resource.picture" var="picture" />
	<display:column title="${picture}" sortable="true">
		<img src="${row.picture}" height="64" width="64"/>
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.getBooksPublishedLastMonth" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="getBooksPublishedLastMonth" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="resource.ticker" var="ticker" />
	<display:column property="ticker" title="${ticker}" sortable="true" />
	
	<spring:message code="resource.picture" var="picture" />
	<display:column title="${picture}" sortable="true">
		<img src="${row.picture}" height="64" width="64"/>
	</display:column>
	
	<spring:message code="book.isbn" var="isbn" />
	<display:column property="isbn" title="${isbn}" sortable="true" />
	
	<spring:message code="book.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	
	<spring:message code="book.author" var="author" />
	<display:column property="author" title="${author}" sortable="true" />
	
	<spring:message code="book.editorial" var="editorial" />
	<display:column property="editorial" title="${editorial}" sortable="true" />
	
	<spring:message code="book.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<spring:message code="book.publicationDate" var="publicationDate" />
	<display:column property="publicationDate" title="${publicationDate}" format="{0,date,dd/MM/YYYY}" sortable="true" />
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.numClientsBanned" />
</h2>
<jstl:out value="${numClientsBanned}" />
<br>

<h2>
	<spring:message code="administrator.findBannedClients" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="findBannedClients" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="client.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="client.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true" />
	
	<spring:message code="client.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	
	<spring:message code="client.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true" />
	
	<spring:message code="client.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
	
	<spring:message code="client.picture" var="picture" />
	<display:column title="${picture}" sortable="true">
		<img src="${row.picture}" height="64" width="64"/>
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.getMaxMinAvgNumActivitiesLastSixMonthPerMonitor" />
</h2>
<jstl:out value="${avgNumActivitiesLastSixMonthPerMonitor}" />
<br>
<jstl:out value="${minNumActivitiesLastSixMonthPerMonitor}" />
<br>
<jstl:out value="${maxNumActivitiesLastSixMonthPerMonitor}" />
<br>

<h2>
	<spring:message code="administrator.getAvgAcceptedAndRejectedRequestForSocialCenter" />
</h2>

<spring:message code="avgAccepted"/>:
<jstl:out value="${avgAcceptedRequestForSocialCenter}" />
<br>
<spring:message code="avgDenied"/>:
<jstl:out value="${avgRejectedRequestForSocialCenter}" />
<br>

<h2>
	<spring:message code="administrator.activitiesCompletedByMonitor" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="activitiesCompletedByMonitor" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="activity.startDate" var="startDate" />
	<display:column property="startDate" title="${startDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.endDate" var="endDate" />
	<display:column property="endDate" title="${endDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	
	<spring:message code="activity.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<spring:message code="activity.seats" var="seats" />
	<display:column property="seats" title="${seats}" sortable="true" />
	
	<spring:message code="activity.viewMonitor" var="monitor" />
	<display:column title="${monitor}">
		${row.monitor.name}
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.activitiesWithoutBeginningByMonitor" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="activitiesWithoutBeginningByMonitor" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="activity.startDate" var="startDate" />
	<display:column property="startDate" title="${startDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.endDate" var="endDate" />
	<display:column property="endDate" title="${endDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	
	<spring:message code="activity.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<spring:message code="activity.seats" var="seats" />
	<display:column property="seats" title="${seats}" sortable="true" />
	
	<spring:message code="activity.viewMonitor" var="monitor" />
	<display:column title="${monitor}">
		${row.monitor.name}
	</display:column>
	
</display:table>
<br/>


<h2>
	<spring:message code="administrator.mostValueSocialCenter" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="getSocialCenterMoreValorate" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="socialCenter.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="socialCenter.city" var="city" />
	<display:column property="city" title="${city}" sortable="true" />
	
	<spring:message code="socialCenter.latitude" var="latitude" />
	<display:column property="latitude" title="${latitude}" sortable="true" />
	
	<spring:message code="socialCenter.longitude" var="longitude" />
	<display:column property="longitude" title="${longitude}" sortable="true" />
	
	<spring:message code="socialCenter.assessments" var="assessments" />
	<display:column title="${assessments}">
		${fn:length(row.assessments)}
	</display:column>
	
</display:table>
<br/>


<h2>
	<spring:message code="administrator.listActivitiesMoreFiftyPercentTheirCapacityOccupied" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="listActivitiesMoreFiftyPercentTheirCapacityOccupied" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="activity.startDate" var="startDate" />
	<display:column property="startDate" title="${startDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.endDate" var="endDate" />
	<display:column property="endDate" title="${endDate}" format="{0,date,dd/MM/YYYY HH:mm}" sortable="true" />
	
	<spring:message code="activity.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	
	<spring:message code="activity.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<spring:message code="activity.seats" var="seats" />
	<display:column property="seats" title="${seats}" sortable="true" />
	
	<spring:message code="activity.viewMonitor" var="monitor" />
	<display:column title="${monitor}">
		${row.monitor.name}
	</display:column>
	
</display:table>
<br/>

<h2>
	<spring:message code="administrator.clientMorePenalties" />
</h2>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="clientMorePenalties" requestURI="${requestURI}" id="row">

	<!-- Attributes -->
	
	<spring:message code="client.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="client.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true" />
	
	<spring:message code="client.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	
	<spring:message code="client.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true" />
	
	<spring:message code="client.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
	
	<spring:message code="client.picture" var="picture" />
	<display:column title="${picture}" sortable="true">
		<img src="${row.picture}" height="64" width="64"/>
	</display:column>
	
</display:table>
<br/>