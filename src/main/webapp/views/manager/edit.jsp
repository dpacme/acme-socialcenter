<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="managerUri/edit.do" modelAttribute="manager">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />
	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="manager.name" path="name" />

	<acme:textbox code="manager.surname" path="surname" />

	<acme:textbox code="manager.email" path="email" />
	
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="monitor.dni" path="identifier"/>
		</div>
		<jstl:if test="${duplicateIdentifier != null}">
			<div>
				<span class="message"><spring:message
						code="${duplicateIdentifier}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<!-- Acciones -->
	
	<acme:submit name="save" code="manager.save"/>
	
	<acme:cancel url="" code="manager.cancel"/>

</form:form>

<br>
