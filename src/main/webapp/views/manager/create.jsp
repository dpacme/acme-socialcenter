<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="managerUri/administrator/create.do" modelAttribute="managerForm">


	<b><spring:message code="manager.PersonalData" /></b>

	<br />

	<acme:textbox code="manager.name" path="name" />

	<acme:textbox code="manager.surname" path="surname" />

	<acme:textbox code="manager.email" path="email" />
	

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<br />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.dni" path="dni"/>
		</div>
		<jstl:if test="${duplicateIdentifier != null}">
			<div>
				<span class="message"><spring:message
						code="${duplicateIdentifier}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />

	<!-- Usuario y contraseņa -->

	<b><spring:message code="manager.LoginData" /></b>

	<br />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="manager.username" path="username" />
		</div>
		<jstl:if test="${duplicate != null}">
			<div>
				<span class="message"><spring:message code="${duplicate}" /></span>
			</div>
		</jstl:if>
	</div>

	<acme:password code="manager.password" path="password" />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:password code="manager.secondPassword" path="secondPassword" />
		</div>
		<jstl:if test="${pass != null}">
			<div>
				<span class="message"><spring:message code="${pass}" /></span>
			</div>
		</jstl:if>
	</div>

	<br />

	<!-- Aceptar para continuar -->
	
	<br />

	<!-- Acciones -->
	<acme:submit name="save" code="manager.signIn"/>
	
	<acme:cancel url="" code="manager.cancel"/>

</form:form>

<br>
