<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<jstl:if test="${noSocialCenter != null}">
	<span class="message"><spring:message code="${noSocialCenter}" /></span>
</jstl:if>

<jstl:if test="${noSocialCenter == null}">
	<div>
		<ul>
			<li><b><spring:message code="socialCenter.name" />:</b>
				${socialCenter.name}</li>
			<li><b><spring:message code="socialCenter.city" />:</b>
				${socialCenter.city}</li>
			<li><b><spring:message code="socialCenter.latitude" />:</b>
				${socialCenter.latitude}</li>
			<li><b><spring:message code="socialCenter.longitude" />:</b>
				${socialCenter.longitude}</li>
		</ul>
	</div>

	<security:authorize access="hasRole('CLIENT')">
		<h3 style="color: blue;">
			<spring:message code="resource.canReserve" />
		</h3>
	</security:authorize>
	<br>
	<h3>
		<spring:message code="socialCenter.books" />
	</h3>
	<br>
	<display:table name="books" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<jstl:set var="reserva" value="false" />
		<jstl:forEach var="reservation" items="${row.getReservations()}">
			<jstl:if test="${reservation.getReturnDate()==null}">
				<jstl:set var="reserva" value="true" />
			</jstl:if>
		</jstl:forEach>

		<jstl:set var="rowColor" value="none" />
		<jstl:if test="${reserva}">
			<jstl:set var="rowColor" value="background-color:red;" />
		</jstl:if>

		<spring:message code="resource.ticker" var="tickerHeader" />
		<display:column property="ticker" title="${tickerHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="resource.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}" style="${rowColor}">
			<a href="${row.picture}" target="_blank"><img
				src="${row.picture}" height="64" width="64"></a>
		</display:column>

		<spring:message code="book.isbn" var="isbnHeader" />
		<display:column property="isbn" title="${isbnHeader}" sortable="true"
			style="${rowColor}" />

		<spring:message code="book.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="book.author" var="authorHeader" />
		<display:column property="author" title="${authorHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="book.editorial" var="editorialHeader" />
		<display:column property="editorial" title="${editorialHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="book.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="book.publicationDate"
			var="publicationDateHeader" />
		<display:column property="publicationDate"
			title="${publicationDateHeader}" sortable="true" style="${rowColor}" />

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column style="${rowColor}">
					<a href="book/manager/edit.do?bookId=${row.id}"><spring:message
							code="socialCenter.resource.edit" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column style="${rowColor}">
					<a href="book/manager/delete.do?bookId=${row.id}"><spring:message
							code="socialCenter.resource.delete" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<spring:message code="resource.details" var="detailsHeader" />
		<display:column title="${detailsHeader}" sortable="false"
			style="${rowColor}">
			<acme:button
				href="resource/showDisplay.do?resourceId=${row.id}&used=${used}"
				name="see" code="resource.show" />
		</display:column>

	</display:table>

	<br>
	<br>

	<jstl:if test="${deleteBook != null}">
		<span class="message"><spring:message code="${deleteBook}" /></span>
	</jstl:if>

	<br>
	<br>

	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${owner!=null}">
			<a href="book/manager/create.do?socialCenterId=${socialCenter.id}"><spring:message
					code="socialCenter.book.create" /></a>
		</jstl:if>
	</security:authorize>

	<br>
	<h3>
		<spring:message code="socialCenter.electronicDevices" />
	</h3>
	<br>
	<display:table name="electronicDevices" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<jstl:set var="reserva" value="false" />
		<jstl:forEach var="reservation" items="${row.getReservations()}">
			<jstl:if test="${reservation.getReturnDate()==null}">
				<jstl:set var="reserva" value="true" />
			</jstl:if>
		</jstl:forEach>

		<jstl:set var="rowColor" value="none" />
		<jstl:if test="${reserva}">
			<jstl:set var="rowColor" value="background-color:red;" />
		</jstl:if>

		<spring:message code="resource.ticker" var="tickerHeader" />
		<display:column property="ticker" title="${tickerHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="resource.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}" style="${rowColor}">
			<a href="${row.picture}" target="_blank"><img
				src="${row.picture}" height="64" width="64"></a>
		</display:column>

		<spring:message code="electronicDevice.brand" var="brandHeader" />
		<display:column property="brand" title="${brandHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="electronicDevice.model" var="modelHeader" />
		<display:column property="model" title="${modelHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="electronicDevice.typeDevice"
			var="typeDeviceHeader" />
		<display:column property="typeDevice" title="${typeDeviceHeader}"
			sortable="true" style="${rowColor}" />

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column style="${rowColor}">
					<a
						href="electronicDevice/manager/edit.do?electronicDeviceId=${row.id}"><spring:message
							code="socialCenter.resource.edit" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column style="${rowColor}">
					<a
						href="electronicDevice/manager/delete.do?electronicDeviceId=${row.id}"><spring:message
							code="socialCenter.resource.delete" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<spring:message code="resource.details" var="detailsHeader" />
		<display:column title="${detailsHeader}" sortable="false"
			style="${rowColor}">
			<acme:button
				href="resource/showDisplay.do?resourceId=${row.id}&used=${used}"
				name="see" code="resource.show" />
		</display:column>

	</display:table>

	<br>
	<br>

	<jstl:if test="${deleteElectronicDevice != null}">
		<span class="message"><spring:message
				code="${deleteElectronicDevice}" /></span>
	</jstl:if>

	<br>
	<br>

	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${owner!=null}">
			<a
				href="electronicDevice/manager/create.do?socialCenterId=${socialCenter.id}"><spring:message
					code="socialCenter.electronicDevice.create" /></a>
		</jstl:if>
	</security:authorize>

	<br>
	<h3>
		<spring:message code="socialCenter.rooms" />
	</h3>
	<br>
	<display:table name="rooms" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="resource.ticker" var="tickerHeader" />
		<display:column property="ticker" title="${tickerHeader}"
			sortable="true" />

		<spring:message code="resource.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}">
			<a href="${row.picture}" target="_blank"><img
				src="${row.picture}" height="64" width="64"></a>
		</display:column>

		<spring:message code="room.capacity" var="capacityHeader" />
		<display:column property="capacity" title="${capacityHeader}"
			sortable="true" />

		<spring:message code="room.board" var="boardHeader" />
		<display:column property="board" title="${boardHeader}"
			sortable="true" />

		<spring:message code="room.tv" var="tvHeader" />
		<display:column property="tv" title="${tvHeader}" sortable="true" />

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column>
					<a href="room/manager/edit.do?roomId=${row.id}"><spring:message
							code="socialCenter.resource.edit" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<security:authorize access="hasRole('MANAGER')">
			<jstl:if test="${owner!=null}">
				<display:column>
					<a href="room/manager/delete.do?roomId=${row.id}"><spring:message
							code="socialCenter.resource.delete" /></a>
				</display:column>
			</jstl:if>
		</security:authorize>

		<spring:message code="resource.details" var="detailsHeader" />
		<display:column title="${detailsHeader}" sortable="false">
			<acme:button
				href="resource/showDisplay.do?resourceId=${row.id}&used=${used}"
				name="see" code="resource.show" />
		</display:column>

	</display:table>

	<br>
	<br>

	<jstl:if test="${deleteRoom != null}">
		<span class="message"><spring:message code="${deleteRoom}" /></span>
	</jstl:if>

	<br>
	<br>

	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${owner!=null}">
			<a href="room/manager/create.do?socialCenterId=${socialCenter.id}"><spring:message
					code="socialCenter.room.create" /></a>
		</jstl:if>
	</security:authorize>

	<br>
	<h3>
		<spring:message code="socialCenter.assessments" />
	</h3>
	<br>
	<display:table name="assessments" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="assessment.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" />

		<spring:message code="assessment.comment" var="commentHeader" />
		<display:column property="comment" title="${commentHeader}"
			sortable="true" />

		<security:authorize access="hasRole('CLIENT')">
			<spring:message code="assessment.actions" var="actionsHeader" />
			<display:column title="${actionsHeader}">
				<jstl:choose>
					<jstl:when
						test="${utilidades:listLikesUserAccount(row.likes, loginService.getPrincipal()) == null || utilidades:listLikesUserAccount(row.likes, loginService.getPrincipal()) == false}">
						<div class="inline">
							<acme:button
								href="assessment/client/likeAssessment.do?assessmentId=${row.id}"
								name="like" code="assessment.actions.like" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" disabled="disabled" name="like"
								value='<spring:message code="assessment.actions.like" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
				<jstl:choose>
					<jstl:when
						test="${utilidades:listLikesUserAccount(row.likes, loginService.getPrincipal()) == null || utilidades:listLikesUserAccount(row.likes, loginService.getPrincipal()) == true}">
						<div class="inline">
							<acme:button
								href="assessment/client/dislikeAssessment.do?assessmentId=${row.id}"
								name="dislike" code="assessment.actions.dislike" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" disabled="disabled" name="dislike"
								value='<spring:message code="assessment.actions.dislike" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
			</display:column>
		</security:authorize>

	</display:table>
	<br />

	<security:authorize access="hasRole('CLIENT')">
		<a
			href="assessment/client/create.do?socialCenterId=${socialCenter.id}"><spring:message
				code="assessment.create" /></a>
	</security:authorize>

	<span class="message"><spring:message code="resource.reserved" /></span>

</jstl:if>
