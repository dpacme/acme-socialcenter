<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="socialCenter">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="assessments" />
	<form:hidden path="requests" />
	<form:hidden path="resources" />

	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="socialCenter.name" path="name" />

	<acme:textbox code="socialCenter.city" path="city" />

	<fieldset style="width: 20%">
		<legend style="font-size: x-large" >
			<b><spring:message code="socialCenter.coordinates" /></b>
		</legend>
		<spring:message code="socialCenter.infoCoordinates" />
		<acme:textbox2 code="socialCenter.latitude" path="latitude" />

		<acme:textbox2 code="socialCenter.longitude" path="longitude" />
	</fieldset>


	<acme:select2 items="${managers}" itemLabel="name"
		code="socialCenter.managers" path="managers" />
	<spring:message code="socialCenter.helpAddManager" />

	<jstl:if test="${managerNotNull != null}">
		<div>
			<span class="message"><spring:message code="${managerNotNull}" /></span>
		</div>
	</jstl:if>


	<br />
	<br />


	<!-- Acciones -->

	<jstl:choose>
		<jstl:when test="${needManagers}">
			<h2>
				<spring:message code="socialCenter.needManagers" />
			</h2>
		</jstl:when>
		<jstl:otherwise>
			<acme:submit name="save" code="socialCenter.save" />

			<acme:cancel url="socialCenter/all.do" code="socialCenter.cancel" />

			<jstl:choose>
				<jstl:when test="${canDelete}">
					<acme:button
						href="socialCenter/administrator/delete.do?socialCenterId=${socialCenter.id}"
						name="delete" code="socialCenter.resource.delete" />
				</jstl:when>
				<jstl:otherwise>
					<br>
					<spring:message code="socialCenter.cantDelete" />
				</jstl:otherwise>
			</jstl:choose>
		</jstl:otherwise>

	</jstl:choose>


</form:form>

<br>
