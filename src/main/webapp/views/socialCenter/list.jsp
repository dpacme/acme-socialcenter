<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<fieldset style="width: 20%">
	<legend>
		<spring:message code="socialCenter.search" />
	</legend>
	<form method="POST" action="socialCenter/search.do">

		<spring:message code="socialCenter.keyword" />
		<spring:message code="socialCenter.keyword" var="keywordPlaceholder" />
		<input type="text" id="keyword" name="keyword"
			placeholder="${keywordPlaceholder}" /> <br /> <input type="submit"
			name="search" value="<spring:message code="socialCenter.search"/>" />
	</form>
</fieldset>

<display:table name="socialCenters" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">
<!-- BUG MBC QUITADO ORDENACION EN NAME, CITY, LATITUDE, LONG -->
	<spring:message code="socialCenter.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />

	<spring:message code="socialCenter.city" var="cityHeader" />
	<display:column property="city" title="${cityHeader}" sortable="true" />

	<spring:message code="socialCenter.latitude" var="latitudeHeader" />
	<display:column property="latitude" title="${latitudeHeader}" sortable="true"
		 />

	<spring:message code="socialCenter.longitude" var="longitudeHeader" />
	<display:column property="longitude" title="${longitudeHeader}" sortable="true"
		 />

	<spring:message code="socialCenter.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button
			href="socialCenter/showDisplay.do?socialCenterId=${row.id}"
			name="see" code="socialCenter.see" />
	</display:column>

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<spring:message code="socialCenter.resource.edit" var="editHeader" />
		<display:column title="${editHeader}">
			<acme:button
				href="socialCenter/administrator/edit.do?socialCenterId=${row.id}"
				name="edit" code="socialCenter.resource.edit" />
		</display:column>
	</security:authorize>

</display:table>

<security:authorize access="hasRole('ADMINISTRATOR')">
	<div>
		<acme:button
			href="socialCenter/administrator/create.do"
			name="edit" code="socialCenter.create" />
	</div>
</security:authorize>

<div id="map_container"></div>

<jstl:if test="${isMap}">
	<script>
		$(document).ready(function() {
			loadMap();
		});
	</script>
</jstl:if>
<style type="text/css">
div#map_container {
	width: 50%;
	height: 350px;
}
</style>

<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCpEullGVfrlP6rB9qQ1fNvn-aiww2NobU"></script>


<script type="text/javascript">
	function loadMap() {
		var table = document.getElementById("row");
		var tbody = table.getElementsByTagName("tbody")[0];
		var rows = tbody.getElementsByTagName("tr");

		var latitud = rows[0].getElementsByTagName("td")[2].firstChild.nodeValue;
		var longitud = rows[0].getElementsByTagName("td")[3].firstChild.nodeValue;

		var latlng = new google.maps.LatLng(latitud, longitud);
		var myOptions = {
			zoom : 4,
			center : latlng,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_container"), myOptions);

		for (i = 0; i < rows.length; i++) {
			var latitud = rows[i].getElementsByTagName("td")[2].firstChild.nodeValue;
			var longitud = rows[i].getElementsByTagName("td")[3].firstChild.nodeValue;

			var nombre = rows[i].getElementsByTagName("td")[0].firstChild.nodeValue;
			var descripcion = rows[i].getElementsByTagName("td")[1].firstChild.nodeValue;

			var latlng = new google.maps.LatLng(latitud, longitud);

			var marker = new google.maps.Marker({
				position : latlng,
				map : map,
				title : nombre + " - " + descripcion
			});

		}

	}
</script>