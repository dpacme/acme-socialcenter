<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>


<div>
	<ul>
		<li><b><spring:message code="activityReservation.startDate" />:</b>
			${activityReservation.startDate}</li>
		<li><b><spring:message code="activityReservation.endDate" />:</b>
			${activityReservation.endDate}</li>
	</ul>
</div>

<br/>
<h2><spring:message code="activityReservation.room" /></h2>
<br/>

<a href="${activityReservation.room.picture}" target="_blank"><img
		src="${activityReservation.room.picture}" width="256"></a>
<ul>
<li><b><spring:message code="resource.ticker" />:</b>
			${activityReservation.room.ticker}</li>
<li><b><spring:message code="room.capacity" />:</b>
	${activityReservation.room.capacity}</li>
<li><b><spring:message code="room.board" />:</b>
	${activityReservation.room.board}</li>
<li><b><spring:message code="room.tv" />:</b> ${activityReservation.room.tv}</li>
</ul>