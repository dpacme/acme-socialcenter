<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li>
			<b><spring:message code="activityReservation.startDate"/>:</b>
			${activityReservation.startDate}
		</li>
		<li>
			<b><spring:message code="activityReservation.endDate"/>:</b>
			${activityReservation.endDate}
		</li>
	</ul>
</div>

<br/>

<b><spring:message code="activityReservation.activity"/></b>
<acme:button href="activity/view.do?activityId=${activityReservation.activity.id}" name="viewActivity"
			code="activityReservation.see" />
			
<br/>

<b><spring:message code="activityReservation.room"/></b>
<acme:button href="room/view.do?roomId=${activityReservation.room.id}" name="viewRoom"
			code="activityReservation.see" />
