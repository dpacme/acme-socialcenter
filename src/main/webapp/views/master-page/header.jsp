<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-SocialCenter Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv" href="administrator/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li><a class="fNiv" href="managerUri/administrator/create.do"><spring:message code="master.page.manager.create"/></a></li>
			<li><a class="fNiv" href="penaltyParameter/administrator/showDisplay.do"><spring:message code="master.page.penaltyParameter"/></a></li>
			<li><a class="fNiv" href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
		</security:authorize>
		
		<security:authorize access="hasRole('CLIENT')">
			<li><a class="fNiv" href="client/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li><a class="fNiv" href="penalty/list.do"><spring:message code="master.page.penalty"/></a></li>
			<li><a class="fNiv" href="incidence/client/myIncidences.do"><spring:message code="master.page.myIncidences"/></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.activities" />
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="activity/list.do"><spring:message code="master.page.listActivities"/></a></li>
					<li><a href="activity/client/list.do"><spring:message code="master.page.myActivities" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv" href="resource/client/myResources.do"><spring:message code="master.page.myResources"/></a></li>
			<li><a class="fNiv" href="request/client/list.do"><spring:message code="master.page.requestResource"/></a></li>
			<li>
			<a class="fNiv"> 
				<spring:message code="master.page.reservations" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="reservation/client/activeReservations.do"><spring:message code="master.page.activeReservations"/></a></li>
				<li><a href="reservation/client/inactiveReservations.do"><spring:message code="master.page.inactiveReservations"/></a></li>
			</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv" href="managerUri/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li><a class="fNiv" href="client/list.do"><spring:message code="master.page.clients"/></a></li>
			<li><a class="fNiv" href="incidence/manager/all.do"><spring:message code="master.page.incidences"/></a></li>
			<li><a class="fNiv" href="category/manager/all.do"><spring:message code="master.page.listCategories"/></a></li>
			<li><a class="fNiv" href="request/manager/list.do"><spring:message code="master.page.requestResource"/></a></li>
			<li>
			<a class="fNiv"> 
				<spring:message code="master.page.reservations" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="reservation/manager/all.do"><spring:message code="master.page.allReservations"/></a></li>
				<li><a href="reservation/manager/activeReservations.do"><spring:message code="master.page.activeReservations"/></a></li>
			</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('MONITOR')">
			<li><a class="fNiv" href="monitor/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.activities" />
				</a>
				<ul>
					<li class="arrow"></li>		
					<li><a href="activity/list.do"><spring:message code="master.page.listActivities"/></a></li>
					<li><a href="activity/monitor/create.do"><spring:message code="master.page.createActivities"/></a></li>		
					<li><a href="activity/monitor/myActivities.do"><spring:message code="master.page.myActivities" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a class="fNiv" href="client/create.do"><spring:message code="master.page.client.registerAsClient" /></a></li>
			<li><a class="fNiv" href="monitor/create.do"><spring:message code="master.page.client.registerAsMonitor" /></a></li>
		</security:authorize>
		
		<li>
			<a class="fNiv"> 
				<spring:message code="master.page.socialCenter" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="socialCenter/all.do"><spring:message code="master.page.listSocialCenter" /></a></li>
				<security:authorize access="hasRole('MANAGER')">
					<li><a href="socialCenter/manager/mySocialCenter.do"><spring:message code="master.page.mySocialCenter"/></a></li>
				</security:authorize>
			</ul>
		</li>
		
		<li>
			<a class="fNiv"> 
				<spring:message code="master.page.resources" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="resource/all.do"><spring:message code="master.page.listResources" /></a></li>
				<security:authorize access="hasRole('MANAGER')">
					<li><a href="resource/manager/loanResources.do"><spring:message code="master.page.loanResources" /></a></li>
				</security:authorize>
			</ul>
		</li>
		
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

