<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />

<security:authorize access="hasRole('MANAGER')">

<display:table name="categories" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="category.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<display:column>
		<a href="category/manager/edit.do?categoryId=${row.id}"><spring:message code="category.edit" /></a>
	</display:column>
		
</display:table>

<br/>


<a href="category/manager/create.do"><spring:message code="category.create" /></a>

</security:authorize>