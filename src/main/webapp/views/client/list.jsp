<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<display:table pagesize="5" class="displaytag" keepStatus="false"
	name="clients" requestURI="${requestURI}" id="row">

	<!-- Attributes -->

	<spring:message code="client.name" var="name" />
	<display:column property="name" title="${namev}" sortable="true" />
	
	<spring:message code="client.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true" />
	
	<spring:message code="client.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	
	<spring:message code="client.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true" />
	
	<spring:message code="client.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
	
	<spring:message code="client.picture" var="pictureHeader" />
	<display:column title="${pictureHeader}">
		<a href="${picture}" target="_blank"><img src="${picture}"
				height="64" width="64"></a>
	</display:column>
	
	<security:authorize access="hasRole('MANAGER')">
		<spring:message code="client.penalties" var="penaltiesHeader" />
		<display:column title="${penaltiesHeader}">
			<acme:button href="penalty/listByClient.do?clientId=${row.getId()}" name="listPenalties"
				code="client.see" />
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('MANAGER')">
		<spring:message code="client.incidences" var="incidencesHeader" />
		<display:column title="${incidencesHeader}">
			<acme:button href="incidence/client/listByClient.do?clientId=${row.getId()}" name="listIncidences"
				code="client.see" />
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('MANAGER')">
		<spring:message code="client.requests" var="requestsHeader" />
		<display:column title="${requestsHeader}">
			<acme:button href="request/client/listByClient.do?clientId=${row.getId()}" name="listRequest"
				code="client.see" />
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('MANAGER')">
		<spring:message code="client.reservations" var="reservationsHeader" />
		<display:column title="${reservationsHeader}">
			<acme:button href="reservation/client/listByClient.do?clientId=${row.getId()}" name="listReservations"
				code="client.see" />
		</display:column>
	</security:authorize>

</display:table>
