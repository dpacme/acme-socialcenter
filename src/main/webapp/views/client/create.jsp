<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="client/create.do" modelAttribute="clientForm">


	<b><spring:message code="client.PersonalData" /></b>

	<br />

	<acme:textbox code="client.name" path="name" />

	<acme:textbox code="client.surname" path="surname" />

	<acme:textbox code="client.email" path="email" />
	

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<br />
	
	<acme:textbox code="client.picture" path="picture" />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.dni" path="dni"/>
		</div>
		<jstl:if test="${duplicateIdentifier != null}">
			<div>
				<span class="message"><spring:message
						code="${duplicateIdentifier}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />

	<!-- Usuario y contraseņa -->

	<b><spring:message code="client.LoginData" /></b>

	<br />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.username" path="username" />
		</div>
		<jstl:if test="${duplicate != null}">
			<div>
				<span class="message"><spring:message code="${duplicate}" /></span>
			</div>
		</jstl:if>
	</div>

	<acme:password code="client.password" path="password" />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:password code="client.secondPassword" path="secondPassword" />
		</div>
		<jstl:if test="${pass != null}">
			<div>
				<span class="message"><spring:message code="${pass}" /></span>
			</div>
		</jstl:if>
	</div>

	<br />

	<!-- Aceptar para continuar -->

	<form:label path="checkBox">
		<spring:message code="client.checkBox" />
	</form:label>
	<form:checkbox path="checkBox" />
	<a href="misc/terms.do"> <spring:message
			code="client.moreInfo" />
	</a>
	<form:errors class="error" path="checkBox" />

	<br />
	<br />

	<!-- Acciones -->
	<acme:submit name="save" code="client.signIn"/>
	
	<acme:cancel url="" code="client.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>
