<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="client/edit.do" modelAttribute="client">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />
	<form:hidden path="banned" />

	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="client.name" path="name" />

	<acme:textbox code="client.surname" path="surname" />

	<acme:textbox code="client.email" path="email" />
	
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />	
	
	<acme:textbox code="client.picture" path="picture" />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="client.dni" path="identifier"/>
		</div>
		<jstl:if test="${duplicateIdentifier != null}">
			<div>
				<span class="message"><spring:message
						code="${duplicateIdentifier}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<!-- Acciones -->
	
	<acme:submit name="save" code="client.save"/>
	
	<acme:cancel url="" code="client.cancel"/>

</form:form>

<br>
