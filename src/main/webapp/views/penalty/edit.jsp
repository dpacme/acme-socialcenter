<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<script>
	$(function() {


		$('.datetimepicker').datetimepicker({
			dateFormat : 'dd/mm/yy',
		});

	});
</script>

<form:form action="penalty/manager/edit.do" modelAttribute="penalty">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="client" />
	<form:hidden path="startDate" />
	
	<spring:message code="penalty.date.click" var="datePlaceholder" />
	<spring:message code="penalty.endDate" />:
	<form:input type="text" id="endDate" path="endDate"
		readonly="readonly" placeholder="${datePlaceholder}"
		class="datetimepicker" />
	<form:errors class="error" path="endDate" />
	<br />
	
	
	<input type="submit" name="save" value="<spring:message code="penalty.save" />" />&nbsp; 

	<input type="button" name="cancel"
		value="<spring:message code="penalty.cancel" />"
		onclick="javascript: window.location.replace('penalty/listByClient.do?clientId=${penalty.getClient().getId()}');" />
	<br />

</form:form>

