<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />


<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="penalties" requestURI="${requestURI}" id="row">

	<!-- Attributes -->

	<spring:message code="penalty.startDate" var="startDate" />
	<display:column property="startDate" title="${startDate}" sortable="true" format="{0,date,dd/MM/YYYY}"/>
	
	<spring:message code="penalty.endDate" var="endDate" />
	<display:column property="endDate" title="${endDate}" sortable="true" format="{0,date,dd/MM/YYYY}"/>
	
	<jsp:useBean id="now" class="java.util.Date"/>
	
	<security:authorize access="hasRole('MANAGER')">
			<display:column>
		<jstl:if test="${row.endDate >= now}">
				<acme:button href="penalty/manager/edit.do?penaltyId=${row.id}"
					name="edit" code="penalty.edit" />
		</jstl:if>
		
			</display:column>
	</security:authorize>
	

</display:table>
