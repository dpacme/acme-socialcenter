<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<fieldset style="width: 20%">
	<legend>
		<spring:message code="resource.search" />
	</legend>
	<form method="POST" action="resource/search.do">

		<spring:message code="resource.keyword" />
		<spring:message code="resource.keyword" var="keywordPlaceholder" />
		<input type="text" id="keyword" name="keyword"
			placeholder="${keywordPlaceholder}" /> <br /> <input type="hidden"
			id="socialCenterId" name="socialCenterId" value="${socialCenter.id}"
			readonly="readonly" /> <input type="hidden" id="loanResources"
			name="loanResources" value="${loanResources}" readonly="readonly" />
		<input type="submit" name="search"
			value="<spring:message code="resource.search"/>" />
	</form>
</fieldset>

<jstl:if test="${noSocialCenter != null}">
	<span class="message"><spring:message code="${noSocialCenter}" /></span>
</jstl:if>


<jstl:if test="${noSocialCenter == null}">

	<security:authorize access="hasRole('CLIENT')">
		<h2 style="color: blue;">
			<spring:message code="resource.canReserve" />
		</h2>
	</security:authorize>
	<display:table name="resources" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<jstl:set var="reserva" value="false" />
		<jstl:forEach var="reservation" items="${row.getReservations()}">
			<jstl:if test="${reservation.getReturnDate()==null}">
				<jstl:if test="${row.getClass().getName()!= 'domain.Room'}">
					<jstl:set var="reserva" value="true" />
				</jstl:if>
			</jstl:if>
		</jstl:forEach>

		<jstl:set var="rowColor" value="none" />
		<jstl:if test="${reserva}">
			<jstl:set var="rowColor" value="background-color:#bb1932;" />
		</jstl:if>

		<display:column style="${rowColor}">
			<jstl:if test="${orderedResources.contains(row)}">
				*
			</jstl:if>
		</display:column>

		<spring:message code="resource.ticker" var="tickerHeader" />
		<display:column property="ticker" title="${tickerHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="resource.picture" var="cityHeader" />
		<display:column title="${cityHeader}" sortable="true"
			style="${rowColor}">
			<a href="${row.picture}" target="_blank"><img
				src="${row.picture}" height="64" width="64"></a>
		</display:column>

		<spring:message code="resource.typeResource" var="typeResourceHeader" />
		<display:column title="${typeResourceHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if test="${row.getClass().getName()== 'domain.Book'}">
				<spring:message code="resource.book" />
			</jstl:if>
			<jstl:if
				test="${row.getClass().getName()== 'domain.ElectronicDevice'}">
				<spring:message code="resource.electronicDevice" />
			</jstl:if>
			<jstl:if test="${row.getClass().getName()== 'domain.Room'}">
				<spring:message code="resource.room" />
			</jstl:if>
		</display:column>

		<spring:message code="room.capacity" var="capacityHeader" />
		<display:column title="${capacityHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if test="${row.getClass().getName()== 'domain.Room'}">
				${row.capacity}
			</jstl:if>
			<jstl:if test="${row.getClass().getName()!= 'domain.Room'}">
				N/A
			</jstl:if>
		</display:column>

		<spring:message code="electronicDevice.brand" var="brandHeader" />
		<display:column title="${brandHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if
				test="${row.getClass().getName()== 'domain.ElectronicDevice'}">
				${row.brand}
			</jstl:if>
			<jstl:if
				test="${row.getClass().getName()!= 'domain.ElectronicDevice'}">
				N/A
			</jstl:if>
		</display:column>

		<spring:message code="electronicDevice.model" var="modelHeader" />
		<display:column title="${modelHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if
				test="${row.getClass().getName()== 'domain.ElectronicDevice'}">
				${row.model}
			</jstl:if>
			<jstl:if
				test="${row.getClass().getName()!= 'domain.ElectronicDevice'}">
				N/A
			</jstl:if>
		</display:column>

		<spring:message code="book.title" var="titleHeader" />
		<display:column title="${titleHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if test="${row.getClass().getName()== 'domain.Book'}">
			${row.title}
		</jstl:if>
			<jstl:if test="${row.getClass().getName()!= 'domain.Book'}">
				N/A
		</jstl:if>
		</display:column>

		<spring:message code="book.author" var="authorHeader" />
		<display:column title="${authorHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if test="${row.getClass().getName()== 'domain.Book'}">
			${row.author}
		</jstl:if>
			<jstl:if test="${row.getClass().getName()!= 'domain.Book'}">
				N/A
		</jstl:if>
		</display:column>

		<spring:message code="book.description" var="descriptionHeader" />
		<display:column title="${descriptionHeader}" sortable="true"
			style="${rowColor}">
			<jstl:if test="${row.getClass().getName()== 'domain.Book'}">
			${row.description}
		</jstl:if>
			<jstl:if test="${row.getClass().getName()!= 'domain.Book'}">
				N/A
		</jstl:if>
		</display:column>

		<spring:message code="resource.socialCenter" var="socialCenterHeader" />
		<display:column title="${socialCenterHeader}" sortable="true"
			style="${rowColor}">
			${row.socialCenter.name} 
			<acme:button
				href="socialCenter/showDisplay.do?socialCenterId=${row.socialCenter.id}"
				name="see" code="socialCenter.see" />
		</display:column>

		<spring:message code="resource.details" var="detailsHeader" />
		<display:column title="${detailsHeader}" sortable="false"
			style="${rowColor}">
			<acme:button
				href="resource/showDisplay.do?resourceId=${row.id}&used=${used}"
				name="see" code="resource.show" />
		</display:column>


	</display:table>

	<br />
	<br />
	<span class="message"><spring:message code="resource.reserved" /></span>
	<br />
	<spring:message code="resource.moreRating" />

</jstl:if>

