<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>


<div>
	<a href="${resource.picture}" target="_blank"><img
		src="${resource.picture}" width="256"></a>
	<ul>
		<li><b><spring:message code="resource.ticker" />:</b>
			${resource.ticker}</li>
		<jstl:if test="${isBook}">
			<li><b><spring:message code="book.isbn" />:</b>
				${resource.isbn}</li>
			<li><b><spring:message code="book.title" />:</b>
				${resource.title}</li>
			<li><b><spring:message code="book.author" />:</b>
				${resource.author}</li>
			<li><b><spring:message code="book.editorial" />:</b>
				${resource.isbn}</li>
			<li><b><spring:message code="book.description" />:</b>
				${resource.description}</li>
			<li><b><spring:message code="book.publicationDate" />:</b>
				${resource.publicationDate}</li>
		</jstl:if>
		<jstl:if test="${isElectronicDevice}">
			<li><b><spring:message code="electronicDevice.brand" />:</b>
				${resource.brand}</li>
			<li><b><spring:message code="electronicDevice.model" />:</b>
				${resource.model}</li>
			<li><b><spring:message code="electronicDevice.typeDevice" />:</b>
				${resource.typeDevice}</li>
		</jstl:if>
		<jstl:if test="${isRoom}">
			<li><b><spring:message code="room.capacity" />:</b>
				${resource.capacity}</li>
			<li><b><spring:message code="room.board" />:</b>
				${resource.board}</li>
			<li><b><spring:message code="room.tv" />:</b> ${resource.tv}</li>
		</jstl:if>
	</ul>
</div>
<security:authorize access="hasRole('CLIENT')">

	<fieldset style="width: 20%">
		<legend>
			<b><spring:message code="resource.reserve" /></b>
		</legend>

		<jstl:choose>
			<jstl:when test="${param.correctReservation}">
				<h2>
					<spring:message code="resource.correctReservation" />
				</h2>
			</jstl:when>
			<jstl:otherwise>
				<jstl:choose>
					<jstl:when test="${isAvailable}">
						<jstl:if test="${isBook}">
							<spring:message code="resource.timeBook" />
						</jstl:if>
						<jstl:if test="${isElectronicDevice}">
							<spring:message code="resource.timeDevice" />
						</jstl:if>
						<acme:button
							href="reservation/client/create.do?resourceId=${resource.id}"
							name="see" code="resource.reserve" />
					</jstl:when>
					<jstl:when test="${timeIsCorrect == false}">
						<h3>
							<spring:message code="resource.limitTime" />
						</h3>
					</jstl:when>
					<jstl:when test="${isAvailable == false}">
						<h3>
							<spring:message code="resource.notAvailable" />
						</h3>
					</jstl:when>
					<jstl:when test="${penalty}">
						<div class="error">
							<spring:message code="resource.penalty" />
						</div>
					</jstl:when>
					<jstl:when test="${limited}">
						<div class="error">
							<spring:message code="resource.limited" />
						</div>
					</jstl:when>

				</jstl:choose>
			</jstl:otherwise>
		</jstl:choose>
	</fieldset>

</security:authorize>
<br>
<h2>
	<spring:message code="resource.comments" />
</h2>

<display:table name="comments" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="comment.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}"
		sortable="false" />


	<spring:message code="comment.score" var="scoreHeader" />
	<display:column property="score" title="${scoreHeader}" sortable="true" />

	<spring:message code="comment.client" var="clientHeader" />
	<display:column property="client.name" title="${clientHeader}"
		sortable="true" />


</display:table>

<security:authorize access="hasRole('CLIENT')">
	<jstl:if test="${used}">
		<acme:button href="comment/client/create.do?resourceId=${resource.id}"
			name="create" code="resource.comment" />
	</jstl:if>
</security:authorize>