<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Mensajes de t�rminos -->

<p style="font-size: 18px">
	<spring:message code="misc.lodp" />
</p>

<spring:message code="misc.terms" />

<p style="font-size: 18px">
	<spring:message code="misc.cookies" />
</p>

<spring:message code="misc.cookiesText" />

<p style="font-size: 18px">
	<spring:message code="misc.baja" />
</p>

<spring:message code="misc.bajaText" />
