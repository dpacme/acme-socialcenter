<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

	<fieldset style="width: 20%">
		<legend style="font-size: x-large" align="center"><b><spring:message code="incidence.reservation.data" /></b></legend>
		
		<ul>
			<li><b><spring:message code="reservation.startDate" />:</b>
				<fmt:formatDate value="${reservation.startDate}" pattern="dd/MM/YYYY" /></li>
			<li><b><spring:message code="reservation.endDate" />:</b>
				<fmt:formatDate value="${reservation.endDate}" pattern="dd/MM/YYYY" /></li>
			<li><b><spring:message code="reservation.returnDate" />:</b>
				<fmt:formatDate value="${reservation.returnDate}" pattern="dd/MM/YYYY" /></li>
		</ul>
	</fieldset>
	
<br>


<h2>
	<spring:message code="reservation.incidences" />
</h2>
<display:table name="incidences" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="incidence.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="incidence.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="true" />

	<spring:message code="incidence.status" var="statusHeader" />
	<display:column title="${statusHeader}" sortable="true">
		<jstl:if test="${row.status == 'open'}">
			<spring:message code="incidence.status.open"/>
		</jstl:if>
		<jstl:if test="${row.status == 'closed'}">
			<spring:message code="incidence.status.closed"/>
		</jstl:if>
		<jstl:if test="${row.status == 'solved'}">
			<spring:message code="incidence.status.solved"/>
		</jstl:if>
	</display:column>
	
	<spring:message code="incidence.manager" var="managerHeader" />
	<display:column title="${managerHeader}" sortable="true" >
		${row.manager.name} ${row.manager.surname}
	</display:column>
	
	<spring:message code="incidence.client" var="clientHeader" />
	<display:column title="${clientHeader}" sortable="true" >
		${row.reservation.client.name} ${row.reservation.client.surname}
	</display:column>
	
	<spring:message code="incidence.details" var="detailsHeader" />
	<display:column title="${detailsHeader}" sortable="true" >
		<acme:button href="incidence/showDisplay.do?incidenceId=${row.id}"
			name="see" code="incidence.details.see" />
	</display:column>

</display:table>

<security:authorize access="hasRole('MANAGER')">
   	<acme:button href="incidence/manager/create.do?reservationId=${reservation.id}" name="createIncidence" code="incidence.create" />
</security:authorize>	