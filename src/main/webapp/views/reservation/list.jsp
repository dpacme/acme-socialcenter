<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>



<display:table name="reservations" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="reservation.startDate" var="startDateHeader" />
	<display:column property="startDate" title="${startDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<spring:message code="reservation.endDate" var="endDateHeader" />
	<display:column property="endDate" title="${endDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<%-- <jstl:if test="${row.returnDate != null}"> --%>
	<jstl:if test="${!requestURI.contains('/activeReservations.do')}">
		<spring:message code="reservation.returnDate" var="returnDateHeader" />
		<display:column property="returnDate" title="${returnDateHeader}"
			sortable="true" />
	</jstl:if>


	<spring:message code="reservation.resource" var="resourceHeader" />
	<display:column title="${resourceHeader}">
		<a href="${row.resource.picture}" target="_blank"><img
			src="${row.resource.picture}" height="64" width="64"></a>

		<a href="resource/showDisplay.do?resourceId=${row.resource.id}"><spring:message
				code="reservation.showResource" /></a>


	</display:column>

	<spring:message code="reservation.resource.ticker" var="tickerHeader" />
	<display:column property="resource.ticker" title="${tickerHeader}"
		sortable="true" />

	<spring:message code="reservation.client" var="clientHeader" />
	<display:column property="client.userAccount.username"
		title="${clientHeader}" sortable="true" />

	<spring:message code="incidence.reservation"
		var="reservationDetailsHeader" />
	<display:column title="${reservationDetailsHeader}" sortable="true">
		<acme:button href="reservation/showDisplay.do?reservationId=${row.id}"
			name="see" code="incidence.reservation.see" />
	</display:column>

	<security:authorize access="hasRole('CLIENT')">
		<jstl:if test="${actives}">
			<display:column sortable="false">
				<jstl:if
					test="${row.client.id == idLogged && row.resource.getClass().getName()== 'domain.Room'}">
					<acme:button
						href="reservation/client/cancelReservation.do?reservationId=${row.id}"
						name="see" code="reservation.cancelReservation" />
				</jstl:if>
			</display:column>
		</jstl:if>
	</security:authorize>

	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${actives}">
			<display:column sortable="false">
				<jstl:if test="${row.resource.socialCenter.id == socialCenterId}">
					<acme:button
						href="reservation/manager/finalizeReservation.do?reservationId=${row.id}"
						name="see" code="reservation.finalizeReservation" />
				</jstl:if>
			</display:column>
		</jstl:if>
	</security:authorize>

</display:table>



<jstl:if test="${param.notCancel == true}">
	<div id="cancelInfo" class="error">
		<spring:message code="reservation.notCancel" />
	</div>
</jstl:if>

<br />


