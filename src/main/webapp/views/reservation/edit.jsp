<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
	function seleccionarCodigo(dia, hora) {
		var form = document.getElementById("formCodigo");
		document.getElementById("codigo").value = dia + "-" + hora;

		form.submit();
	}
</script>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />


<br>
<fieldset>
	<legend style="font-size: x-large" align="center">
		<b><spring:message code="reservation.schedule" /></b>
	</legend>
	<br>
	<div style="width: 100%">
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.monday" /></b>
				</legend>
				<jstl:forEach items="${mondayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('L','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.tuesday" /></b>
				</legend>
				<jstl:forEach items="${tuesdayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('M','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.wednesday" /></b>
				</legend>
				<jstl:forEach items="${wednesdayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('X','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.thursday" /></b>
				</legend>
				<jstl:forEach items="${thursdayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('J','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.friday" /></b>
				</legend>
				<jstl:forEach items="${fridayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('V','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.saturday" /></b>
				</legend>
				<jstl:forEach items="${saturdayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('S','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
		<div style="float: left; width: 13%;">
			<fieldset style="width: 80%">
				<legend style="font-size: x-large" align="center">
					<b><spring:message code="reservation.sunday" /></b>
				</legend>
				<jstl:forEach items="${sundayHours}" var="hour">
					<jstl:choose>
						<jstl:when test="${hour.disponible}">
							<div id="horario" onclick="seleccionarCodigo('D','${hour.hora}')">
								<b>${hour.hora}</b>
							</div>
						</jstl:when>
						<jstl:otherwise>
							<div id="horarioNoDisponible">
								<b>${hour.hora}</b>
							</div>
						</jstl:otherwise>
					</jstl:choose>

				</jstl:forEach>
				<br>
			</fieldset>
		</div>
	</div>
</fieldset>

<form id="formCodigo" method="POST"
	action="reservation/client/reservarRoom.do">

	<input type="hidden" id="codigo" name="codigo" readonly="readonly" />
	<input type="hidden" id="resourceId" name="resourceId" readonly="readonly" value="${reservation.resource.id}"/>

	<input style="display: none;" type="submit" name="sendMessage"
		value="<spring:message code="incidence.message.send"/>" />
</form>

<br />
