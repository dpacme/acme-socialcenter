<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li>
			<b><spring:message code="monitor.name"/>:</b>
			${monitor.name}
		</li>
		<li>
			<b><spring:message code="monitor.surname"/>:</b>
			${monitor.surname}
		</li>
		<li>
			<b><spring:message code="monitor.email"/>:</b>
			${monitor.email}
		</li>
		<li>
			<b><spring:message code="monitor.phone"/>:</b>
			${monitor.phone}
		</li>
		<li>
			<b><spring:message code="monitor.postalAddress"/>:</b>
			${monitor.postalAddress}
		</li>
	</ul>
</div>

<br/>
<security:authorize access="hasRole('MANAGER')">


<b><spring:message code="monitor.listActivities"/></b>
<acme:button href="activity/listActivities.do?monitorId=${monitor.id}" name="listActivity"
			code="monitor.listActivities.see" />
</security:authorize>
