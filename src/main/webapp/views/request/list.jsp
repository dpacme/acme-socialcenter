<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<security:authorize access="hasRole('CLIENT')">

		<div>
			<H5>
				<a href="request/client/create.do"> <spring:message
						code="request.create" />
				</a>
			</H5>
		</div>

</security:authorize>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="requests" requestURI="${requestURI}" id="row">

	<!-- Attributes -->

	<spring:message code="request.status" var="status" />
	<display:column property="status" title="${status}" sortable="true" />

	<spring:message code="request.comment" var="comment" />
	<display:column property="comment" title="${comment}"
		sortable="true" />

	<spring:message code="request.socialCenter" var="socialCenterHeader" />
	<display:column title="${socialCenterHeader}" sortable="true" >
		${row.socialCenter.name} 
		<acme:button href="socialCenter/showDisplay.do?socialCenterId=${row.socialCenter.id}"
			name="see" code="socialCenter.see" />
	</display:column>
	
	<security:authorize access="hasRole('MANAGER')">
		<display:column>
			<jstl:if test="${manager.getSocialCenter().getId()==row.getSocialCenter().getId()}">
				<jstl:if test="${row.status=='pending'}">
					<acme:button href="request/manager/accept.do?requestId=${row.id}" name="acceptRequest"
					code="request.accept" />
					<acme:button href="request/manager/reject.do?requestId=${row.id}" name="rejectRequest"
					code="request.rejected" />
				</jstl:if>
				<jstl:if test="${row.status=='accepted'}">
					<acme:button href="request/manager/reject.do?requestId=${row.id}" name="acceptRequest"
					code="request.rejected" />
					<acme:button href="request/manager/complete.do?requestId=${row.id}" name="completeRequest"
					code="request.complete" />
				</jstl:if>
				<jstl:if test="${row.status=='rejected'}">
					<acme:button href="request/manager/accept.do?requestId=${row.id}" name="rejectRequest"
					code="request.accept" />
				</jstl:if>
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

