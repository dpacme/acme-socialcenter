<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="request/client/edit.do" modelAttribute="request">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="status" />
	<form:hidden path="client" />
	
	<acme:textbox code="request.comment" path="comment" />
	
	<div>
		<form:label path="socialCenter">
			<spring:message code="request.socialCenter" />
		</form:label>	
		<form:select path="socialCenter">
					<form:options items="${socialCenters}" itemValue="id" itemLabel="name" />
		</form:select>
		<form:errors path="socialCenter" cssClass="error" />
	</div>
	<input type="submit" name="save" value="<spring:message code="request.save" />" />&nbsp; 

	<input type="button" name="cancel"
		value="<spring:message code="request.cancel" />"
		onclick="javascript: window.location.replace('request/client/list.do');" />
<br />

</form:form>

