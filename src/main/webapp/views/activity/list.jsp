<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<display:table pagesize="5" class="displaytag"
	name="activities" requestURI="${requestURI}" id="row">

	<spring:message code="activity.startDate" var="startDate" />
	<display:column property="startDate" title="${startDate}" sortable="true" format="{0,date,dd/MM/YYYY HH:mm}"/>
	
	<spring:message code="activity.endDate" var="endDate" />
	<display:column property="endDate" title="${endDate}" sortable="true" format="{0,date,dd/MM/YYYY HH:mm}"/>
	
	<spring:message code="activity.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	
	<spring:message code="activity.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<spring:message code="activity.seats" var="seats" />
	<display:column property="seats" title="${seats}" sortable="true" />
	
	<spring:message code="activity.listClients" var="listClientsHeader" />
	<display:column title="${listClientsHeader}">
		<acme:button href="client/listClients.do?activityId=${row.id}" name="listActivity"
			code="activity.listClients.see" />
	</display:column>
	
	<spring:message code="activity.viewMonitor" var="viewMonitorHeader" />
	<display:column title="${viewMonitorHeader}">
		<acme:button href="monitor/show.do?monitorId=${row.getMonitor().getId()}" name="viewMonitor"
			code="activity.viewMonitor.see" />
	</display:column>
	
	<spring:message code="activity.viewReservation" var="viewReservationHeader" />
	<display:column title="${viewReservationHeader}">
	<jstl:if test="${row.activityReservation != null}">
		<acme:button href="activity/activityReservation/show.do?activityId=${row.getId()}" name="viewReservation"
			code="activity.viewReservation.see" />
			</jstl:if>
	</display:column>
	
	<security:authorize access="hasRole('CLIENT')">
			<spring:message code="activity.apply" var="applyHeader" />
			<display:column title="${applyHeader}">
				<jstl:if test="${client.getActivities().contains(row)}">
					<acme:button href="activity/client/unapply.do?activityId=${row.id}" name="applyReservation"
					code="activity.apply.no" />
				</jstl:if>
				<jstl:if
					test="${!client.getActivities().contains(row)}">
					<acme:button href="activity/client/apply.do?activityId=${row.id}" name="viewReservation"
					code="activity.apply.yes" />
				</jstl:if>
			</display:column>
	</security:authorize>
	
	<jstl:if test="${loginService.getPrincipal().getId()==row.getMonitor().getUserAccount().getId()}">
		<display:column>
			<acme:button href="activity/monitor/edit.do?activityId=${row.getId()}" name="edit"
				code="activity.edit" />
		</display:column>
	</jstl:if>

</display:table>

<jstl:if test="${activityStart != null}">
	<span class="message"><spring:message code="${activityStart}" /></span>
</jstl:if>
