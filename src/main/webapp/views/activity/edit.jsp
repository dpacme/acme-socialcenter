<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<script>
	$(function() {


		$('.datetimepicker').datetimepicker({
			dateFormat : 'dd/mm/yy',
		});

	});
</script>

<form:form action="${requestURI}" modelAttribute="activity">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="monitor" />
	<form:hidden path="clients" />

	<b><spring:message code="activity.data" /></b>

	<br />
	
	<spring:message code="activity.date.click" var="datePlaceholder" />
	<spring:message code="activity.startDate" />:
	<form:input type="text" id="startDate" path="startDate"
		readonly="readonly" placeholder="${datePlaceholder}"
		class="datetimepicker" />
	<form:errors class="error" path="startDate" />
	<br />

	<spring:message code="activity.endDate" />:
	<form:input type="text" id="endDate" path="endDate" readonly="readonly"
		placeholder="${datePlaceholder}" class="datetimepicker" />
	<form:errors class="error" path="endDate" />
	<jstl:if test="${errorDate!=null}">
		<span class="message"><spring:message code="${errorDate}" /></span>
	</jstl:if>
	<br />

	<acme:textbox code="activity.title" path="title" />

	<acme:textbox code="activity.description" path="description" />
	
	<spring:message code="activity.seats" />
	<form:input type="number" min="0" step="1" path="seats"/>
	<form:errors cssClass="error" path="seats" />
	<br/>

	
	<!-- Acciones -->
	<acme:submit name="save" code="activity.save"/>
	
	<jstl:if test="${requestURI=='activity/monitor/edit.do'}">
		<acme:submit name="delete" code="activity.delete"/>
	</jstl:if>
	
	
	
	<acme:cancel url="activity/monitor/myActivities.do" code="activity.cancel"/>

</form:form>

<br>

<jstl:if test="${errorClients!=null}">
	<span class="message"><spring:message code="${errorClients}" /></span>
</jstl:if>
