<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="comment/client/edit.do" modelAttribute="comment">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="client" />
	<form:hidden path="resource" />
	
	<acme:textbox code="comment.description" path="description" />
	
	<spring:message code="comment.score" />
	<form:input type="number"  min="1" max="5" path="score"/>
	<form:errors cssClass="error" path="score" />
	<br/>
	
	<input type="submit" name="save" value="<spring:message code="comment.save" />" />&nbsp; 

	<input type="button" name="cancel"
		value="<spring:message code="comment.cancel" />"
		onclick="javascript: window.location.replace('resource/showDisplay.do?resourceId=${comment.resource.id}&used=true');" />		
<br />

</form:form>

