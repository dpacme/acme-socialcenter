<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${requestURI}" modelAttribute="assessment">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="socialCenter" />

	<acme:textbox code="assessment.title" path="title" />
	<acme:textbox code="assessment.comment" path="comment" />
	
	<br />

	<!-- Acciones -->
	<acme:submit name="save" code="assessment.save"/>
	
	<input type="button" name="cancel"
		value="<spring:message code="assessment.cancel" />"
		onclick="javascript: window.location.replace('socialCenter/showDisplay.do?socialCenterId=${assessment.socialCenter.id}');" />

</form:form>

<br>

