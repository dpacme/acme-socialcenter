<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->


<form:form action="${requestURI}" modelAttribute="penaltyParameter">

	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<acme:textboxDays code="penaltyParameter.days" path="days"/>

	<acme:submit name="save" code="penaltyParameter.save" />

	<acme:cancel url="penaltyParameter/administrator/showDisplay.do"
		code="penaltyParameter.cancel" />

</form:form>

<br>
