<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<script>
	$(function() {


		$('.datetimepicker').datepicker({
			dateFormat : 'dd/mm/yy',
		});

	});
</script>

<form:form action="${requestURI}" modelAttribute="book">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="reservations" />
	<form:hidden path="comments" />
	<form:hidden path="socialCenter" />
	<form:hidden path="ticker" />

	<!-- Campos obligatorios a rellenar -->


	<acme:textbox code="book.picture" path="picture" />

	<acme:textbox code="book.isbn" path="isbn" />
	
	<acme:textbox code="book.title" path="title" />
	
	<acme:textbox code="book.author" path="author" />
	
	<acme:textbox code="book.editorial" path="editorial" />
	
	<acme:textbox code="book.description" path="description" />

	<spring:message code="book.date.click" var="datePlaceholder" />
	<spring:message code="book.publicationDate" />:
	<form:input type="text" id="publicationDate" path="publicationDate"
		readonly="readonly" placeholder="${datePlaceholder}"
		class="datetimepicker" />
	<form:errors class="error" path="publicationDate" />
	<br />
	
	<acme:select items="${categories}" itemLabel="title" code="book.categories" path="categories"/>
	
	<!-- Acciones -->
	
	<acme:submit name="save" code="book.save"/>
	
	
	 <acme:cancel url="socialCenter/manager/mySocialCenter.do" code="book.cancel"/>

</form:form>

<br>
